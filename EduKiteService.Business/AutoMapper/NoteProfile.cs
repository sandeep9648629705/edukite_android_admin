﻿using AutoMapper;
using EdukiteService.Business.Entities;
using EdukiteService.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EdukiteService.Business.AutoMapper
{
    public class NoteProfile : Profile
    {
        public NoteProfile()
        {
            CreateMap<NoteModel, Note>()
       .ForMember(dest1 => dest1.AssetId, opt => opt.MapFrom(src => src.AssetId))
       .ForMember(dest2 => dest2.Comment, opt => opt.MapFrom(src => src.Text))
       .ForMember(dest2 => dest2.CreatedDatetime, opt => opt.MapFrom(o => DateTime.Now))
        .ForMember(dest2 => dest2.imagePath, opt => opt.MapFrom(src => src.ImagePath));

            CreateMap<Note, BaseModel>()
         .ForMember(dest1 => dest1.Id, opt => opt.MapFrom(src => src.NoteId))
         .ForMember(dest2 => dest2.Text, opt => opt.MapFrom(src => src.Comment))
         .ForMember(dest2 => dest2.DateTime, opt => opt.MapFrom(src => src.CreatedDatetime))
          .ForMember(dest2 => dest2.ImagePath, opt => opt.MapFrom(src => src.imagePath));

            CreateMap<IEnumerable<BaseModel>, NotesResponse>().ForMember(dest => dest.Notes, opt => opt.MapFrom(src => src));
            CreateMap<Note, NotesResponse>().ForMember(dest => dest.NoteId, opt => opt.MapFrom(src => src.NoteId));

        }
    }
}
