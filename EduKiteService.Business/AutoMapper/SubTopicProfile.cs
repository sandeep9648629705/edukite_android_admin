﻿using AutoMapper;
using EdukiteService.Business.Entities;
using EdukiteService.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EduKiteService.Business.AutoMapper
{
    public class SubTopicProfile:Profile
    {
        public SubTopicProfile()
        {
            CreateMap<SubTopicModel, Subtopic>()
                .ForMember(x => x.Assets, o => o.Ignore())
                .ForMember(x => x.BookmarkSubtopics, o => o.Ignore())
                .ForMember(x => x.Notes, o => o.Ignore())
                .ForMember(x => x.Topic, o => o.Ignore())
                .ForMember(x => x.VideoComments, o => o.Ignore());

        }
    }
}
