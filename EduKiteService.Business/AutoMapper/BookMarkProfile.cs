﻿using AutoMapper;
using EdukiteService.Business.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EdukiteService.Business.AutoMapper
{
    public class BookMarkProfile : Profile
    {
        public BookMarkProfile()
        {
            CreateMap<IEnumerable<BookMarksModel>, BookMarksResponse>().ForMember(dest => dest.BookMarks, opt => opt.MapFrom(src => src));
        }
    }
}
