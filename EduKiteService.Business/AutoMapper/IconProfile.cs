﻿using AutoMapper;
using EdukiteService.Business.Entities;
using EdukiteService.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EduKiteService.Business.AutoMapper
{
    public class IconProfile : Profile
    {
        public IconProfile()
        {
            CreateMap<Icon, IconsModel>();

            CreateMap<IconsModel, Icon>();
        }
    }
}
