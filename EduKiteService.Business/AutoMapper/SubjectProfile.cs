﻿using AutoMapper;
using EdukiteService.Business.Entities;
using EdukiteService.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EduKiteService.Business.AutoMapper
{
    public class SubjectProfile : Profile
    {
        public SubjectProfile()
        {
            CreateMap<SubjectModel, Subject>()
                 .ForMember(destination => destination.Theme, opts => opts.MapFrom(source => source.Themedb))                
                .ForMember(x => x.Modules, o => o.Ignore())
                .ForMember(x => x.Notes, o => o.Ignore())
                .ForMember(x => x.SubjectGradeCombinations, o => o.Ignore())
                .ForMember(x => x.UserSubjectGradeMappings, o => o.Ignore())
                .ForMember(x => x.UserTrackingDetails, o => o.Ignore())
                .ForMember(x => x.VideoComments, o => o.Ignore())
                .ForMember(x => x.Grade, o => o.Ignore());

            CreateMap<Subject, SubjectModel>()
                .ForMember(destination => destination.Themedb, opts => opts.MapFrom(source => source.Theme))
                .ForMember(destination => destination.theme, opts => opts.MapFrom(source => source.Theme))
                .ForMember(x => x.name, o => o.Ignore())
                .ForMember(x => x.id, o => o.Ignore())
                .ForMember(x => x.totalExamPapers, o => o.Ignore())
                .ForMember(x => x.completedExamPapers, o => o.Ignore());

        }
    }
}
