﻿using AutoMapper;
using EdukiteService.Business.Entities;
using EdukiteService.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EdukiteService.Bussiness.AutoMapper
{
    public class NotificationProfile : Profile
    {
        public NotificationProfile()
        {
            CreateMap<int, NotificationRespose>().ForMember(dest => dest.Count, opt => opt.MapFrom(src => src));
        }
    }
}
