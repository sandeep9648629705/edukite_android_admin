﻿using AutoMapper;
using EdukiteService.Business.Entities;
using EdukiteService.Data;

namespace EduKiteService.Business.AutoMapper
{
    public class GradeProfile : Profile
    {
        public GradeProfile()
        {
            CreateMap<GradeModel, Grade>()
      .ForMember(dest1 => dest1.Phase, opt => opt.Ignore())
       .ForMember(dest2 => dest2.Modules, opt => opt.Ignore())
       .ForMember(dest1 => dest1.SubjectGradeCombinations, opt => opt.Ignore())
       .ForMember(dest2 => dest2.Subscriptions, opt => opt.Ignore())
       .ForMember(dest1 => dest1.SubscriptionAmounts, opt => opt.Ignore())
       .ForMember(dest1 => dest1.IsPublished, opt => opt.Ignore())
       .ForMember(dest2 => dest2.UserTrackingDetails, opt => opt.Ignore());
        }

    }
}
