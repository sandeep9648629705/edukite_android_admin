﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AutoMapper;
using EdukiteService.Business.Entities;
using EdukiteService.Data;

namespace EduKiteService.Business.AutoMapper
{
    public class ThemeProfile:Profile
    {
        public ThemeProfile()
        {
            CreateMap<Theme, ThemeModel>();
        }
    }
}
