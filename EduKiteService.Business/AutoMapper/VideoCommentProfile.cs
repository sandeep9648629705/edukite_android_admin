﻿using AutoMapper;
using EdukiteService.Data;
using EdukiteService.Business.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EdukiteService.Business.AutoMapper
{
    public class VideoCommentProfile : Profile
    {
        public VideoCommentProfile()
        {
            CreateMap<VideoCommentModel, VideoComment>()
                        .ForMember(dest1 => dest1.Comment, opt => opt.MapFrom(src => src.Text))
                        .ForMember(dest1 => dest1.ParentId, opt => opt.MapFrom(src => src.CommentId))
                        .ForMember(dest1 => dest1.CreatedDateTime, opt => opt.MapFrom(src => DateTime.Now));


            CreateMap<List<VideoCommentReplies>, VideoCommentResponse>().ForMember(dest => dest.Comments, opt => opt.MapFrom(src => src));
            CreateMap<List<VideoCommentBaseModel>, VideoCommentResponse>().ForMember(dest => dest.Replies, opt => opt.MapFrom(src => src));
        }
    }
}
