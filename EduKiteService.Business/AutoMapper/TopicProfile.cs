﻿using AutoMapper;
using EdukiteService.Business.Entities;
using EdukiteService.Data;

namespace EduKiteService.Business.AutoMapper
{
    public class TopicProfile : Profile
    {
        public TopicProfile()
        {
            CreateMap<Topic, TopicModel>();
            CreateMap<TopicModel, Topic>();
        }
    }
}
