﻿using EdukiteService.Business.Entities;
using EdukiteService.Data;
using EdukiteService.Business.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EdukiteService.Business.Interface
{
    public interface INoteService
    {
        bool DeleteNote(NoteModel notesModel);
        NotesResponse AddNote(NoteModel notesModel);
        NotesResponse GetNotes(NoteModel notes);
    }
}
