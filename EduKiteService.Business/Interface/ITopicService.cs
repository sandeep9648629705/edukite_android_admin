﻿using EdukiteService.Business.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EdukiteService.Business.Interface
{
    public interface ITopicService
    {
        TopicDetailsModel GetTopicDetails(int topicId);
        List<TopicModel> GetTopics();

        List<TopicModel> GetTopicsBySubjcectID(int SubjID);

        TopicModel GetTopicByName(string topicName, int SubID);
        //add topic
        void AddTopic(TopicModel topic);

        int AddTopicRetID(TopicModel topic);
        //edit topic get topic details by id
        TopicModel GetTopicById(int topicId);
        //updated edit topic
        void UpdateTopic(TopicModel topicModel);
        //delete topic
        void DeleteTopic(int topicID);
        //save re-ordered topics based on topicNo sortable
        Task<string> UpdateSortedTopics(List<TopicModel> lsttopicModel);
        void DeleteTopicBySubjectId(int SubjID);

    }
}
