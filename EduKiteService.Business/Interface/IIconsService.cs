﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using EdukiteService.Business.Entities;
using EdukiteService.Data;

namespace EdukiteService.Business.Interface
{
    public interface IIconsService
    {
        void AddIcon(List<Icon> iconData, int id);
        string GetIconById(int subId);
        void UpdateIcon(List<Icon> iconData, int id);
        void DeleteIcon(int subjectId);
    }
}
