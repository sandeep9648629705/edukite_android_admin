﻿using EdukiteService.Business.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EdukiteService.Business.Interface
{
    public interface IModuleService
    {
        string AddModule(ModuleModel moduleModel);

        void AddModuleList(List<ModuleModel> lstModuleModel);

        Task<string> AddModuleListInsertAsync(List<ModuleModel> lstModuleModel);

        void DeleteModule(int gradeId);
        void DeleteModuleBySubjectId(int subId);
    }
}
