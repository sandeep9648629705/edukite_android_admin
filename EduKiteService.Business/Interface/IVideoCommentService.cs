﻿using EdukiteService.Business.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EdukiteService.Business.Interface
{
    public interface IVideoCommentService
    {
        VideoCommentResponse GetComments(VideoCommentModel videoCommentModel, int PageNum = 0, int PageSize = 20);
        VideoCommentResponse GetRepliesComments(VideoCommentModel videoCommentModel, int PageNum = 0, int PageSize = 20);
        void AddComment(VideoCommentModel videoCommentModel);
        bool AddReply(VideoCommentModel videoCommentModel);
    }
}
