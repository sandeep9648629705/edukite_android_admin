﻿using EdukiteService.Business.Entities;
using EdukiteService.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EduKiteService.Business.Interface
{
    public  interface IUsersServices
    {
        List<UsersModel> AddSubscription(int UserId, int UserGroup);

        void AddUser(UsersModel user);
        void DeletedeactiveUser(int userid);

        UserModel GetUserByName(string FirstName);
        UserModel GetUserByLastName(string LastName);
        List<getUserModel> GetUsers();
        getUserModel GetUserByUserID(int Userid);
        void UpdateUser(getUserModel User);

    }
}
