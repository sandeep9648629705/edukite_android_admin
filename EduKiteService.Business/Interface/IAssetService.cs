﻿using EdukiteService.Business.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EdukiteService.Business.Interface
{
    public interface IAssetService
    {

        List<AssetTypeModel> GetAssetType();
        List<AssetTypeModel> GetSimIntAssetType();

        void AddAsset(AssetModel assetModel);

        int AddAssetTypeReturnId(AssetTypeModel assetTypeModel);

        void AddAssetList(List<AssetModel> lstassetModel);

        Task<string> AssetbulkInsertAsync(List<AssetModel> lstassetModel);
        string AddAssetInsert(AssetModel assetdata);
        List<AssetModel> GetAssetByTopicId(int topicId);

        List<AssetModel> GetAssetByTopicIdWithoutSubTopic(int topicId);
        AssetModel GetAssetByAssetId(Guid AssetId);

        
        void UpdateAsset(AssetModel assetModel);
        void DeleteAsset(Guid assetID);
        Task<string> UpdateSortedAsset(List<AssetModel> lstAssetModel);
        void DeleteAssetByTopicId(int topicId);
        void DeleteAssetBySubTopicId(int subTopicId);

    }
}
