﻿using EdukiteService.Business.Entities;
using EdukiteService.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EduKiteService.Business.Interface
{
    public interface IExamPaperService
    {
        List<ExamPapertype> GetExamPaperType();
        void AddExamPaper(ExamPaperModel examModel);
        ExamYearModel GetExamYearIDByYearName(string year);
        ExamYearModel GetExamYearIDByYearName(string year,int yearno);
        ExamPaperModel GetExamPaperIdbyPaperNo(string PaperNo);
        ExamPaperQuestionModel GetExamPaperQuestionIdby(string QuestionNo, int Paperid);
        ExamPaperQuestionModel GetExamPaperQuestionId(string QuestionNo, int Paperid);

        List<ExamPaperModel> GetPaperList();
        List<ExamPaperQuestionModel> GetQuestions(int paperid);
        List<ExamPaperQuestionOptionModel> GetOptions(int id);
        void AddPaper(ExamPaperModel paper);
        void AddQuestion(ExamPaperQuestionModel paperModel);
        void AddExamyear(ExamYearModel Examyear);
        void AddOption(ExamPaperQuestionOptionModel optionModel);




    }
}
