﻿using EdukiteService.Business.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EdukiteService.Business.Interface
{
    public interface IProvinceService
    {
        List<ProvinceModel> GetProvince();
    }
}
