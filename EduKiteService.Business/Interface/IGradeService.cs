﻿using EdukiteService.Business.Entities;
using EdukiteService.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EdukiteService.Business.Interface
{
    public interface IGradeService
    {
        List<GradeModel> GetGrades();
        List<GradeModel> GetGrades(ActivityModel activityModel, int userId);

        void AddGrades(GradeModel grade);

        void UpdateGrade(GradeModel grade);
        GradeModel GetGradeById(int gradeID);
        GradeModel GetGradeByName(string gradeName);
        String GetGradeNameByGradeID(int gradeId);
        void PublishGrade(int gradeID);
        void DeleteGrade(int gradeID);
        bool IsGradePublished(int gradeID);
      

    }
}
