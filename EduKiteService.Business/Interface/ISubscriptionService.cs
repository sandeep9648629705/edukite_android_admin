﻿using EdukiteService.Business.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EdukiteService.Business.Interface
{
    public interface ISubscriptionService
    {
        SubscriptionResponse GetSubscriptions(int UserId, string DeviceId);
        bool ActivateByCode(int UserId, string DeviceId, string Code);
        SubscriptionResponse ChooseSubscriptions(int UserId, string DeviceId);
    }
}
