﻿using EdukiteService.Business.Entities;
using EdukiteService.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EdukiteService.Business.Interface
{
    public interface ISubjectService
    {
        List<SubjectModel> GetSubjects(int gradid);
        SubjectDetailsModel GetSubjectDetails(int SubjectId);

        List<ThemeModel> GetThemes();
        int AddSubject(SubjectModel subjectData);
        SubjectModel GetSubjectById(int id);
        String GetSubjectNameById(int id);
        void UpdateSubject(SubjectModel subjectData);
        void DeleteSubject(int subjectId);
        void PublishSubject(int subjectID);

        int GetPublishedSubjectCount(int gradeID);
        ThemeModel GetThemesById(int themeId);

        SubjectModel GetSubjectsByName(string subjectName, int gradeId);

        void DeleteSubjectAll(int gradeId);

    }
}
