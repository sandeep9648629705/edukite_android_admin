﻿using AutoMapper;
using EdukiteService.Business.Entities;
using EdukiteService.Data;
using EdukiteService.Data.interfaces;
using EduKiteService.Business.Interface;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EduKiteService.Business.Implementation
{
   public class ExamPaperService: IExamPaperService
    {
        public IRepository<ExamPaper> _ExamPaperRespository;
        public IRepository<ExamPapertype> _ExamPaperTypeRespository;
        public IRepository<ExamYear> _ExamYearRespository;
        public IRepository<ExamPaperQuestion> _ExamPaperQuestionRespository;
        public IRepository<ExamPaperQuestionOption> _ExamPaperQuestionOptionsRespository;
        public IRepository<ExamPaperTopic> _ExampaperTopicRespository;
        //public IRepository<ExamPaperQuestion> _QuestionsList;


        public IMapper mapper { get; set; }
        public ExamPaperService(IRepository<ExamPaper> ExamPaperRespository, IRepository<ExamPapertype> ExamPaperTypeRespository,
         IRepository<ExamYear> ExamYearRespository, IRepository<ExamPaperQuestion> ExamPaperQuestionRespository, IRepository<ExamPaperQuestionOption> ExamPaperQuestionOptionsRespository
            , IRepository<ExamPaperTopic> ExampaperTopicRespository)
        {
            _ExamPaperRespository = ExamPaperRespository;
            _ExamPaperTypeRespository = ExamPaperTypeRespository;
            _ExamYearRespository = ExamYearRespository;
            _ExamPaperQuestionRespository = ExamPaperQuestionRespository;
            _ExamPaperQuestionOptionsRespository = ExamPaperQuestionOptionsRespository;
            _ExampaperTopicRespository = ExampaperTopicRespository;
        }

               
        public void AddExamPaper(ExamPaperModel examModel)
        {
            ExamPaper data = mapper.Map<ExamPaperModel,ExamPaper>(examModel);
            _ExamPaperRespository.Add(data);
            _ExamPaperRespository.SaveChanges();

        }
        public List<ExamPapertypeModel> GetExamPaperType()
        {
            using (Edukite_AndroidEntities ee = new Edukite_AndroidEntities())
            {
                var EtList = ee.ExamPapertypes.ToList();
                List<ExamPapertypeModel> ETData = mapper.Map<List<ExamPapertype>, List<ExamPapertypeModel>>(EtList);
                return ETData;
            }
        }

        public ExamYearModel GetExamYearIDByYearName( string year)
        {
                var record = _ExamYearRespository.Fetch(x => x.Year.Equals(year)).SingleOrDefault();
                ExamYearModel EYData = mapper.Map<ExamYear, ExamYearModel>(record);                
                return EYData;
        }
        public ExamYearModel GetExamYearIDByYearName(string year, int yearno)
        {
            var record = _ExamYearRespository.Fetch(x => x.Year.Equals(year) && x.YearNo == yearno).SingleOrDefault();
            ExamYearModel EYData = mapper.Map<ExamYear, ExamYearModel>(record);
            return EYData;
        }
        public ExamPaperModel GetExamPaperIdbyPaperNo(string PaperNo)
        {
            var record = _ExamPaperRespository.Fetch(x => x.PaperNo==PaperNo).SingleOrDefault();
            ExamPaperModel EPData = mapper.Map<ExamPaper, ExamPaperModel>(record);
            return EPData;
        }
        public ExamPaperQuestionModel GetExamPaperQuestionIdby(string QuestionNo,int Paperid)
        {
            var record = _ExamPaperQuestionRespository.Fetch(x => x.PaperId == Paperid && x.QuestionNo.Equals(QuestionNo)).SingleOrDefault();
            ExamPaperQuestionModel EPQData = mapper.Map<ExamPaperQuestion, ExamPaperQuestionModel>(record);
            return EPQData;
        }

        public ExamPaperQuestionModel GetExamPaperQuestionId(string QuestionNo, int Paperid)
        {
            var record = _ExamPaperQuestionRespository.Fetch(x => x.PaperId == Paperid && x.QuestionNo.Equals(QuestionNo)).SingleOrDefault();
            ExamPaperQuestionModel EPQData = mapper.Map<ExamPaperQuestion, ExamPaperQuestionModel>(record);
            return EPQData;
        }
		public List<ExamPaperModel> GetPaperList()
        {
            var topics = _ExamPaperRespository.GetAll().ToList();
            return mapper.Map<List<ExamPaper>, List<ExamPaperModel>>(topics);
        }
        public List<ExamPaperQuestionModel> GetQuestions(int paperid)
        {
            var id = paperid;
            var questions = _ExamPaperQuestionRespository.Fetch(x => x.PaperId==id).ToList();
            return mapper.Map<List<ExamPaperQuestion>, List<ExamPaperQuestionModel>>(questions);
        }
        public List<ExamPaperQuestionOptionModel> GetOptions(int id)
        {

            var options = _ExamPaperQuestionOptionsRespository.Fetch(x => x.QuestionId == id).ToList();
            return mapper.Map<List<ExamPaperQuestionOption>, List<ExamPaperQuestionOptionModel>>(options);
        }
        public void AddPaper(ExamPaperModel paperModel)
        {
            ExamPaper clientModel = mapper.Map<ExamPaperModel, ExamPaper>(paperModel);
            _ExamPaperRespository.Add(clientModel);
            _ExamPaperRespository.SaveChanges();
        }

        public void AddQuestion(ExamPaperQuestionModel paperModel)
        {
            ExamPaperQuestion clientModel = mapper.Map<ExamPaperQuestionModel, ExamPaperQuestion>(paperModel);
            _ExamPaperQuestionRespository.Add(clientModel);
            _ExamPaperQuestionRespository.SaveChanges();
        }

        public void AddExamyear(ExamYearModel Examyear)
        {
            ExamYear clientModel = mapper.Map<ExamYearModel, ExamYear>(Examyear);
            _ExamYearRespository.Add(clientModel);
            _ExamYearRespository.SaveChanges();
        }
        public void AddOption(ExamPaperQuestionOptionModel optionModel)
        {
            ExamPaperQuestionOption clientModel = mapper.Map<ExamPaperQuestionOptionModel, ExamPaperQuestionOption>(optionModel);
            _ExamPaperQuestionOptionsRespository.Add(clientModel);
            _ExamPaperQuestionOptionsRespository.SaveChanges();
        }



        List<ExamPapertype> IExamPaperService.GetExamPaperType()
        {
            throw new NotImplementedException();
        }
    }
}
