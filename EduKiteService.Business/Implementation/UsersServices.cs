﻿using AutoMapper;
using EdukiteService.Business.Interface;
using EdukiteService.Business.Entities;
using EdukiteService.Data;
using EdukiteService.Data.interfaces;
using EdukiteService.Utilities;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Data.Entity.Migrations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using EduKiteService.Business.Interface;

namespace EduKiteService.Business.Implementation
{
    public class UsersServices : IUsersServices
    {
        public IMapper mapper { get; set; }
        private readonly IRepository<User> _UsersRepository;
        private readonly IRepository<Subscription> _SubscriptionRepository;
        private readonly IRepository<UserGradeMapping> _UserGradeMappingRepository;
        private readonly IRepository<Grade> _UserGradeRepository;
        private readonly IRepository<School> _UserSchoolRepository;
        private readonly IRepository<UserGroup> _UserGroupRepository;
        private readonly IRepository<UserType> _UserTypeRepository;
        private readonly IRepository<Province> _UserProvinceRepository;

        public UsersServices(IRepository<User> UsersRepository, IRepository<Subscription> SubscriptionRepository,
            IRepository<UserGradeMapping> UserGradeMappingRepository, IRepository<Grade> GradeRepository,
            IRepository<School> SchoolRepository, IRepository<UserGroup> UserGroupRepository, IRepository<UserType> UserTypeRepository,
            IRepository<Province> UserProvinceRepository)
        {
            _UsersRepository = UsersRepository;
            _SubscriptionRepository = SubscriptionRepository;
            _UserGradeMappingRepository = UserGradeMappingRepository;
            _UserGradeRepository = GradeRepository;
            _UserSchoolRepository = SchoolRepository;
            _UserGroupRepository = UserGroupRepository;
            _UserTypeRepository = UserTypeRepository;
            _UserProvinceRepository = UserProvinceRepository;
        }
        public void AddUser(UsersModel userModel)
        {
            User clientModel = mapper.Map<UsersModel, User>(userModel);
            _UsersRepository.Add(clientModel);
            _UsersRepository.SaveChanges();
        }

        public List<UsersModel> AddSubscription(int UserId, int UserGroup)
        {
            return _UsersRepository.ExecWithStoreProcedure<UsersModel>(Constants.sp_SetSubscription,
                 new SqlParameter(Constants.SP_PARAM_ACTIVITY_USERID, SqlDbType.Int) { Value = UserId },
                 new SqlParameter(Constants.SP_PARAM_ACTIVITY_USERGROUPID, SqlDbType.Int) { Value = UserGroup }
                 ).ToList();
        }

        public UserModel GetUserByName(string FirstName)
        {
            var record = _UsersRepository.Fetch(x => x.FirstName.Equals(FirstName)).FirstOrDefault();
            UserModel clientModel = mapper.Map<User, UserModel>(record);
            return clientModel;

        }

        public UserModel GetUserByLastName(string LastName)
        {
            var record = _UsersRepository.Fetch(x => x.LastName.Equals(LastName)).FirstOrDefault();
            UserModel clientModel = mapper.Map<User, UserModel>(record);
            return clientModel;

        }
        //-----------------------------Sandeep-----------------------------------------
        public List<getUserModel> GetUsers()
        {
            var users = _UsersRepository.GetAll().ToList();
            
            return mapper.Map<List<User>, List<getUserModel>>(users);
        }
        public getUserModel GetUserByUserID(int Userid)
        {
            var data = _UsersRepository.Fetch(x => x.UserId == (Userid)).SingleOrDefault();
            return mapper.Map<User, getUserModel>(data);
        }

        public void DeletedeactiveUser(int userid)
        {
            User record = _UsersRepository.Fetch(x => x.UserId == userid).SingleOrDefault();
            var record1 = _SubscriptionRepository.Fetch(x => x.UserId == userid).ToList();
            var record2 = _UserGradeMappingRepository.Fetch(x => x.userId == userid).ToList();
            if (record.IsActive == false)
            {
                if (record1!=null)
                {
                    _SubscriptionRepository.DeleteAll(record1);
                }
                if (record2 != null)
                {
                    _UserGradeMappingRepository.DeleteAll(record2);
                }
                _UsersRepository.Delete(record);
                _UsersRepository.SaveChanges();

            }        
            // _UsersRepository.UpdateChanges(record);

        }
		 public void UpdateUser(getUserModel User)
        {
            try
            {
                var user = mapper.Map<getUserModel, User>(User); //Mapper.Map<User>(User);
            using (Edukite_AndroidEntities edukite_AndroidEntities = new Edukite_AndroidEntities())
            {
                var record = edukite_AndroidEntities.Users.FirstOrDefault(x => x.UserId == user.UserId);
                    record = user;
                    edukite_AndroidEntities.Users.AddOrUpdate(record);
                    edukite_AndroidEntities.SaveChanges();
            }
                
            }
            catch (Exception ex)
            {
                throw ex;
            }

        }
        public void GetschoolnamebyUserID(int userid)
        {
            var data = _UserSchoolRepository.Fetch(x => x.SchoolId == (userid)).SingleOrDefault();
            //return data.SchoolName;
            
        }

    }
}
