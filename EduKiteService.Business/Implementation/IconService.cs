﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using EdukiteService.Business.Entities;
using EdukiteService.Business.Interface;


using AutoMapper;
using EdukiteService.Data;
using EdukiteService.Data.interfaces;
using EdukiteService.Utilities;
using System.Data;
using System.Data.SqlClient;

namespace EdukiteService.Business.Implementation
{
    public class IconService : IIconsService
    {
        public IMapper mapper { get; set; }
        public IRepository<Icon> _iconsRepository;

        public IconService(IRepository<Icon> iconRepository)
        {
            _iconsRepository = iconRepository;
        }

        public IconService()
        {
        }

        public void AddIcon(List<Icon> iconData, int id)
        {
            foreach (Icon i in iconData)
            {
                i.SubjectId = id;
                _iconsRepository.Add(i);
                _iconsRepository.SaveChanges();

            }
        }

        public string GetIconById(int subId)
        {
            var record = _iconsRepository.Fetch(x => x.SubjectId == subId).Take(1).SingleOrDefault();
            return record.IconPath;
        }

        public void UpdateIcon(List<Icon> iconData, int id)
        {
            List<Icon> record = new List<Icon>();
            foreach (Icon p in iconData)
            {
                p.SubjectId = id;
                record.Add(p);
            }

            var icons = _iconsRepository.Fetch(x => x.SubjectId == id).ToList();
            _iconsRepository.DeleteAll(icons);
            _iconsRepository.AddRange(record);
            _iconsRepository.SaveChanges();


        }
        public void DeleteIcon(int subjectId)
        {
            List<Icon> record = new List<Icon>();
            var icons = _iconsRepository.Fetch(x => x.SubjectId == subjectId).ToList();
            _iconsRepository.DeleteAll(icons);
            _iconsRepository.SaveChanges();
        }

    }
}