﻿using AutoMapper;
using EdukiteService.Business.Entities;
using EdukiteService.Data;
using EdukiteService.Data.interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;
using EdukiteService.Business.Interface;
namespace EdukiteService.Business.Implementation
{
    public class VideoCommentService : IVideoCommentService
    {
        
        private readonly IRepository<VideoComment> _VideoCommentRepository;
        public VideoCommentService(IRepository<VideoComment> VideoCommentRepository)
        {
            _VideoCommentRepository = VideoCommentRepository;
        }


        public void AddComment(VideoCommentModel videoCommentModel)
        {
            var videoComment = Mapper.Map<VideoComment>(videoCommentModel);
            _VideoCommentRepository.Add(videoComment);
        }

        public bool AddReply(VideoCommentModel videoCommentModel)
        {
            var comment = _VideoCommentRepository.Fetch(x => x.Id == videoCommentModel.CommentId).SingleOrDefault();
            if (comment == null)
                return false;

            var videoComment = Mapper.Map<VideoComment>(videoCommentModel);
            _VideoCommentRepository.Add(videoComment);
            return true;
        }

        public VideoCommentResponse GetComments(VideoCommentModel videoCommentModel, int PageNum = 0, int PageSize = 20)
        {

            using (Edukite_AndroidEntities edukite_AndroidEntities = new Edukite_AndroidEntities())
            {
                var Query1 = (from v1 in edukite_AndroidEntities.VideoComments
                              where v1.ParentId != 0
                              group v1 by v1.ParentId
                              into record2
                              select new { ParentId = record2.Key, RepliesCount = record2.Count() });
                var Query2 = (from v2 in edukite_AndroidEntities.VideoComments
                              join r2 in Query1 on v2.Id equals r2.ParentId into record3
                              from r3 in record3.DefaultIfEmpty()
                              join u in edukite_AndroidEntities.Users on v2.UserId equals u.UserId
                              where v2.UserId == videoCommentModel.UserId /*&& v2.DeviceId == videoCommentModel.DeviceId*/ && v2.TopicId == videoCommentModel.TopicId
                              && v2.SubjectId == videoCommentModel.SubjectId && v2.SubTopicId == videoCommentModel.SubTopicId
                              && v2.AssetId == videoCommentModel.AssetId
                              select new VideoCommentReplies
                              {
                                  Id = r3 != null ? r3.ParentId : v2.Id,
                                  Text = v2.Comment,
                                  PostedBy = u.FirstName + " " + u.LastName,
                                  DateTime = v2.CreatedDateTime,
                                  UserThumbailPath = v2.userThumbailPath,
                                  RepliesCount = r3 != null ? r3.RepliesCount : 0
                              }).Distinct().OrderBy(x => x.Id).Skip((PageNum - 1) * PageSize).Take(PageSize).ToList();

                return Mapper.Map<VideoCommentResponse>(Query2);
            }


        }

        public VideoCommentResponse GetRepliesComments(VideoCommentModel videoCommentModel, int PageNum = 0, int PageSize = 20)
        {
            using (Edukite_AndroidEntities edukite_AndroidEntities = new Edukite_AndroidEntities())
            {
                var lists = (from v2 in edukite_AndroidEntities.VideoComments
                             join u in edukite_AndroidEntities.Users on v2.UserId equals u.UserId
                             where v2.UserId == videoCommentModel.UserId /*&& v2.DeviceId == videoCommentModel.DeviceId*/ && v2.ParentId == videoCommentModel.CommentId
                             select new VideoCommentBaseModel
                             {
                                 Id = v2.Id,
                                 Text = v2.Comment,
                                 PostedBy = u.FirstName + " " + u.LastName,
                                 DateTime = v2.CreatedDateTime,
                                 UserThumbailPath = v2.userThumbailPath,
                             }).Distinct().OrderBy(x => x.Id).Skip((PageNum - 1) * PageSize).Take(PageSize).ToList();

                return Mapper.Map<VideoCommentResponse>(lists);
            }
        }
    }
}
