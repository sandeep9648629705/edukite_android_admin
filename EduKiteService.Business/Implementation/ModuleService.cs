﻿using AutoMapper;
using EdukiteService.Data;
using EdukiteService.Data.interfaces;
using EdukiteService.Business.Interface;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using EdukiteService.Business.Entities;

namespace EdukiteService.Business.Implementation
{
    public class ModuleService : IModuleService
    {
        public IRepository<Module> _ModuleRespository;
        public IMapper mapper { get; set; }
        public ModuleService(IRepository<Module> moduleRepository)
        {
            _ModuleRespository = moduleRepository;
        }

        public string AddModule(ModuleModel moduleModel)
        {
            var record = _ModuleRespository.Fetch(x => x.ModuleName == moduleModel.ModuleName && x.SubjectId == moduleModel.SubjectId && x.GradeId == moduleModel.GradeId).SingleOrDefault();
            if (record == null)// only insert if Module not exist
            {
                Module data = mapper.Map<ModuleModel, Module>(moduleModel);
                _ModuleRespository.Add(data);
                _ModuleRespository.SaveChanges();
            }
            return "Module added Successfully!";
        }



        public void AddModuleList(List<ModuleModel> lstModuleModel)
        {
            List<Module> data = mapper.Map<List<ModuleModel>, List<Module>>(lstModuleModel);
            _ModuleRespository.AddRange(data);
            _ModuleRespository.SaveChanges();
        }

        public async Task<string> AddModuleListInsertAsync(List<ModuleModel> lstModuleModel)
        {
            List<Module> data = mapper.Map<List<ModuleModel>, List<Module>>(lstModuleModel);
            _ModuleRespository.AddRange(data);
            _ModuleRespository.SaveChangesAsync();
            return "Module added Successfully!";
        }

        public void DeleteModule(int gradeId)
        {
            var record = _ModuleRespository.Fetch(x => x.GradeId == gradeId).ToList();
            _ModuleRespository.DeleteAll(record);
            _ModuleRespository.SaveChanges();
        }

        public void DeleteModuleBySubjectId(int subId)
        {
            var record = _ModuleRespository.Fetch(x => x.SubjectId == subId).ToList();
            _ModuleRespository.DeleteAll(record);
            _ModuleRespository.SaveChanges();
        }
    }
}
