﻿using EdukiteService.Business.Interface;
using System;
using EdukiteService.Data;
using EdukiteService.Data.interfaces;
using EdukiteService.Utilities;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AutoMapper;
using System.Data;
using System.Data.SqlClient;
using EdukiteService.Business.Entities;

namespace EdukiteService.Business.Implementation
{
    public class ProvinceService : IProvinceService
    {
        public IMapper Mapper { get; set; }
        private readonly IRepository<Province> _ProvinceRepository;

        public ProvinceService(IRepository<Province> ProvinceRepository)
        {
            _ProvinceRepository = ProvinceRepository;
        }


        public List<ProvinceModel> GetProvince()
        {
            var province = _ProvinceRepository.GetAll().ToList();
            return Mapper.Map<List<Province>, List<ProvinceModel>>(province);
        }

        public List<ProvinceModel> GetProvince(int selectedProvince)
        {
            throw new NotImplementedException();
        }
    }
}
