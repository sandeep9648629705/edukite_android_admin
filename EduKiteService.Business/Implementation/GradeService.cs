﻿using AutoMapper;
using EdukiteService.Business.Interface;
using EdukiteService.Business.Entities;
using EdukiteService.Data;
using EdukiteService.Data.interfaces;
using EdukiteService.Utilities;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EdukiteService.Business.Implementation
{
    public class GradeService : IGradeService
    {
        public IMapper mapper { get; set; }
        private readonly IRepository<Grade> _GradeRepository;

        public GradeService(IRepository<Grade> GradeRepository)
        {
            _GradeRepository = GradeRepository;
        }

        public List<GradeModel> GetGrades()
        {
            var Grades = _GradeRepository.GetAll().ToList();
            return mapper.Map<List<Grade>, List<GradeModel>>(Grades);
        }
        public List<GradeModel> GetGrades(ActivityModel activityModel, int userId)
        {
            return _GradeRepository.ExecWithStoreProcedure<GradeModel>(Constants.SP_GET_GRADES,
                 new SqlParameter(Constants.SP_PARAM_ACTIVITY_USERID, SqlDbType.Int) { Value = userId },
                 new SqlParameter(Constants.SP_PARAM_ACTIVITY_PROVINCEID, SqlDbType.Int) { Value = activityModel.ProvinceId },
                 new SqlParameter(Constants.SP_PARAM_ACTIVITY_DISTRICTID, SqlDbType.Int) { Value = activityModel.DistrictId },
                 new SqlParameter(Constants.SP_PARAM_ACTIVITY_CIRCUITID, SqlDbType.Int) { Value = activityModel.CircuitId },
                 new SqlParameter(Constants.SP_PARAM_ACTIVITY_SCHOOLID, SqlDbType.Int) { Value = activityModel.SchoolId }
                 ).ToList();
        }

        public void AddGrades(GradeModel gradeData)
        {

            Grade clientModel = mapper.Map<GradeModel, Grade>(gradeData);
            _GradeRepository.Add(clientModel);
            _GradeRepository.SaveChanges();
        }

        public void UpdateGrade(GradeModel grade)
        {
            Grade clientModel = mapper.Map<GradeModel, Grade>(grade);
            _GradeRepository.UpdateChanges(clientModel);
        }

        public GradeModel GetGradeById(int gradeID)
        {
            var record = _GradeRepository.Fetch(x => x.GradeId == gradeID).SingleOrDefault();
            GradeModel clientModel = mapper.Map<Grade, GradeModel>(record);
            return clientModel;
        }

        public GradeModel GetGradeByName(string gradeName)
        {
            var record = _GradeRepository.Fetch(x => x.GradeName.Equals(gradeName)).FirstOrDefault();
            GradeModel clientModel = mapper.Map<Grade, GradeModel>(record);
            return clientModel;

        }

        public string GetGradeNameByGradeID(int gradeId)
        {
            var record = _GradeRepository.Fetch(x => x.GradeId.Equals(gradeId)).FirstOrDefault();
            GradeModel clientModel = mapper.Map<Grade, GradeModel>(record);
            return "Grade " + clientModel.GradeName;
        }

        public void PublishGrade(int gradeID)
        {
            Grade record = _GradeRepository.Fetch(x => x.GradeId == gradeID).SingleOrDefault();
            record.IsPublished = true;
            record.isEnabled = true;
            _GradeRepository.UpdateChanges(record);
        }

        public void DeleteGrade(int gradeID)
        {
            Grade record = _GradeRepository.Fetch(x => x.GradeId == gradeID).SingleOrDefault();            
            _GradeRepository.Delete(record);
            
            _GradeRepository.SaveChanges();
        }

        public bool IsGradePublished(int gradeID)
        {
            bool isGradePublished = _GradeRepository.Fetch(x => x.GradeId == gradeID).Select(x => x.IsPublished).SingleOrDefault();
            return isGradePublished;
        }
        
    }
}
