﻿using AutoMapper;
using EdukiteService.Business.Entities;
using EdukiteService.Business.Interface;
using EdukiteService.Data;
using EdukiteService.Data.interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EdukiteService.Business.Implementation
{
    public class NoteService : INoteService
    {

        private readonly IRepository<Note> _NotesRepository;
        public NoteService(IRepository<Note> NotesRepository)
        {
            _NotesRepository = NotesRepository;
        }


        public NotesResponse AddNote(NoteModel notesModel)
        {
            var note = Mapper.Map<Note>(notesModel);

            using (Edukite_AndroidEntities edukite_AndroidEntities = new Edukite_AndroidEntities())
            {
                edukite_AndroidEntities.Notes.Add(note);
                edukite_AndroidEntities.SaveChanges();
            }

            return Mapper.Map<NotesResponse>(note);
        }

        public bool DeleteNote(NoteModel noteModel)
        {
            var record = _NotesRepository.Fetch(x => x.UserId == noteModel.UserId/* && x.DeviceId == noteModel.DeviceId*/ && x.NoteId == noteModel.NoteId).SingleOrDefault();
            if (record == null) return false;

            _NotesRepository.Delete(record);
            return true;
        }

        public NotesResponse GetNotes(NoteModel notesModel)
        {
            var lists = _NotesRepository.Fetch(x => x.UserId == notesModel.UserId /*&& x.DeviceId == notesModel.DeviceId*/
             && x.TopicId == notesModel.TopicId && x.SubTopicId == notesModel.SubTopicId
             && x.SubjectId == notesModel.SubjectId && x.AssetId == notesModel.AssetId).ToList();

            return Mapper.Map<NotesResponse>(Mapper.Map<IEnumerable<BaseModel>>(lists));


        }
    }
}
