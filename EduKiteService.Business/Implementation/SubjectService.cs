﻿using AutoMapper;
using EdukiteService.Business.Entities;
using EdukiteService.Data;
using EdukiteService.Data.interfaces;
using EdukiteService.Utilities;
using EdukiteService.Business.Interface;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace EdukiteService.Business.Implementation
{
    public class SubjectService : ISubjectService
    {

        public IRepository<Subject> _subjectRepository;
        // public IRepository<Icon> _iconsRepository;
        private readonly IIconsService _iconsService;
        private readonly ITopicService _TopicService;
        public IMapper mapper { get; set; }
        public SubjectService(IRepository<Subject> subjectRepository, IIconsService iconsService, ITopicService topicService)
        {
            _subjectRepository = subjectRepository;
            _iconsService = iconsService;
            _TopicService = topicService;
        }
        //public SubjectService(IRepository<Icon> iconRepository, int i)
        //{
        //    _iconsRepository = iconRepository;

        //}

        public List<SubjectModel> GetSubjects(int gradid)
        {
            using (Edukite_AndroidEntities edukiteEntities = new Edukite_AndroidEntities())
            {
                //var Grades = (from sub in edukiteEntities.Subjects
                //             join Comb in edukiteEntities.SubjectGradeCombinations on
                //             sub.SubjectId equals Comb.SubjectId 
                //             where
                //             Comb.GradeId ==gradid
                //             select new SubjectModel {
                //                 SubjectId=sub.SubjectId,
                //                 SubjectName=sub.SubjectName

                //             }).ToList();

                var Subjects = _subjectRepository.Fetch(x => x.GradeID == gradid).ToList();
                return mapper.Map<List<Subject>, List<SubjectModel>>(Subjects);
            }
        }
        public SubjectDetailsModel GetSubjectDetails(int subjectId)
        {
            SubjectDetailsModel obj = _subjectRepository.ExecWithStoreProcedure<SubjectDetailsModel>(Constants.SP_GETSUBJECTDETAILS,
                  new SqlParameter(Constants.SP_PARAM_ACTIVITY_SUBJECTID, SqlDbType.Int) { Value = subjectId }
                  ).FirstOrDefault();

            var objTopics = _subjectRepository.ExecWithStoreProcedure<TopicsDetailsModel>(Constants.SP_GETTOPICDETAILSBYSUBJECT,
                new SqlParameter(Constants.SP_PARAM_ACTIVITY_SUBJECTID, SqlDbType.Int) { Value = subjectId }
                ).ToList();
            if (objTopics.Count != 0)
            {
                obj.topics = objTopics;
            }
            return obj;
        }

        public List<ThemeModel> GetThemes()
        {
            using (Edukite_AndroidEntities ee = new Edukite_AndroidEntities())
            {
                var themeList = (ee.Themes).ToList();
                List<ThemeModel> tData = mapper.Map<List<Theme>, List<ThemeModel>>(themeList);
                return tData;
            }

        }

        public int AddSubject(SubjectModel subjectData)
        {
            Subject clientModel = mapper.Map<SubjectModel, Subject>(subjectData);
            _subjectRepository.Add(clientModel);
            _subjectRepository.SaveChanges();
            int id = clientModel.SubjectId;
            return id;
        }

        public SubjectModel GetSubjectById(int id)
        {
            var record = _subjectRepository.Fetch(x => x.SubjectId == id).SingleOrDefault();
            SubjectModel clientModel = mapper.Map<Subject, SubjectModel>(record);
            return clientModel;
        }
        
        public string GetSubjectNameById(int id)
        {
            var record = _subjectRepository.Fetch(x => x.SubjectId.Equals(id)).FirstOrDefault();
            SubjectModel clientModel = mapper.Map<Subject, SubjectModel>(record);
            return clientModel.SubjectName;
        }

        public void UpdateSubject(SubjectModel subjectData)
        {
            Subject clientModel = mapper.Map<SubjectModel, Subject>(subjectData);
            _subjectRepository.UpdateChanges(clientModel);
        }

        public void DeleteSubject(int subjectId)
        {
            Subject record = _subjectRepository.Fetch(x => x.SubjectId == subjectId).SingleOrDefault();
            _subjectRepository.Delete(record);
            _subjectRepository.SaveChanges();
        }

        public void DeleteSubjectAll(int gradeId)
        {
            var record = _subjectRepository.Fetch(x => x.GradeID == gradeId).ToList();
            if (record!=null)
            {
                for (int i=0;i<record.Count();i++)
                {
                    _iconsService.DeleteIcon(record[i].SubjectId);
                    _TopicService.DeleteTopicBySubjectId(record[i].SubjectId);
                }
            }
            _subjectRepository.DeleteAll(record);
            _subjectRepository.SaveChanges();
        }

        public void PublishSubject(int subjectID)
        {
            Subject record = _subjectRepository.Fetch(x => x.SubjectId == subjectID).SingleOrDefault();
            record.IsSubjectPublished = true;
            _subjectRepository.UpdateChanges(record);
        }

        public int GetPublishedSubjectCount(int gradeID)
        {
            return _subjectRepository.Fetch(x => x.GradeID == gradeID && x.IsSubjectPublished == false).Count();
        }

        public ThemeModel GetThemesById(int themeId)
        {
            using (Edukite_AndroidEntities ee = new Edukite_AndroidEntities())
            {
                var themeList = (from s in ee.Themes where s.ThemeId == themeId select s).FirstOrDefault<Theme>();
                ThemeModel clientModel = mapper.Map<Theme, ThemeModel>(themeList);
                return clientModel;
            }

        }

        public SubjectModel GetSubjectsByName(string subjectName, int gradeId)
        {
            var record = _subjectRepository.Fetch(x => x.SubjectName == subjectName && x.GradeID==gradeId).SingleOrDefault();
            SubjectModel clientModel = mapper.Map<Subject, SubjectModel>(record);
            return clientModel;
        }
    }
}
