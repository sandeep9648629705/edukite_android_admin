﻿using AutoMapper;
using EdukiteService.Business.Entities;
using EdukiteService.Business.Interface;
using EdukiteService.Data;
using EdukiteService.Data.interfaces;
using EdukiteService.Utilities;
using System;
using System.Collections.Generic;
using System.Data.Entity.Core.Objects;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EdukiteService.Business.Implementation
{
    public class SubscriptionService : ISubscriptionService
    {
        private readonly IRepository<Subscription> _SubscriptionRepository;
        //private readonly IRepository<UserSubjectMapping> _UserSubjectMappingRepository;
        private readonly IRepository<PromoCode> _PromoCodeRepository;
        public IMapper mapper { get; set; }
        public SubscriptionService(IRepository<Subscription> SubscriptionRepository,
            IRepository<PromoCode> PromoCodeRepository)
        {
            _SubscriptionRepository = SubscriptionRepository;
            //_UserSubjectMappingRepository = UserSubjectMappingRepository;
            _PromoCodeRepository = PromoCodeRepository;
        }

        public bool ActivateByCode(int UserId, string DeviceId, string Code)
        {
            var promoCode = _PromoCodeRepository.Fetch(x => x.PromoCode1 == Code && x.ExpiryDate >= DateTime.Now).SingleOrDefault();
            if (promoCode == null)
                return false;

            List<Subscription> subscriptions = null;
            using (Edukite_AndroidEntities edukite_AndroidEntities = new Edukite_AndroidEntities())
            {

                subscriptions = (from s in edukite_AndroidEntities.Subscriptions
                                 join u in edukite_AndroidEntities.Users on s.UserId equals u.UserId
                                 where u.UserId == UserId && u.DeviceId == DeviceId
                                 select s).ToList();
            }
            if (!subscriptions.Any())
                return false;


            foreach (var subscription in subscriptions)
            {
                subscription.PromoCodeId = promoCode.PromoCodeId;
                subscription.EndDatetime = promoCode.ExpiryDate;
                subscription.IsPaid = true;
                _SubscriptionRepository.UpdateChanges(subscription);
            }

            return true;
        }

        public SubscriptionResponse GetSubscriptions(int UserId, string DeviceId)
        {
            using (Edukite_AndroidEntities edukite_AndroidEntities = new Edukite_AndroidEntities())
            {
                IEnumerable<SubscriptionsModel> result = (from s in edukite_AndroidEntities.Subscriptions
                                                          join g in edukite_AndroidEntities.Grades on s.GradeId equals g.GradeId
                                                          join u in edukite_AndroidEntities.Users on s.UserId equals u.UserId
                                                          join st in edukite_AndroidEntities.SubscriptionTypes on s.SubscriptionTypeId equals st.SubscriptionTypeId
                                                          where s.UserId == UserId && u.DeviceId == DeviceId
                                                          select new SubscriptionsModel
                                                          {
                                                              Id = s.SubscriptionId,
                                                              GradeName = g.GradeName,
                                                              DateTime = s.CreatedDateTime,
                                                              DaysLeft = (s.StartDatetime != null && s.EndDatetime != null) ? EntityFunctions.DiffDays(s.StartDatetime, s.EndDatetime) : 0,
                                                              Type = st.SubscriptionType1
                                                          }).ToList();

                return Mapper.Map<SubscriptionResponse>(result);
            }
        }

        public SubscriptionResponse ChooseSubscriptions(int UserId, string DeviceId)
        {
            SubscriptionResponse response = null;
            using (Edukite_AndroidEntities edukite_AndroidEntities = new Edukite_AndroidEntities())
            {
                var subscriptionsModel = (from s in edukite_AndroidEntities.Subscriptions
                                          join g in edukite_AndroidEntities.Grades on s.GradeId equals g.GradeId
                                          join u in edukite_AndroidEntities.Users on s.UserId equals u.UserId
                                          join st in edukite_AndroidEntities.SubscriptionTypes on s.SubscriptionTypeId equals st.SubscriptionTypeId
                                          join sa in edukite_AndroidEntities.SubscriptionAmounts on new { X = s.GradeId ?? 0, Y = s.SubscriptionTypeId ?? 0 } equals new { X = sa.GradeId, Y = sa.SubscriptionTypeId }
                                          where s.UserId == UserId && u.DeviceId == DeviceId
                                          select new
                                          {
                                              Id = s.SubscriptionId,
                                              GradeId = g.GradeId,
                                              GradeName = g.GradeName,
                                              DateTime = s.CreatedDateTime,
                                              DaysLeft = (s.StartDatetime != null && s.EndDatetime != null) ? EntityFunctions.DiffDays(s.StartDatetime, s.EndDatetime) : 0,
                                              Type = st.SubscriptionType1,
                                              SubscriptionTypeId = sa.SubscriptionTypeId,
                                              Price = sa.Price,
                                          }).ToList().Select(Q => new SubscriptionsModel
                                          {
                                              Id = Q.Id,
                                              GradeName = Q.GradeName,
                                              DateTime = Q.DateTime,
                                              DaysLeft = Q.DaysLeft,
                                              Type = string.Format("{0}* {1}", Q.Type, "subscription"),
                                             // Subjects = string.Join(",", _UserSubjectMappingRepository.Fetch(x => x.UserId == UserId && x.GradeId == Q.GradeId).Select(x => x.Subject.SubjectName).ToList()),
                                              Price = Q.SubscriptionTypeId == 1 ? string.Format("R {0}", Q.Price) : string.Format("R1 {0}", Q.Price)
                                          }).ToList();

                response = Mapper.Map<SubscriptionResponse>(subscriptionsModel);
            }

            foreach (var subscription in response.Subscriptions)
            {
                Parallel.Invoke(() =>
                {
                    using (Edukite_AndroidEntities edukite_AndroidEntities = new Edukite_AndroidEntities())
                    {
                        subscription.PromoDiscounts = (from es in edukite_AndroidEntities.ExtendedSubscriptions
                                                       join p in edukite_AndroidEntities.PromoCodes on es.PromoCodeId equals p.PromoCodeId
                                                       join ct in edukite_AndroidEntities.CodeTypes on p.CodeTypeId equals ct.CodeTypeId
                                                       where ct.CodeTypeId == (int)CodeTypes.PROMOCODE && p.ExpiryDate > DateTime.Now
                                                        && es.SubscriptionId == subscription.Id
                                                       select new PromoCodeModel
                                                       {
                                                           Id = p.PromoCodeId,
                                                           Name = p.PromoCode1,
                                                           IsDefault = p.IsDefault ?? false,
                                                           Price = p.Price ?? 0
                                                       }).ToList();
                    }
                }, () =>
                {
                    using (Edukite_AndroidEntities edukite_AndroidEntities = new Edukite_AndroidEntities())
                    {
                        subscription.EdukiteCredits = (from es in edukite_AndroidEntities.ExtendedSubscriptions
                                                       join p in edukite_AndroidEntities.PromoCodes on es.PromoCodeId equals p.PromoCodeId
                                                       join ct in edukite_AndroidEntities.CodeTypes on p.CodeTypeId equals ct.CodeTypeId
                                                       where ct.CodeTypeId == (int)CodeTypes.EDUKITECREDITS && p.ExpiryDate > DateTime.Now
                                                       && es.SubscriptionId == subscription.Id
                                                       select new PromoCodeModel
                                                       {
                                                           Id = p.PromoCodeId,
                                                           Name = p.PromoCode1,
                                                           IsDefault = p.IsDefault ?? false,
                                                           Price = p.Price ?? 0
                                                       }).ToList();
                    }
                });
            }


            return response;
        }
    }
}
