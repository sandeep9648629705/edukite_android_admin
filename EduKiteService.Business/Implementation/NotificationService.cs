﻿using AutoMapper;
using EdukiteService.Business.Entities;
using EdukiteService.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using EdukiteService.Business.Interface;

namespace EdukiteService.Business.Implementation
{
    public class NotificationService : INotificationService
    {
        public NotificationRespose GetNotificationCount(int UserId, string DeviceId)
        {
            using (Edukite_AndroidEntities edukite_AndroidEntities = new Edukite_AndroidEntities())
            {
                var notificationCount = (from n in edukite_AndroidEntities.Notifications
                                         join u in edukite_AndroidEntities.Users on n.UserId equals u.UserId
                                         where u.UserId == UserId && u.DeviceId == DeviceId
                                         select n.Id
                         ).Count();
                return Mapper.Map<NotificationRespose>(notificationCount);
            }
        }
    }
}
