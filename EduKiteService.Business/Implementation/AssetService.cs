﻿using AutoMapper;
using EdukiteService.Business.Entities;
using EdukiteService.Business.Interface;
using EdukiteService.Data;
using EdukiteService.Data.interfaces;

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EdukiteService.Business.Implementation
{
    public class AssetService : IAssetService
    {
        public IRepository<Asset> _assetRespository;
        public IRepository<AssetType> _assetTypeRepository;
        public IRepository<BookmarkAsset> _BookmarkAssetRepository;
        public IRepository<UsageHistory> _UsageHistoryRepository;

        public IMapper mapper { get; set; }
        public AssetService(IRepository<Asset> assetRespository, IRepository<AssetType> assetTypeRepository, IRepository<BookmarkAsset> BookmarkAssetRepository
            , IRepository<UsageHistory> UsageHistoryRepository)
        {
            _assetRespository = assetRespository;
            _assetTypeRepository = assetTypeRepository;
            _BookmarkAssetRepository = BookmarkAssetRepository;
            _UsageHistoryRepository = UsageHistoryRepository;

        }

        public List<AssetTypeModel> GetAssetType()
        {
            using (Edukite_AndroidEntities ee = new Edukite_AndroidEntities())
            {
                var atList = (ee.AssetTypes).ToList();
                List<AssetTypeModel> aData = mapper.Map<List<AssetType>, List<AssetTypeModel>>(atList);
                return aData;
            }
        }

        public void AddAsset(AssetModel assetModel)
        {
            Asset data = mapper.Map<AssetModel, Asset>(assetModel);
            _assetRespository.Add(data);
            _assetRespository.SaveChanges();
        }

        public int AddAssetTypeReturnId(AssetTypeModel assetTypeModel)
        {
            var record = _assetTypeRepository.Fetch(x => x.AssetType1.Equals(assetTypeModel.AssetType1)).FirstOrDefault();
            AssetTypeModel clientModel = mapper.Map<AssetType, AssetTypeModel>(record);
            // return clientModel;
            //AssetType data = mapper.Map<AssetTypeModel, AssetType>(assetTypeModel);
            //_assetTypeRepository.Add(data);
            //_assetTypeRepository.SaveChanges();
            return clientModel.AssetTypeId;
        }

        public void AddAssetList(List<AssetModel> lstassetModel)
        {
            List<Asset> data = mapper.Map<List<AssetModel>, List<Asset>>(lstassetModel);
            _assetRespository.AddRange(data);
            _assetRespository.SaveChanges();
        }

        public string AddAssetInsert(AssetModel assetdata)
        {
            int assetNo=0;
            if (assetdata.SubtopicId !=null)
            {
                 assetNo = _assetRespository.Fetch(x => x.TopicId == assetdata.TopicId && x.SubtopicId==assetdata.SubtopicId).Count();
            }
            else
            {
                assetNo = _assetRespository.Fetch(x => x.TopicId == assetdata.TopicId && x.SubtopicId == null).Count();
            }
            

            if (assetNo >= 0)
            {
                var assetNotoInsert = 1 + assetNo;
                assetdata.AssetNo = assetNotoInsert;
            }

            Asset data = mapper.Map<AssetModel, Asset>(assetdata);
            // data.AssetId = Guid.NewGuid();
            _assetRespository.Add(data);
            _assetRespository.SaveChanges();
            return data.AssetId.ToString();
        }

        public async Task<string> AssetbulkInsertAsync(List<AssetModel> lstassetModel)
        {
            try
            {
                List<Asset> data = mapper.Map<List<AssetModel>, List<Asset>>(lstassetModel);
                _assetRespository.AddRange(data);
                _assetRespository.SaveChangesAsync();
                return "Assets added Successfully!";
            }
            catch(Exception ex)
            {
                return ex.Message;
            }
        }

        public List<AssetModel> GetAssetByTopicId(int topicId)
        {
            var data = _assetRespository.Fetch(x => x.TopicId == topicId).OrderBy(x => x.AssetNo).ToList();
            return mapper.Map<List<Asset>, List<AssetModel>>(data);
        }

        public AssetModel GetAssetByAssetId(Guid AssetId)
        {
            var data = _assetRespository.Fetch(x => x.AssetId == AssetId).SingleOrDefault();
            return mapper.Map<Asset, AssetModel>(data);
        }

        public void UpdateAsset(AssetModel assetModel)
        {

            Asset clientModel = mapper.Map<AssetModel, Asset>(assetModel);
            _assetRespository.UpdateChanges(clientModel);
        }
        public void DeleteAsset(Guid assetId)
        {
            Asset record = _assetRespository.Fetch(x => x.AssetId == assetId).SingleOrDefault();
            _assetRespository.Delete(record);
            _assetRespository.SaveChanges();
        }

        public async Task<string> UpdateSortedAsset(List<AssetModel> lstAssetModel)
        {
            List<Asset> data = mapper.Map<List<AssetModel>, List<Asset>>(lstAssetModel);
            foreach (Asset entity in data)
            {
                var assetData = _assetRespository.Fetch(x => x.AssetId == entity.AssetId).SingleOrDefault();
                assetData.AssetNo = entity.AssetNo;
                await _assetRespository.SaveChangesTaskAsync();
            }
            return "Sorted Topics Updated Successfully!";
        }

        public List<AssetModel> GetAssetByTopicIdWithoutSubTopic(int topicId)
        {
            var data = _assetRespository.Fetch(x => x.TopicId == topicId && x.SubtopicId == null).OrderBy(x => x.AssetNo).ToList();
            return mapper.Map<List<Asset>, List<AssetModel>>(data);
        }

        public List<AssetTypeModel> GetSimIntAssetType()
        {
            using (Edukite_AndroidEntities ee = new Edukite_AndroidEntities())
            {
                var atList = (ee.AssetTypes.Where(p=>p.AssetTypeId==3 || p.AssetTypeId==4)).ToList();
                List<AssetTypeModel> aData = mapper.Map<List<AssetType>, List<AssetTypeModel>>(atList);
                return aData;
            }
        }

        public void DeleteAssetByTopicId(int topicId)
        {
            var record = _assetRespository.Fetch(x => x.TopicId == topicId).ToList();
            _assetRespository.DeleteAll(record);
            _assetRespository.SaveChanges();
        }

        public void DeleteAssetBySubTopicId(int subTopicId)
        {
            var record = _assetRespository.Fetch(x => x.SubtopicId == subTopicId).ToList();
            _assetRespository.DeleteAll(record);
            _assetRespository.SaveChanges();
        }
        public void DeleteBookmarkassetByAssetId(int subTopicId)
        {
            var assetid = _assetRespository.Fetch(x => x.SubtopicId == subTopicId).ToList();
            var assetdata=mapper.Map<List<Asset>, List<AssetModel>>(assetid);
            var record = _BookmarkAssetRepository.Fetch(x => x.AssetId.Equals(assetid)).ToList();
            if (record!=null)
            {
                _BookmarkAssetRepository.DeleteAll(record);
                _BookmarkAssetRepository.SaveChanges();
            }
            else
            {
                _BookmarkAssetRepository.SaveChanges();
            }
           
        }
        public void DeleteUsageHistoryAssetAssetId(int assetid)
        {
            var record = _UsageHistoryRepository.Fetch(x => x.AssetId.Equals(assetid)).ToList();
            if (record != null)
            {
                _UsageHistoryRepository.DeleteAll(record);
                _UsageHistoryRepository.SaveChanges();
            }
            else
            {
                _UsageHistoryRepository.SaveChanges();
            }
        }
    }
}
