﻿using AutoMapper;
using EdukiteService.Business.Entities;
using EdukiteService.Data;
using EdukiteService.Business.Interface;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EdukiteService.Business.Implementation
{
    public class BookMarkService : IBookMarkService
    {
        static readonly string[] SizeSuffixes = { "bytes", "KB", "MB", "GB", "TB", "PB", "EB", "ZB", "YB" };

        public BookMarksResponse GetBookmarks(BooKMarkRequest bookMark)
        {
            using (Edukite_AndroidEntities edukite_AndroidEntities = new Edukite_AndroidEntities())
            {
                var Query1 = from B in edukite_AndroidEntities.BookmarkAssets
                             join A in edukite_AndroidEntities.Assets on new { X = B.AssetId, Y = B.Version } equals new { X = A.AssetId, Y = A.Version }
                             join UH in edukite_AndroidEntities.UsageHistories on A.AssetId equals UH.AssetId into BookMarkAssets
                             from A2 in BookMarkAssets.DefaultIfEmpty()
                             group A2 by new { AssetId = B.AssetId, BookmarkAssetId = B.BookmarkAssetId, UserId = B.UserId, Version = B.Version } into grouped
                             select new
                             {
                                 ID = grouped.Key.BookmarkAssetId,
                                 AssetId = grouped.Key.AssetId,
                                 UserID = grouped.Key.UserId,
                                 IsCompleted = grouped.Sum(x => x.AssetId != null ? 1 : 0),
                                 Version = grouped.Key.Version
                             };

                var results = (from A2 in Query1
                               join A1 in edukite_AndroidEntities.Assets on new { X = A2.AssetId, Y = A2.Version } equals new { X = A1.AssetId, Y = A1.Version }
                               join T in edukite_AndroidEntities.Topics on A1.TopicId equals T.TopicId
                               join ST in edukite_AndroidEntities.Subtopics on T.TopicId equals ST.TopicId
                               join U in edukite_AndroidEntities.Users on A2.UserID equals U.UserId
                               join G in edukite_AndroidEntities.Grades on U.GradeID equals G.GradeId
                               where U.UserId == bookMark.userId && U.DeviceId == bookMark.deviceId
                               select new BookMarksModel
                               {
                                   Id = A2.ID,
                                   TopicName = T.TopicName,
                                   SubTopicName = ST.SubtopicName,
                                   GradeName = G.GradeName,
                                   AssetName = A1.AssetName,
                                   AssetPath = A1.FolderName + "/" + A1.FileName,
                                   IsCompleted = A2.IsCompleted > 0,
                                   FileSize = A1.AssetLength != null ? A1.AssetLength.ToString() : null
                               }).OrderBy(x => x.Id).Skip((bookMark.PageNo - 1) * bookMark.PageSize).Take(bookMark.PageSize).ToList();


                foreach (var items in results)
                {
                    items.AssetPath = string.Format("{0}{1}", ConfigurationManager.AppSettings["DomainURL"], items.AssetPath);
                    items.FileSize = SizeSuffix(items.FileSize != null ? Convert.ToInt64(items.FileSize) : 0, 1);
                }

                return Mapper.Map<BookMarksResponse>(results);
            }

        }

        /// <summary>
        /// CONVERT INTO KB,MB,GB
        /// </summary>
        /// <param name="val"></param>
        /// <param name="decimalPlaces"></param>
        /// <returns></returns>
        private string SizeSuffix(Int64? val, int decimalPlaces = 1)
        {
            if (val == null) return "";
            Int64 value = (Int64)val;


            if (value == 0) { return string.Format("{0:n" + decimalPlaces + "} bytes", 0); }

            // mag is 0 for bytes, 1 for KB, 2, for MB, etc.
            int mag = (int)Math.Log(value, 1024);

            // 1L << (mag * 10) == 2 ^ (10 * mag) 
            // [i.e. the number of bytes in the unit corresponding to mag]
            decimal adjustedSize = (decimal)value / (1L << (mag * 10));

            // make adjustment when the value is large enough that
            // it would round up to 1000 or more
            if (Math.Round(adjustedSize, decimalPlaces) >= 1000)
            {
                mag += 1;
                adjustedSize /= 1024;
            }

            return string.Format("{0:n" + decimalPlaces + "} {1}",
                adjustedSize,
                SizeSuffixes[mag]);
        }      

    }
}