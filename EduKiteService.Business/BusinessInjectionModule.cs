﻿using Autofac;
using Module = Autofac.Module;
using System.Data.Entity;
using AutoMapper;
using System.Collections.Generic;
using EdukiteService.Data;
using EdukiteService.Data.implementations;
using EdukiteService.Data.interfaces;
using EdukiteService.Business.Implementation;
using EdukiteService.Business.Interface;
using EduKiteService.Business.Interface;
using EduKiteService.Business.Implementation;

namespace Edukite.Bussiness
{
    public class BusinessInjectionModule : Module
    {
        protected override void Load(ContainerBuilder builder)
        {
            AutoMapperConfiguration.RegisterProfiles(builder);
            builder.Register(a => new Edukite_AndroidEntities()).InstancePerRequest();
            builder.RegisterGeneric(typeof(EFRepository<>)).As(typeof(IRepository<>)).InstancePerRequest();
            builder.RegisterType<UserService>().As<IUserService>().InstancePerRequest().PropertiesAutowired();
            builder.RegisterType<MasterDataService>().As<IMasterDataService>().InstancePerRequest().PropertiesAutowired();
            builder.RegisterType<GradeService>().As<IGradeService>().InstancePerRequest().PropertiesAutowired();
            builder.RegisterType<ProvinceService>().As<IProvinceService>().InstancePerRequest().PropertiesAutowired();
            builder.RegisterType<SubjectService>().As<ISubjectService>().InstancePerRequest().PropertiesAutowired();
            builder.RegisterType<TopicService>().As<ITopicService>().InstancePerRequest().PropertiesAutowired();
            builder.RegisterType<SubTopicService>().As<ISubTopicService>().InstancePerRequest().PropertiesAutowired();
            builder.RegisterType<AssetService>().As<IAssetService>().InstancePerRequest().PropertiesAutowired();
            builder.RegisterType<ModuleService>().As<IModuleService>().InstancePerRequest().PropertiesAutowired();
            builder.RegisterType<VideoCommentService>().As<IVideoCommentService>().InstancePerRequest();
            builder.RegisterType<NoteService>().As<INoteService>().InstancePerRequest().PropertiesAutowired();
            // builder.RegisterType<SubscriptionService>().As<ISubscriptionService>().InstancePerRequest().PropertiesAutowired();
            builder.RegisterType<BookMarkService>().As<IBookMarkService>().InstancePerRequest().PropertiesAutowired();
            builder.RegisterType<NotificationService>().As<INotificationService>().InstancePerRequest().PropertiesAutowired();
            builder.RegisterType<UsersServices>().As<IUsersServices>().InstancePerRequest().PropertiesAutowired();
            builder.RegisterType<ExamPaperService>().As<IExamPaperService>().InstancePerRequest().PropertiesAutowired();
        }
    }


    internal class AutoMapperConfiguration
    {
        internal static void RegisterProfiles(ContainerBuilder builder)
        {
            builder.RegisterAssemblyTypes(typeof(AutoMapperConfiguration).Assembly).As<Profile>();

            builder.Register(context => new MapperConfiguration(cfg =>
            {
                var pro = context.Resolve<IEnumerable<Profile>>();
                foreach (var profile in context.Resolve<IEnumerable<Profile>>())
                {
                    cfg.AddProfile(profile);
                }
            })).AsSelf().SingleInstance();

            builder.Register(c => c.Resolve<MapperConfiguration>().CreateMapper(c.Resolve))
                .As<IMapper>()
                .InstancePerLifetimeScope();


        }
    }
}