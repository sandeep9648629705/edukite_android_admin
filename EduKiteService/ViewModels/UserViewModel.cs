﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace EduKiteService.ViewModels
{
    public class UserViewModel
    {
       
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string EmailId { get; set; }
        public string ContactNo { get; set; }
        public int? ProvinceId { get; set; }
        public int? GradeID { get; set; }
        public int UserTypeId { get; set; }
        public int? SchoolId { get; set; }
        public int? Gender { get; set; }
        public DateTime? dob { get; set; }
        public int? TelephoneNo { get; set; }
        public string City { get; set; }
        public int? FavSubID { get; set; }
        public string P_Name { get; set; }
        public string P_EmailId { get; set; }
        public int? P_CellPhoneNo { get; set; }
        public int? P_TelePhoneNo { get; set; }
        public int UserGroup { get; set; }
        public int? Package { get; set; }
        public string CellphoneCode { get; set; }
        public string TelephoneCode { get; set; }
        public string P_CellphoneCode { get; set; }
        public string P_TelephoneCode { get; set; }
       // Subscription column
        public int UserId { get; set; }
        public string UserGroupID { get; set; }
        public DateTime SubscribedDatetime { get; set; }
        public int? PromoCodeId { get; set; }
        public int? SubscriptionTypeId { get; set; }
        public DateTime StartDatetime { get; set; }
        public DateTime EndDatetime { get; set; }
        public string IsPaid { get; set; }
        public DateTime CreatedDateTime { get; set; }
    }
}