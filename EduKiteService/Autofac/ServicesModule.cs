﻿using Autofac;
using Module = Autofac.Module;
using EdukiteService.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using EdukiteService.Data.implementations;
using EdukiteService.Data.interfaces;
using EdukiteService.Business.Implementation;
using EdukiteService.Business.Interface;


namespace EdukiteService.Business.Autofac
{
    public class ServicesModule : Module
    {
        protected override void Load(ContainerBuilder builder)
        {
            builder.Register(a => new Edukite_AndroidEntities()).InstancePerRequest();
            builder.RegisterGeneric(typeof(EFRepository<>)).As(typeof(IRepository<>)).InstancePerRequest();
            builder.RegisterType<VideoCommentService>().As<IVideoCommentService>().InstancePerRequest();
            builder.RegisterType<NoteService>().As<INoteService>().InstancePerRequest();
          //  builder.RegisterType<SubscriptionService>().As<ISubscriptionService>().InstancePerRequest();
            builder.RegisterType<BookMarkService>().As<IBookMarkService>().InstancePerRequest();
            builder.RegisterType<NotificationService>().As<INotificationService>().InstancePerRequest();
            builder.RegisterType<GradeService>().As<IGradeService>().InstancePerRequest();
            builder.RegisterType<IconService>().As<IIconsService>().InstancePerRequest();
            
        }
    }
}
