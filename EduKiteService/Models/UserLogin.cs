﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace EduKiteService.Models
{
    public class UserLogin
    {
        public string status { get; set; }
        public string statusMessage { get; set; }
        public string OTP { get; set; }
        public string userId { get; set; }
    }
}