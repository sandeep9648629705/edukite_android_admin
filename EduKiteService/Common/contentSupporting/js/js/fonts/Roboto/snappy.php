<?php

return array(


    'pdf' => array(
        'enabled' => true,
        'binary'  => base_path('vendor/wemersonjanuario/wkhtmltopdf-windows/bin/32bit/wkhtmltopdf.exe'),//'/usr/local/bin/wkhtmltopdf',  for windows
        //'binary'  => base_path('vendor/h4cc/wkhtmltopdf-amd64/bin/wkhtmltopdf'),//'/usr/local/bin/wkhtmltopdf' for linux,
       // 'binary'  =>   base_path('vendor/h4cc/wkhtmltopdf-amd64/bin/wkhtmltopdf'),//'/usr/local/bin/wkhtmltopdf',  for mac
        'timeout' => false,
        'options' => array(),
        'env'     => array(),
    ),
    'image' => array(
        'enabled' => true,
        'binary'  => base_path('vendor/wemersonjanuario/wkhtmltopdf-windows/bin/32bit/wkhtmltoimage.exe'),//'/usr/local/bin/wkhtmltoimage', for windows
        //'binary'  => base_path('vendor/h4cc/wkhtmltoimage-amd64/bin/wkhtmltoimage-amd64'),//'/usr/local/bin/wkhtmltoimage', for linux
        //'binary'  => base_path('vendor/h4cc/wkhtmltoimage-amd64/bin/wkhtmltoimage'),//'/usr/local/bin/wkhtmltoimage', for mac
        'timeout' => false,
        'options' => array(),
        'env'     => array(),
    ),


);
