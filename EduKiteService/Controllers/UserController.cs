﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using EdukiteService.Business;
using EdukiteService.Business.Interface;
using EdukiteService.Utilities;
using EdukiteService.Data;
using EdukiteService.Business.Entities;

namespace EduKiteService.Controllers
{
    public class UserController : ApiController
    {
        public readonly IUserService _userService;
        public readonly IGradeService _gradeService;
        public UserController(IUserService userService, IGradeService gradeService)
        {
            _userService = userService;
            _gradeService = gradeService;
        }
        Dictionary<string, string> keyValuePairs;

        [HttpPost]
        [ActionName("Login")]
        public Models.jsonResponse Login(JObject jInput)
        {
            User ipUser = JsonConvert.DeserializeObject<User>(jInput.ToString());
            keyValuePairs = new Dictionary<string, string>();

            try
            {
                using (var context = new Edukite_AndroidEntities())
                {
                    IEnumerable<User> lstUser = from user in context.Users
                                                where user.ContactNo == ipUser.ContactNo
                                                orderby user.ContactNo
                                                select user;
                    if (lstUser.Count() > 0)
                    {
                        keyValuePairs.Add("OTP", new UserAuthentication().GenerateOTP(lstUser.FirstOrDefault().ContactNo));
                        keyValuePairs.Add("userId", lstUser.FirstOrDefault().UserId.ToString());
                        return new Models.jsonResponse() { status = 200, statusMessage = "Success", data = keyValuePairs };
                    }
                    else
                    {
                        return new Models.jsonResponse() { status = 400, statusMessage = "Failure" };
                    }
                }
            }
            catch (Exception ex)
            {
                return new Models.jsonResponse() { status = 400, statusMessage = "Failure: " + ex.Message };
            }
        }

        [HttpPost]
        [ActionName("GenerateOTP")]
        public Models.jsonResponse GenerateOTP(JObject jInput)
        {
            User ipUser = JsonConvert.DeserializeObject<User>(jInput.ToString());
            keyValuePairs = new Dictionary<string, string>();

            try
            {
                keyValuePairs.Add("OTP", new UserAuthentication().GenerateOTP(ipUser.ContactNo));
                return new Models.jsonResponse() { status = 200, statusMessage = "Success", data = keyValuePairs };
            }
            catch (Exception ex)
            {
                return new Models.jsonResponse() { status = 400, statusMessage = "Failure: " + ex.Message };
            }

        }

        [HttpPost]
        [ActionName("Register")]
        public Models.jsonResponse Register(JObject jInput)
        {
            User ipUser = JsonConvert.DeserializeObject<User>(jInput.ToString());
            keyValuePairs = new Dictionary<string, string>();

            try
            {
                using (var context = new Edukite_AndroidEntities())
                {
                    IEnumerable<User> lstUser = from user in context.Users
                                                where user.EmailId == ipUser.EmailId
                                                orderby user.EmailId
                                                select user;
                    if (lstUser.Count() > 0)
                    {
                        return new Models.jsonResponse() { status = 400, statusMessage = "User already exists." };
                    }
                    else
                    {
                        context.Users.Add(ipUser);
                        context.SaveChanges();

                        lstUser = from user in context.Users
                                  where user.EmailId == ipUser.EmailId
                                  orderby user.EmailId
                                  select user;

                        keyValuePairs.Add("userId", lstUser.FirstOrDefault().UserId.ToString());

                        return new Models.jsonResponse() { status = 200, statusMessage = "Success", data = keyValuePairs };
                    }
                }
            }
            catch (Exception ex)
            {
                return new Models.jsonResponse() { status = 400, statusMessage = "Failure: " + ex.Message };
            }

        }

        [HttpPost]
        [ActionName("InviteCode")]
        public Models.jsonResponse InviteCode(JObject jInput)
        {
            User ipUser = JsonConvert.DeserializeObject<User>(jInput.ToString());

            try
            {
                using (var context = new Edukite_AndroidEntities())
                {
                    var result = context.Users.SingleOrDefault(b => b.UserId == ipUser.UserId);
                    if (result != null)
                    {
                        result.InviteCode = ipUser.InviteCode;
                        context.SaveChanges();
                    }
                }

                return new Models.jsonResponse() { status = 200, statusMessage = "Success" };
            }
            catch (Exception ex)
            {
                return new Models.jsonResponse() { status = 400, statusMessage = "Failure: " + ex.Message };
            }
        }

        [HttpPost]
        [ActionName("RegisterNewDevice")]
        public Models.jsonResponse RegisterNewDevice(JObject jInput)
        {
            User ipUser = JsonConvert.DeserializeObject<User>(jInput.ToString());
            keyValuePairs = new Dictionary<string, string>();

            try
            {
                using (var context = new Edukite_AndroidEntities())
                {
                    var result = context.Users.SingleOrDefault(b => b.UserId == ipUser.UserId);
                    if (result != null)
                    {
                        result.DeviceId = ipUser.DeviceId;
                        context.SaveChanges();
                    }
                }

                return new Models.jsonResponse() { status = 200, statusMessage = "Success" };
            }
            catch (Exception ex)
            {
                return new Models.jsonResponse() { status = 400, statusMessage = "Failure: " + ex.Message };
            }
        }
        [HttpGet]
        [Route("api/user/GetUserDetails")]
        public HttpResponseMessage GetUserDetails(UserModel use)
        {
            var user = _userService.getUserDetails(use.UserId);
            return Request.CreateResponse(HttpStatusCode.OK, new APIMessage { StatusCode = (int)HttpStatusCode.OK, StatusMessage = Constants.SuccessMessage, Data = user });
        }
        [HttpPost]
        [Route("api/user/UpdateUserDetails")]
        public HttpResponseMessage UpdateUserDetails(UsersMode use)
        {
            bool user = _userService.UpdateUserDetails(use);
            return Request.CreateResponse(HttpStatusCode.OK, new APIMessage { StatusCode = (int)HttpStatusCode.OK, StatusMessage = Constants.SuccessMessage });
        }

    }
}
