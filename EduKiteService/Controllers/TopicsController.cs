﻿using EdukiteService.Business.Entities;
using EdukiteService.Business.Interface;
using System;
using System.Collections.Generic;
using System.Web;
using System.Web.Mvc;
using System.IO;
using System.Linq;
using System.Xml;
using System.Threading.Tasks;
using System.Threading;
using Microsoft.AspNet.SignalR;
using System.IO.Compression;


namespace EduKiteService.Controllers
{
    public class TopicsController : Controller
    {
        private readonly ITopicService _TopicService;
        private readonly ISubTopicService _SubTopicService;
        private readonly IGradeService _GradeService;
        private readonly ISubjectService _SubjectService;
        private readonly IAssetService _AssetService;
        private readonly IModuleService _ModuleService;

        public TopicsController(ITopicService topicService, IGradeService gradeService, ISubTopicService subTopicService, ISubjectService subjectService, IAssetService assetService, IModuleService moduleService)//
        {
            _TopicService = topicService;
            _GradeService = gradeService;
            _SubTopicService = subTopicService;
            _SubjectService = subjectService;
            _AssetService = assetService;
            _ModuleService = moduleService;
        }

        // GET: Topics
        public ActionResult Index(int GradeId, int SubId)
        {
            try
            {
                TopicInitialization(GradeId, SubId);
                bool IsBulkAddingNeeded = false;
                TempData["tmpGradeIdSubjectId"] = GradeId + "~" + SubId;
                var lstmodelTopics = _TopicService.GetTopicsBySubjcectID(SubId);
                if (lstmodelTopics != null && lstmodelTopics.Count > 0)
                {
                    IsBulkAddingNeeded = false;
                }
                else
                { IsBulkAddingNeeded = true; }
                ViewBag.IsBulkAddingNeeded = IsBulkAddingNeeded;

            }
            catch (Exception ex )
            {

                throw ex;
            }

            return View();
        }

        // GET: Topics/Details/5
        public ActionResult Details(int id)
        {
            return View();
        }

        [HttpGet]
        // GET: Topics/Create
        public ActionResult Create(int SubId, int GradeID)
        {
            try
            {
                TempData["tmpGradeIdSubjectId"] = GradeID + "~" + SubId;
                TempData.Keep();
                TopicInitialization(GradeID, SubId);
            }
            catch (Exception)
            {
                throw;
            }
            return View();
        }

        // POST: Topics/Create
        [HttpPost]
        public ActionResult Create(TopicModel topicModel)
        {

            try
            {
                int GradeId = 0; int SubId = 0;
                //if (TempData["tmpGradeIdSubjectId"] != null)
                //{
                //    string[] gradeIdSubId = TempData["tmpGradeIdSubjectId"].ToString().Split('~');
                //    GradeId = Convert.ToInt32(gradeIdSubId[0]);
                //    SubId = Convert.ToInt32(gradeIdSubId[1]);
                //}

                if (Request.Form["hdnGradeIdSubID"] != null)
                {
                    string[] gradeIdSubId = Request.Form["hdnGradeIdSubID"].ToString().Split('~');
                    GradeId = Convert.ToInt32(gradeIdSubId[0]);
                    SubId = Convert.ToInt32(gradeIdSubId[1]);
                }

                if (topicModel != null)
                {
                    var addTopic = new TopicModel();
                    addTopic.TopicName = topicModel.TopicName;
                    addTopic.TextIcon = topicModel.TextIcon;
                    addTopic.Description = topicModel.Description;
                    addTopic.SubjectId = SubId;
                    _TopicService.AddTopic(addTopic);
                }

                //    http://localhost:30888/topics/index?GradeId=60&SubId=25
                // TODO: Add insert logic here
                return RedirectToAction("Index", "Topics", new { @GradeId = GradeId, @SubId = SubId });
            }
            catch (Exception ex)
            {
                throw ex;
            }

        }

        // GET: Topics/Edit/5
        [HttpGet]
        public ActionResult Edit(int id, int SubId, int GradeID)
        {
            TopicModel topicModel = new TopicModel();
            try
            {

                TempData["tmpGradeIdSubjectId"] = GradeID + "~" + SubId;
                TempData.Keep();
                TopicInitialization(GradeID, SubId);
                topicModel = _TopicService.GetTopicById(id);
            }
            catch (Exception)
            {
                throw;
            }
            return View(topicModel);
        }

        // POST: Topics/Edit/5
        [HttpPost]
        public ActionResult Edit(TopicModel topicModel)
        {
            int GradeId = 0; int SubId = 0;
            try
            {
                //if (TempData["tmpGradeIdSubjectId"] != null)
                //{
                //    string[] gradeIdSubId = TempData["tmpGradeIdSubjectId"].ToString().Split('~');
                //    GradeId = Convert.ToInt32(gradeIdSubId[0]);
                //    SubId = Convert.ToInt32(gradeIdSubId[1]);
                //}

                if (Request.Form["hdnGradeIdSubID"] != null)
                {
                    string[] gradeIdSubId = Request.Form["hdnGradeIdSubID"].ToString().Split('~');
                    GradeId = Convert.ToInt32(gradeIdSubId[0]);
                    SubId = Convert.ToInt32(gradeIdSubId[1]);
                }
                _TopicService.UpdateTopic(topicModel);
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return RedirectToAction("Index", "Topics", new { @GradeId = GradeId, @SubId = SubId });
        }

        // POST: Topics/Delete/5
        public ActionResult Delete(int id)
        {
            int GradeId = 0; int SubId = 0;
            try
            {
                if (TempData["tmpGradeIdSubjectId"] != null)
                {
                    string[] gradeIdSubId = TempData["tmpGradeIdSubjectId"].ToString().Split('~');
                    GradeId = Convert.ToInt32(gradeIdSubId[0]);
                    SubId = Convert.ToInt32(gradeIdSubId[1]);
                }
                _TopicService.DeleteTopic(id);
            }
            catch (Exception)
            {
                throw;
            }
            // TODO: Add delete logic here
            return RedirectToAction("Index", "Topics", new { @GradeId = GradeId, @SubId = SubId });
        }

     //-------------------------------------RikArena----------------------------------------------------//
        [HttpPost]
        public async Task<ActionResult> BulkAdding(AssetModel assetModelCombi)
        {
            try
            {
                var hubContext = GlobalHost.ConnectionManager.GetHubContext<BulkaddingHub>();
                int GradeId = 0; int SubId = 0;
                if (TempData["tmpGradeIdSubjectId"] != null)
                {
                    string[] gradeIdSubId = TempData["tmpGradeIdSubjectId"].ToString().Split('~');
                    GradeId = Convert.ToInt32(gradeIdSubId[0]);
                    SubId = Convert.ToInt32(gradeIdSubId[1]);
                }
                HttpPostedFileBase file = assetModelCombi.PostedFile;
                if (file == null)
                {
                    ModelState.AddModelError("File", "Please Upload Your file");
                }
                else if (file.ContentLength > 0)
                {
                    string[] AllowedFileExtensions = new string[] { ".zip" };

                    if (!AllowedFileExtensions.Contains(file.FileName.Substring(file.FileName.LastIndexOf('.'))))
                    {
                        ModelState.AddModelError("", "Please file of type: " + string.Join(", ", AllowedFileExtensions));
                    }
                    string SuccMsg = await BulkAddingZIP(file, GradeId, SubId, hubContext);

                }

                return RedirectToAction("Index", "Topics", new { @GradeId = GradeId, @SubId = SubId });
            }
            catch (Exception ex)
            {
                throw ex;
            }

        }
        private async Task<string> BulkAddingZIP(HttpPostedFileBase file, int GradeId, int SubId, IHubContext hubContext)
        {
            try
            {
                int itemsCount = 0;
                var percentage = 0;
                long TotalCoreFileSize = 0;
                int assetFileCountforPerc = 0;

                #region size of upload directory 
                var fileName = Path.GetFileName(file.FileName);
                string fileuploadpath = file.FileName;
                String path = Server.MapPath("~/Content/");
                #region copy core folder 
                //prepare directory to copy for Zip File 
                if (!Directory.Exists(path))
                {
                    Directory.CreateDirectory(path);
                }
                hubContext.Clients.All.AddProgress("Reading Files....", percentage + "%");
                string FolderPathCopyAssetCore = fileuploadpath.Replace(fileName, "") + "\\core\\";
                if (!Directory.Exists(FolderPathCopyAssetCore))
                {
                    Directory.CreateDirectory(FolderPathCopyAssetCore);
                }
                if (Directory.Exists(FolderPathCopyAssetCore))
                {
                    DirectoryInfo diSource = new DirectoryInfo(FolderPathCopyAssetCore);
                    long sizeOfSourceCoreDir = DirectorySize(diSource, true);

                    long inMBSs = ((sizeOfSourceCoreDir) / (1024 * 1024));
                    itemsCount = Convert.ToInt32(inMBSs);
                    TotalCoreFileSize = inMBSs;

                    //update msg in view 
                    hubContext.Clients.All.AddProgress("(0 MB of " + TotalCoreFileSize + " MB added.)", percentage + "%");
                    DirectoryInfo diTarget = new DirectoryInfo(path);

                    var assetZIPpath = Path.Combine(Server.MapPath("~/Content/"), fileName);
                    assetZIPpath = Path.Combine(path, fileName);
                    hubContext.Clients.All.AddProgress("Uploading Files....", percentage + "%");
                    //save ZIP file
                    if (System.IO.File.Exists(assetZIPpath))
                    {
                        System.IO.File.Delete(assetZIPpath);
                    }
                    else
                    {
                        file.SaveAs(assetZIPpath);
                    }
                                   
                    hubContext.Clients.All.AddProgress("File Uploaded....", "100" + "%");
                    //Extract Zip File 
                    string folder = fileName.Replace(".zip", "");
                    string assetzipfile = Path.Combine(path, folder);
                    if (Directory.Exists(assetzipfile))
                    {
                        DeleteDirectory(assetzipfile);
                        Directory.CreateDirectory(assetzipfile);
                    }
                    else
                    {
                        Directory.CreateDirectory(assetzipfile);
                    }
                    hubContext.Clients.All.AddProgress("Zip Files is extracting....", percentage + "%");
                    ZipFile.ExtractToDirectory(Server.MapPath("~/Content/" + fileName), Server.MapPath("~/Content/"+ folder+"/"));
                    hubContext.Clients.All.AddProgress("Zip Files is extracted ....", "100" + "%");
                                   
                    #region read from asset.xml and insert data to db
                    List<AssetModel> AssetLst = new List<AssetModel>();
                    XmlDocument doc = new XmlDocument();
                    hubContext.Clients.All.AddProgress("XML file reading ....", percentage + "%");
                    doc.Load(Server.MapPath("~/Content/" + folder + "/" + "Asset.xml"));
                    foreach (XmlNode node in doc.SelectNodes("/NewDataSet/dtAsset"))
                    {
                        //Fetch the Node values and assign it to Model.
                        AssetModel assetCombinationModel = new AssetModel();
                        if (IsNodeExist(node, "AssetId"))
                        {
                            assetCombinationModel.AssetId = Guid.Parse(node["AssetId"].InnerText);
                            if (IsNodeExist(node, "TermNo"))
                            {
                                var termNo = string.IsNullOrEmpty(node["TermNo"].InnerText) ? 0 : Convert.ToInt32(node["TermNo"].InnerText);
                                assetCombinationModel.TermNo = Convert.ToInt32(termNo);
                            }
                            if (IsNodeExist(node, "WeekNo"))
                            {
                                var weekNo = string.IsNullOrEmpty(node["WeekNo"].InnerText) ? 0 : Convert.ToInt32(node["WeekNo"].InnerText);
                                assetCombinationModel.WeekNo = Convert.ToInt32(weekNo);
                            }

                            TopicModel topicModel = new TopicModel();
                            if (IsNodeExist(node, "TopicNo"))
                            {
                                var topicNo = string.IsNullOrEmpty(node["TopicNo"].InnerText) ? 0 : Convert.ToInt32(node["TopicNo"].InnerText);
                                topicModel.TopicNo = Convert.ToInt32(topicNo);
                                //   assetCombinationModel.topicModelInAsset.TopicNo = Convert.ToInt32(node["TopicNo"].InnerText);
                            }
                            if (IsNodeExist(node, "TopicName"))
                            {
                                //assetCombinationModel.topicModelInAsset.TopicName = node["TopicName"].InnerText;
                                topicModel.TopicName = node["TopicName"].InnerText;
                            }
                            assetCombinationModel.topicModelInAsset = topicModel;

                            SubTopicModel subTopicModel = new SubTopicModel();
                            if (IsNodeExist(node, "SubtopicNo"))
                              {
                                var stNo = string.IsNullOrEmpty(node["SubtopicNo"].InnerText) ? 0 : Convert.ToInt32(node["SubtopicNo"].InnerText);
                                subTopicModel.SubtopicNo = Convert.ToInt32(stNo);
                              }
                            if (IsNodeExist(node, "SubtopicName"))
                            {
                                subTopicModel.SubtopicName = node["SubtopicName"].InnerText;
                            }
                            else
                            {
                                subTopicModel.SubtopicName = string.Empty;
                            }
                            assetCombinationModel.subtopicModelInAsset = subTopicModel;

                            if (IsNodeExist(node, "AssetNo"))
                            {
                                var assetNo = string.IsNullOrEmpty(node["AssetNo"].InnerText) ? 0 : Convert.ToInt32(node["AssetNo"].InnerText);
                                assetCombinationModel.AssetNo = Convert.ToInt32(assetNo);
                            }
                            if (IsNodeExist(node, "AssetName"))
                                assetCombinationModel.AssetName = node["AssetName"].InnerText;

                            AssetTypeModel assetTypeModel = new AssetTypeModel();
                            if (IsNodeExist(node, "AssetType"))
                            {
                                assetTypeModel.AssetType1 = node["AssetType"].InnerText;
                                assetCombinationModel.assetTypeModelInAsset = assetTypeModel;
                            }
                            if (IsNodeExist(node, "FileName"))
                                assetCombinationModel.FileName = node["FileName"].InnerText;
                            if (IsNodeExist(node, "Description"))
                                assetCombinationModel.Description = node["Description"].InnerText;
                            if (IsNodeExist(node, "AssetLength"))
                            {
                                var assetLength = string.IsNullOrEmpty(node["AssetLength"].InnerText) ? 0 : Convert.ToInt32(node["AssetLength"].InnerText);
                                assetCombinationModel.AssetLength = Convert.ToInt32(assetLength);
                            }
                            if (IsNodeExist(node, "SuggestedActiveDuration"))
                            {
                                var suggestedActiveDuration = string.IsNullOrEmpty(node["SuggestedActiveDuration"].InnerText) ? 0 : Convert.ToInt32(node["SuggestedActiveDuration"].InnerText);
                                assetCombinationModel.SuggestedActiveDuration = Convert.ToInt32(suggestedActiveDuration);
                            }
                            if (IsNodeExist(node, "FolderName"))
                                assetCombinationModel.FolderName = node["FolderName"].InnerText;
                            if (IsNodeExist(node, "ThumbnailFileName"))
                                assetCombinationModel.ThumbnailFileName = node["ThumbnailFileName"].InnerText;
                            if (IsNodeExist(node, "Version"))
                            {
                                var version = string.IsNullOrEmpty(node["Version"].InnerText) ? 0 : Convert.ToInt32(node["Version"].InnerText);
                                assetCombinationModel.Version = Convert.ToInt32(version);
                            }
                            if (IsNodeExist(node, "isAssetHide"))
                            {
                                assetCombinationModel.isAssetHide = true;
                            }
                            else
                            {
                                assetCombinationModel.isAssetHide = true;
                            }

                            ModuleModel moduleModel = new ModuleModel();
                            if (IsNodeExist(node, "ModuleName"))
                            {
                                moduleModel.ModuleName = node["ModuleName"].InnerText;
                                moduleModel.CreatedDatime = DateTime.Now;
                                moduleModel.GradeId = GradeId;
                                moduleModel.SubjectId = SubId;
                                
                            }
                            moduleModel.FolderPath = "C:/HostingSpaces/kft706/EWEB/Content/" + folder;
                            moduleModel.IsEncrypted = false;

                            assetCombinationModel.moduleModelInAsset = moduleModel;
                        }
                        AssetLst.Add(assetCombinationModel);
                    }

                    //total item count ie: asset list count + core folder size in MB
                    itemsCount = itemsCount + AssetLst.Count;

                    List<AssetModel> lstassetModel = new List<AssetModel>();
                    //List<ModuleModel> lstmoduleModel = new List<ModuleModel>();
                    foreach (var assetItem in AssetLst)
                    {
                        int TopicID = 0; int SubTopicID = 0; int AssetTypeID = 0;
                        //insert to Topic Table
                        if (assetItem.topicModelInAsset != null)
                        {

                            assetItem.topicModelInAsset.SubjectId = SubId;
                            //save to db
                            //TopicID = _TopicService.AddTopicRetID(assetItem.topicModelInAsset);

                            Task<int> TopicIDTsk = new Task<int>(() => _TopicService.AddTopicRetID(assetItem.topicModelInAsset));
                            TopicIDTsk.Start();

                            // ViewBag.TopicInsertMsg = "Fetching Topic from xml....";
                            TopicID = await TopicIDTsk;
                            // ViewBag.TopicInsertMsg = "Added Topic with ID : " + TopicID;

                            // TopicID = 100;

                            //insert to SubTopic table
                            if (assetItem.subtopicModelInAsset != null)
                            {
                                assetItem.subtopicModelInAsset.TopicId = TopicID;
                                //save to db
                                // SubTopicID = _SubTopicService.AddSubTopicRetID(assetItem.subtopicModelInAsset);

                                if (!string.IsNullOrEmpty(assetItem.subtopicModelInAsset.SubtopicName))
                                {
                                    Task<int> SubTopicIDTsk = new Task<int>(() => _SubTopicService.AddSubTopicRetID(assetItem.subtopicModelInAsset));
                                    SubTopicIDTsk.Start();
                                    SubTopicID = await SubTopicIDTsk;
                                }

                            }

                            // Get assetType from assettype table
                            if (assetItem.assetTypeModelInAsset != null)
                            {
                                //save to db
                                //AssetTypeID = _AssetService.AddAssetTypeReturnId(assetItem.assetTypeModelInAsset);
                                Task<int> AssetTypeIDTsk = new Task<int>(() => _AssetService.AddAssetTypeReturnId(assetItem.assetTypeModelInAsset));
                                AssetTypeIDTsk.Start();
                                AssetTypeID = await AssetTypeIDTsk;
                            }
                            //insert list of asset to Asset table
                            assetItem.TopicId = TopicID;
                            if (SubTopicID != 0)
                            {
                                assetItem.SubtopicId = SubTopicID;
                            }
                            else
                            {
                                assetItem.SubtopicId = null;  //null
                            }

                            assetItem.AssetTypeId = AssetTypeID;
                            lstassetModel.Add(assetItem);


                            //prepare list of Module data to insert into Model table after this loop completes 
                            if (assetItem.moduleModelInAsset != null)
                            {
                                //lstmoduleModel.Add(assetItem.moduleModelInAsset);
                                Task<string> ModuleTsk = new Task<string>(() => _ModuleService.AddModule(assetItem.moduleModelInAsset));
                                ModuleTsk.Start();
                                string ModuleSuccMsg = await ModuleTsk;
                                hubContext.Clients.All.AddProgress("(" + ModuleSuccMsg + ". 0 MB of " + TotalCoreFileSize + " MB added.)", percentage + "%");
                            }
                        }
                        assetFileCountforPerc += 1;
                        percentage = (assetFileCountforPerc * 100) / itemsCount;
                        //update msg in view 
                        hubContext.Clients.All.AddProgress("(Creating Topic and SubTopic.... 0 MB of " + TotalCoreFileSize + " MB added.)", percentage + "%");
                    }

                    //save to db
                    //  Task<void> AssetListInsert = new Task<void>(() => _AssetService.AddAssetList(lstassetModel));
                    string AssetSuccMsg = await _AssetService.AssetbulkInsertAsync(lstassetModel);// Asset list save
                    percentage = (assetFileCountforPerc * 100) / itemsCount;
                    //update msg in view 
                    hubContext.Clients.All.AddProgress("(" + AssetSuccMsg + ". 0 MB of " + TotalCoreFileSize + " MB added.)", percentage + "%");

                    //_AssetService.AddAssetList(lstassetModel);
                    //_ModuleService.AddModuleList(lstmoduleModel);//Module list save 
                    // string ModuleSuccMsg = await _ModuleService.AddModuleListInsertAsync(lstmoduleModel);

                    //update msg in view 
                    // hubContext.Clients.All.AddProgress("(" + ModuleSuccMsg + ". 0 MB of " + TotalCoreFileSize + " MB added.)", percentage + "%");

                    #endregion
                    //copy core folder from asset to destination folder 
                    CopyAll(diSource, diTarget, path, hubContext, TotalCoreFileSize, percentage, assetFileCountforPerc, itemsCount);
                }
                #endregion

                #endregion

                // ModelState.Clear();

                //remove file logic
                if (System.IO.File.Exists(path))
                {
                    //  System.IO.File.Delete(path);
                }
                //
                hubContext.Clients.All.AddProgress("(File uploaded successfully)", "100%");
                return "File uploaded successfully";
            }
            catch (Exception ex)
            {
                return ex.Message;
            }
        }
        private void DeleteDirectory(string subPath)
        {
            try
            {
                // Delete all files from the Directory
                foreach (string filename in Directory.GetFiles(subPath))
                {
                    System.IO.File.Delete(filename);
                }
                // Check all child Directories and delete files
                foreach (string subfolder in Directory.GetDirectories(subPath))
                {
                    DeleteDirectory(subfolder);
                }
                Directory.Delete(subPath);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        //-----------------------------------RikArena_18/09/2019-------------------------------------------//
        [HttpPost]
        public async Task<ActionResult> UpdateTopicsOrderAsync(List<TopicModel> lstTopicModel)
        {
            string msg = string.Empty;
            try
            {
                msg = await _TopicService.UpdateSortedTopics(lstTopicModel);
                if (!string.IsNullOrEmpty(msg))
                {
                    return Json(msg, JsonRequestBehavior.AllowGet);
                }
                else
                {
                    return Json(true, JsonRequestBehavior.AllowGet);
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        #region methods
        static long DirectorySize(DirectoryInfo dInfo, bool includeSubDir)
        {
            long totalSize = dInfo.EnumerateFiles()
                         .Sum(file => file.Length);
            if (includeSubDir)
            {
                totalSize += dInfo.EnumerateDirectories()
                         .Sum(dir => DirectorySize(dir, true));
            }
            return totalSize;
        }

        private async Task<string> bulkAdding(HttpPostedFileBase file, int GradeId, int SubId, IHubContext hubContext)
        {
            try
            {
                int itemsCount = 0;
                var percentage = 0;
                long TotalCoreFileSize = 0;
                int assetFileCountforPerc = 0;

                #region size of upload directory 
                var fileName = Path.GetFileName(file.FileName);
                string fileuploadpath = file.FileName;
                String path = Server.MapPath("~/Content/Upload/" + GradeId + "/" + SubId + "/");

                #region copy core folder 
                //prepare directory to copy "Core" folder from the asset.xml
                if (!Directory.Exists(path))
                {
                    Directory.CreateDirectory(path);
                }
                hubContext.Clients.All.AddProgress("Reading Files....", percentage + "%");
                string FolderPathCopyAssetCore = fileuploadpath.Replace(fileName, "") + "\\core\\";
                if (!Directory.Exists(FolderPathCopyAssetCore))
                {
                    Directory.CreateDirectory(FolderPathCopyAssetCore);
                }
                if (Directory.Exists(FolderPathCopyAssetCore))
                {
                    DirectoryInfo diSource = new DirectoryInfo(FolderPathCopyAssetCore);
                    string targetDirectory = path + "\\core\\";
                    if (!Directory.Exists(targetDirectory))
                    {
                        Directory.CreateDirectory(targetDirectory);
                    }
                    long sizeOfSourceCoreDir = DirectorySize(diSource, true);

                    long inMBSs = ((sizeOfSourceCoreDir) / (1024 * 1024));
                    itemsCount = Convert.ToInt32(inMBSs);
                    TotalCoreFileSize = inMBSs;

                    //update msg in view 
                    hubContext.Clients.All.AddProgress("(0 MB of " + TotalCoreFileSize + " MB added.)", percentage + "%");
                    DirectoryInfo diTarget = new DirectoryInfo(targetDirectory);


                    #region read from asset.xml and insert data to db
                    List<AssetModel> AssetLst = new List<AssetModel>();
                    XmlDocument doc = new XmlDocument();
                    // asset.xml file 
                    var assetXMLpath = Path.Combine(Server.MapPath("~/Content/Upload"), fileName);
                    assetXMLpath = Path.Combine(path, fileName);
                    //save asset file
                    file.SaveAs(assetXMLpath);
                    doc.Load(Server.MapPath("~/Content/Upload/" + GradeId + "/" + SubId + "/" + fileName));


                    foreach (XmlNode node in doc.SelectNodes("/NewDataSet/dtAsset"))
                    {
                        //Fetch the Node values and assign it to Model.
                        AssetModel assetCombinationModel = new AssetModel();
                        if (IsNodeExist(node, "AssetId"))
                        {
                            assetCombinationModel.AssetId = Guid.Parse(node["AssetId"].InnerText);
                            if (IsNodeExist(node, "TermNo"))
                            {
                                var termNo = string.IsNullOrEmpty(node["TermNo"].InnerText) ? 0 : Convert.ToInt32(node["TermNo"].InnerText);
                                assetCombinationModel.TermNo = Convert.ToInt32(termNo);
                            }
                            if (IsNodeExist(node, "WeekNo"))
                            {
                                var weekNo = string.IsNullOrEmpty(node["WeekNo"].InnerText) ? 0 : Convert.ToInt32(node["WeekNo"].InnerText);
                                assetCombinationModel.WeekNo = Convert.ToInt32(weekNo);
                            }

                            TopicModel topicModel = new TopicModel();
                            if (IsNodeExist(node, "TopicNo"))
                            {
                                var topicNo = string.IsNullOrEmpty(node["TopicNo"].InnerText) ? 0 : Convert.ToInt32(node["TopicNo"].InnerText);
                                topicModel.TopicNo = Convert.ToInt32(topicNo);
                                //   assetCombinationModel.topicModelInAsset.TopicNo = Convert.ToInt32(node["TopicNo"].InnerText);
                            }
                            if (IsNodeExist(node, "TopicName"))
                            {
                                //assetCombinationModel.topicModelInAsset.TopicName = node["TopicName"].InnerText;
                                topicModel.TopicName = node["TopicName"].InnerText;
                            }
                            assetCombinationModel.topicModelInAsset = topicModel;

                            SubTopicModel subTopicModel = new SubTopicModel();
                            if (IsNodeExist(node, "SubtopicNo"))
                            {
                                if (subTopicModel.SubtopicNo == null)
                                {
                                    subTopicModel.SubtopicNo = Convert.ToInt32(null);//null
                                }
                                else
                                {
                                    subTopicModel.SubtopicNo = Convert.ToInt32(node["SubtopicNo"].InnerText);

                                }

                            }
                            if (IsNodeExist(node, "SubtopicName"))
                            {
                                subTopicModel.SubtopicName = node["SubtopicName"].InnerText;
                            }
                            else
                            {
                                subTopicModel.SubtopicName = string.Empty;
                            }
                            //rik
                            assetCombinationModel.subtopicModelInAsset = subTopicModel;


                            if (IsNodeExist(node, "AssetNo"))
                            {
                                var assetNo = string.IsNullOrEmpty(node["AssetNo"].InnerText) ? 0 : Convert.ToInt32(node["AssetNo"].InnerText);
                                assetCombinationModel.AssetNo = Convert.ToInt32(assetNo);
                            }
                            if (IsNodeExist(node, "AssetName"))
                                assetCombinationModel.AssetName = node["AssetName"].InnerText;

                            AssetTypeModel assetTypeModel = new AssetTypeModel();
                            if (IsNodeExist(node, "AssetType"))
                            {
                                assetTypeModel.AssetType1 = node["AssetType"].InnerText;
                                assetCombinationModel.assetTypeModelInAsset = assetTypeModel;
                            }
                            if (IsNodeExist(node, "FileName"))
                                assetCombinationModel.FileName = node["FileName"].InnerText;
                            if (IsNodeExist(node, "Description"))
                                assetCombinationModel.Description = node["Description"].InnerText;
                            if (IsNodeExist(node, "AssetLength"))
                            {
                                var assetLength = string.IsNullOrEmpty(node["AssetLength"].InnerText) ? 0 : Convert.ToInt32(node["AssetLength"].InnerText);
                                assetCombinationModel.AssetLength = Convert.ToInt32(assetLength);
                            }
                            if (IsNodeExist(node, "SuggestedActiveDuration"))
                            {
                                var suggestedActiveDuration = string.IsNullOrEmpty(node["SuggestedActiveDuration"].InnerText) ? 0 : Convert.ToInt32(node["SuggestedActiveDuration"].InnerText);
                                assetCombinationModel.SuggestedActiveDuration = Convert.ToInt32(suggestedActiveDuration);
                            }
                            if (IsNodeExist(node, "FolderName"))
                                assetCombinationModel.FolderName = node["FolderName"].InnerText;
                            if (IsNodeExist(node, "ThumbnailFileName"))
                                assetCombinationModel.ThumbnailFileName = node["ThumbnailFileName"].InnerText;
                            if (IsNodeExist(node, "Version"))
                            {
                                var version = string.IsNullOrEmpty(node["Version"].InnerText) ? 0 : Convert.ToInt32(node["Version"].InnerText);
                                assetCombinationModel.Version = Convert.ToInt32(version);
                            }

                            ModuleModel moduleModel = new ModuleModel();
                            if (IsNodeExist(node, "ModuleName"))
                            {
                                moduleModel.ModuleName = node["ModuleName"].InnerText;
                                moduleModel.CreatedDatime = DateTime.Now;
                                moduleModel.GradeId = GradeId;
                                moduleModel.SubjectId = SubId;
                            }
                            assetCombinationModel.moduleModelInAsset = moduleModel;
                        }
                        AssetLst.Add(assetCombinationModel);
                    }

                    //total item count ie: asset list count + core folder size in MB
                    itemsCount = itemsCount + AssetLst.Count;

                    List<AssetModel> lstassetModel = new List<AssetModel>();
                    //List<ModuleModel> lstmoduleModel = new List<ModuleModel>();
                    foreach (var assetItem in AssetLst)
                    {
                        int TopicID = 0; int SubTopicID = 0; int AssetTypeID = 0;
                        //insert to Topic Table
                        if (assetItem.topicModelInAsset != null)
                        {

                            assetItem.topicModelInAsset.SubjectId = SubId;
                            //save to db
                            //TopicID = _TopicService.AddTopicRetID(assetItem.topicModelInAsset);

                            Task<int> TopicIDTsk = new Task<int>(() => _TopicService.AddTopicRetID(assetItem.topicModelInAsset));
                            TopicIDTsk.Start();

                            // ViewBag.TopicInsertMsg = "Fetching Topic from xml....";
                            TopicID = await TopicIDTsk;
                            // ViewBag.TopicInsertMsg = "Added Topic with ID : " + TopicID;

                            // TopicID = 100;

                            //insert to SubTopic table
                            if (assetItem.subtopicModelInAsset != null)
                            {
                                assetItem.subtopicModelInAsset.TopicId = TopicID;
                                //save to db
                                // SubTopicID = _SubTopicService.AddSubTopicRetID(assetItem.subtopicModelInAsset);

                                if (!string.IsNullOrEmpty(assetItem.subtopicModelInAsset.SubtopicName))
                                {
                                    Task<int> SubTopicIDTsk = new Task<int>(() => _SubTopicService.AddSubTopicRetID(assetItem.subtopicModelInAsset));
                                    SubTopicIDTsk.Start();
                                    SubTopicID = await SubTopicIDTsk;
                                }

                            }

                            // Get assetType from assettype table
                            if (assetItem.assetTypeModelInAsset != null)
                            {
                                //save to db
                                //AssetTypeID = _AssetService.AddAssetTypeReturnId(assetItem.assetTypeModelInAsset);
                                Task<int> AssetTypeIDTsk = new Task<int>(() => _AssetService.AddAssetTypeReturnId(assetItem.assetTypeModelInAsset));
                                AssetTypeIDTsk.Start();
                                AssetTypeID = await AssetTypeIDTsk;
                            }
                            //insert list of asset to Asset table
                            assetItem.TopicId = TopicID;
                            if (SubTopicID != 0)
                            {
                                assetItem.SubtopicId = SubTopicID;
                            }
                            else
                            {
                                assetItem.SubtopicId = null;  //null
                            }
                                               
                            assetItem.isAssetHide = true;
                            assetItem.AssetTypeId = AssetTypeID;
                            lstassetModel.Add(assetItem);


                            //prepare list of Module data to insert into Model table after this loop completes 
                            if (assetItem.moduleModelInAsset != null)
                            {
                                //lstmoduleModel.Add(assetItem.moduleModelInAsset);
                                Task<string> ModuleTsk = new Task<string>(() => _ModuleService.AddModule(assetItem.moduleModelInAsset));
                                ModuleTsk.Start();
                                string ModuleSuccMsg = await ModuleTsk;
                                hubContext.Clients.All.AddProgress("(" + ModuleSuccMsg + ". 0 MB of " + TotalCoreFileSize + " MB added.)", percentage + "%");
                            }
                        }
                        assetFileCountforPerc += 1;
                        percentage = (assetFileCountforPerc * 100) / itemsCount;
                        //update msg in view 
                        hubContext.Clients.All.AddProgress("(Creating Topic and SubTopic.... 0 MB of " + TotalCoreFileSize + " MB added.)", percentage + "%");
                    }

                    //save to db
                    //  Task<void> AssetListInsert = new Task<void>(() => _AssetService.AddAssetList(lstassetModel));
                    string AssetSuccMsg = await _AssetService.AssetbulkInsertAsync(lstassetModel);// Asset list save
                    percentage = (assetFileCountforPerc * 100) / itemsCount;
                    //update msg in view 
                    hubContext.Clients.All.AddProgress("(" + AssetSuccMsg + ". 0 MB of " + TotalCoreFileSize + " MB added.)", percentage + "%");

                    //_AssetService.AddAssetList(lstassetModel);
                    //_ModuleService.AddModuleList(lstmoduleModel);//Module list save 
                    // string ModuleSuccMsg = await _ModuleService.AddModuleListInsertAsync(lstmoduleModel);

                    //update msg in view 
                    //                    hubContext.Clients.All.AddProgress("(" + ModuleSuccMsg + ". 0 MB of " + TotalCoreFileSize + " MB added.)", percentage + "%");

                    #endregion
                    //copy core folder from asset to destination folder 
                    CopyAll(diSource, diTarget, targetDirectory, hubContext, TotalCoreFileSize, percentage, assetFileCountforPerc, itemsCount);
                }

                #endregion

                #endregion

                // ModelState.Clear();

                //remove file logic
                if (System.IO.File.Exists(path))
                {
                    //  System.IO.File.Delete(path);
                }
                //
                hubContext.Clients.All.AddProgress("(File uploaded successfully)", "100%");
                return "File uploaded successfully";
            }
            catch (Exception ex)
            {
                return ex.Message;
                throw;
            }
        }


        public static void CopyAll(DirectoryInfo source, DirectoryInfo target, string targetDirectory, IHubContext hubContext, long TotalCoreFileSize, int percentage, int assetFileCountforPerc, int itemsCount)
        {
            Directory.CreateDirectory(target.FullName);

            // Copy each file into the new directory.
            foreach (FileInfo fi in source.GetFiles())
            {
                //  Console.WriteLine(@"Copying {0}\{1}", target.FullName, fi.Name);
                string fildetails = string.Format("Copying {0} - {1}", target.FullName, fi.Name);
                fi.CopyTo(Path.Combine(target.FullName, fi.Name), true);

                DirectoryInfo targetCopied = new DirectoryInfo(targetDirectory);
                long sizeOfDir = DirectorySize(targetCopied, true);
                long inMBSs = ((sizeOfDir) / (1024 * 1024));

                percentage = ((assetFileCountforPerc + Convert.ToInt32(inMBSs)) * 100) / itemsCount;
                //update msg in view 
                hubContext.Clients.All.AddProgress("(" + inMBSs + " MB of " + TotalCoreFileSize + " MB added.)", percentage + "%");

            }

            // Copy each subdirectory using recursion.
            foreach (DirectoryInfo diSourceSubDir in source.GetDirectories())
            {
                DirectoryInfo nextTargetSubDir =
                    target.CreateSubdirectory(diSourceSubDir.Name);
                CopyAll(diSourceSubDir, nextTargetSubDir, targetDirectory, hubContext, TotalCoreFileSize, percentage, assetFileCountforPerc, itemsCount);
            }
        }

        public bool IsNodeExist(XmlNode node, string prop)
        {
            try
            {
                if (node[prop].InnerText == null || node[prop] != null)
                    return true;
                else
                    return false;
            }
            catch (Exception ex)
            {
                return false;
            }
        }


        public JsonResult IsTopicNameExist(TopicModel topicModel)
        {
            try
            {
                //if (TempData["SubjectName"] != null)
                //{
                //    if (Subject.SubjectVM.SubjectName == TempData["SubjectName"].ToString())
                //    {
                //        return Json(true, JsonRequestBehavior.AllowGet);
                //    }
                //}

                int GradeId = 0; int SubId = 0;
                if (TempData["tmpGradeIdSubjectId"] != null)
                {
                    string[] gradeIdSubId = TempData["tmpGradeIdSubjectId"].ToString().Split('~');
                    GradeId = Convert.ToInt32(gradeIdSubId[0]);
                    SubId = Convert.ToInt32(gradeIdSubId[1]);
                }

                var data = _TopicService.GetTopicByName(topicModel.TopicName, SubId);

                if (data != null)
                {
                    return Json("Topic Name already exists", JsonRequestBehavior.AllowGet);
                }
                else
                {
                    return Json(true, JsonRequestBehavior.AllowGet);
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return Json(true, JsonRequestBehavior.AllowGet);
        }

        private void TopicInitialization(int GradeID, int SubId)
        {
            var modelSubject = _SubjectService.GetSubjectById(SubId);
            string GradeName = _GradeService.GetGradeNameByGradeID(GradeID);
            ViewBag.GradeID = GradeID;
            ViewBag.SubjectID = SubId;

            //ViewBag.GradeName = GradeName;// for heading 
            //ViewBag.SubjectName = modelSubject.SubjectName;//for heading 
            //ViewData.Clear();
            ViewData["vdTopicGradeName"] = GradeName;// for heading 
            ViewData["vdTopicSubjectName"] = modelSubject.SubjectName;//for heading 
            ViewBag.TopicData = _TopicService.GetTopicsBySubjcectID(SubId);// for partial view 

        }

        #endregion



    }
}
