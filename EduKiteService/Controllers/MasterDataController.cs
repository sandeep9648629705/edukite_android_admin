﻿using EdukiteService.Utilities;
using EdukiteService.Business.Implementation;
using EdukiteService.Business.Interface;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace EduKiteService.Controllers
{
    public class MasterDataController : ApiController
    {
        private readonly IMasterDataService _masterData;
        public MasterDataController(IMasterDataService masterData)
        {
            _masterData = masterData;
        }
        [HttpGet]
        [ActionName("GetCountries")]
        public HttpResponseMessage GetCountries()
        {
            var countries = _masterData.GetCountries();

            return Request.CreateResponse(HttpStatusCode.OK, new APIMessage
            {
                StatusCode = (int)HttpStatusCode.OK,
                StatusMessage = Constants.SuccessMessage,
                Data = countries,
            });
        }
    }
}
