﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using EdukiteService.Business;
using EdukiteService.Data;
using System.Data.Entity;

namespace EduKiteService.Controllers
{
    public class CourseController : ApiController
    {
        Dictionary<string, string> keyValuePairs;

        [HttpPost]
        [ActionName("SelectGrade")]
        public Models.jsonResponse SelectGrade(JObject jInput)
        {
            User ipUser = JsonConvert.DeserializeObject<User>(jInput.ToString());

            try
            {
                using (var context = new Edukite_AndroidEntities())
                {
                    var result = context.Users.SingleOrDefault(b => b.UserId == ipUser.UserId);
                    if (result != null)
                    {
                        result.GradeID = ipUser.GradeID;
                        context.SaveChanges();
                    }
                }

                return new Models.jsonResponse() { status = 200, statusMessage = "Success" };
            }
            catch (Exception ex)
            {
                return new Models.jsonResponse() { status = 400, statusMessage = "Failure: " + ex.Message };
            }
        }

        [HttpPost]
        [ActionName("GetSubjects")]
        public Models.JsonResponseSubjects GetSubjects(JObject jInput)
        {
            SubjectGradeCombination ipUser = JsonConvert.DeserializeObject<SubjectGradeCombination>(jInput.ToString());

            try
            {
                using (var context = new Edukite_AndroidEntities())
                {
                    context.Configuration.ProxyCreationEnabled = false;

                    var lstSubjects = (from subj in context.Subjects
                                       join subjectGrade in context.SubjectGradeCombinations
                                       on subj.SubjectId equals subjectGrade.SubjectId
                                       where subjectGrade.GradeId.Equals(ipUser.GradeId)
                                       select new Models.RespSubject
                                       {
                                           SubjectId = subj.SubjectId,
                                           SubjectName = subj.SubjectName
                                       }).ToList();

                    if (lstSubjects.Count() > 0)
                    {
                        return new Models.JsonResponseSubjects() { status = 200, statusMessage = "Success", data = lstSubjects };
                    }
                    else
                    {
                        return new Models.JsonResponseSubjects() { status = 400, statusMessage = "Failure" };
                    }
                }
            }
            catch (Exception ex)
            {
                return new Models.JsonResponseSubjects() { status = 400, statusMessage = "Failure: " + ex.Message };
            }
        }

        [HttpPost]
        [ActionName("GetPopularVideos")]
        public Models.jsonResponse GetPopularVideos(JObject jInput)
        {
            User ipUser = JsonConvert.DeserializeObject<User>(jInput.ToString());
            keyValuePairs = new Dictionary<string, string>();

            return new Models.jsonResponse() { status = 200, statusMessage = "Success" };
        }

        [HttpPost]
        [ActionName("GetRecentActivity")]
        public Models.jsonResponse GetRecentActivity(JObject jInput)
        {
            User ipUser = JsonConvert.DeserializeObject<User>(jInput.ToString());
            keyValuePairs = new Dictionary<string, string>();

            return new Models.jsonResponse() { status = 200, statusMessage = "Success" };
        }

        [HttpPost]
        [ActionName("BookMark")]
        public Models.jsonResponse BookMark(JObject jInput)
        {
            Models.ReqBookMark ipBookMark = JsonConvert.DeserializeObject<Models.ReqBookMark>(jInput.ToString());

            try
            {
                using (var context = new Edukite_AndroidEntities())
                {
                    IEnumerable<BookmarkAsset> lstBookmarkAsset = from bookmarkAsset in context.BookmarkAssets
                                                                  where bookmarkAsset.UserId == ipBookMark.userId &&
                                                                  bookmarkAsset.AssetId == ipBookMark.assetId
                                                                  select bookmarkAsset;

                    IEnumerable<BookmarkSubtopic> lstBookmarkSubtopic = from bookmarkSubtopic in context.BookmarkSubtopics
                                                                        where bookmarkSubtopic.UserId == ipBookMark.userId &&
                                                                        bookmarkSubtopic.SubtopicId == ipBookMark.subTopicId
                                                                        select bookmarkSubtopic;

                    IEnumerable<BookmarkTopic> lstBookmarkTopic = from bookmarkTopic in context.BookmarkTopics
                                                                  where bookmarkTopic.UserId == ipBookMark.userId &&
                                                                  bookmarkTopic.TopicId == ipBookMark.topicId
                                                                  select bookmarkTopic;

                    if (ipBookMark.bookmark == "0")
                    {
                        if (lstBookmarkAsset.Count() > 0)
                        {
                            var asset = new BookmarkAsset { BookmarkAssetId = lstBookmarkAsset.First().BookmarkAssetId };
                            context.Entry(asset).State = EntityState.Detached;
                            context.Entry(asset).State = EntityState.Deleted;
                            //context.BookmarkAssets.Remove(asset);
                            context.SaveChanges();
                        }
                        if (lstBookmarkSubtopic.Count() > 0)
                        {
                            var Subtopic = new BookmarkSubtopic { BookmarkSubtopicId = lstBookmarkSubtopic.First().BookmarkSubtopicId };
                            //context.Entry(Subtopic).State = EntityState.Deleted;
                            context.BookmarkSubtopics.Remove(Subtopic);
                            context.SaveChanges();
                        }
                        if (lstBookmarkTopic.Count() > 0)
                        {
                            var Topic = new BookmarkTopic { BookmarkTopicId = lstBookmarkTopic.First().BookmarkTopicId };
                            //context.Entry(Topic).State = EntityState.Deleted;
                            context.BookmarkTopics.Remove(Topic);
                            context.SaveChanges();
                        }
                    }
                    else if (ipBookMark.bookmark == "1")
                    {
                        if (lstBookmarkAsset.Count() == 0)
                        {
                            var asset = new BookmarkAsset { AssetId = ipBookMark.assetId, UserId = ipBookMark.userId };
                            context.BookmarkAssets.Add(asset);
                            context.SaveChanges();
                        }
                        if (lstBookmarkSubtopic.Count() == 0)
                        {
                            var Subtopic = new BookmarkSubtopic { SubtopicId = ipBookMark.subTopicId, UserId = ipBookMark.userId };
                            context.BookmarkSubtopics.Add(Subtopic);
                            context.SaveChanges();
                        }
                        if (lstBookmarkTopic.Count() == 0)
                        {
                            var Topic = new BookmarkTopic { TopicId = ipBookMark.topicId, UserId = ipBookMark.userId };
                            context.BookmarkTopics.Add(Topic);
                            context.SaveChanges();
                        }
                    }

                    return new Models.jsonResponse() { status = 200, statusMessage = "Success" };
                }
            }
            catch (Exception ex)
            {
                return new Models.jsonResponse() { status = 400, statusMessage = "Failure: " + ex.Message };
            }
        }
    }
}