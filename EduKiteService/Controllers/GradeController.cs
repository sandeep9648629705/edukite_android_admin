﻿using EdukiteService.Business.Interface;
using EdukiteService.Utilities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace EduKiteService.Controllers
{
    public class GradeController : ApiController
    {
        private readonly IGradeService _GradeService;

        public GradeController(IGradeService GradeService)
        {
            _GradeService = GradeService;
        }
        [HttpGet]
        [Route("api/grade/GetGrades")]
        public HttpResponseMessage GetAllGrades()
        {

            var Grades = _GradeService.GetGrades();

            return Request.CreateResponse(HttpStatusCode.OK, new APIMessage { StatusCode = (int)HttpStatusCode.OK, StatusMessage = Constants.SuccessMessage, Data = Grades });

        }
    }
}
