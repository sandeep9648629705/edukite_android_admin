﻿using EdukiteService.Business.Entities;
using EdukiteService.Business.Interface;
using EduKiteService.Models;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace EdukiteService.Controllers
{
    public class NotificationController : BaseApi
    {

        private readonly INotificationService _NotificationService;
        public NotificationController(INotificationService NotificationService)
        {
            _NotificationService = NotificationService;

        }
        [HttpPost]
        [ActionName("GetNotificationCount")]
        public HttpResponseMessage GetNotificationCount(JObject jObject)
        {
            return Response(() =>
            {
                var UserId = jObject.GetValue("UserId", StringComparison.OrdinalIgnoreCase).Value<int>();
                var DeviceId = jObject.GetValue("DeviceId", StringComparison.OrdinalIgnoreCase).Value<string>();
                return _NotificationService.GetNotificationCount(UserId, DeviceId);
            });
        }
    }
}
