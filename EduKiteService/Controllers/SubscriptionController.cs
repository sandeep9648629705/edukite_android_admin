﻿using EdukiteService.Business.Entities;
using EdukiteService.Utilities;
using EduKiteService.Models;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using EdukiteService.Business.Interface;

namespace EduKiteService.Controllers
{
    public class SubscriptionController : BaseApi
    {
        private readonly ISubscriptionService _SubscriptionService;
        public SubscriptionController(ISubscriptionService SubscriptionService)
        {
            _SubscriptionService = SubscriptionService;
        }

        [HttpPost]
        [ActionName("GetSubscriptions")]
        public HttpResponseMessage GetSubscriptions(JObject jObject)
        {
            return Response(() =>
            {
                var UserId = ((JObject)jObject).GetValue("UserId", StringComparison.OrdinalIgnoreCase).Value<int>();
                var DeviceId = ((JObject)jObject).GetValue("DeviceId", StringComparison.OrdinalIgnoreCase).Value<string>();
                return _SubscriptionService.GetSubscriptions(UserId, DeviceId);
            });
        }

        [HttpPost]
        [ActionName("ActivateByCode")]
        public HttpResponseMessage ActivateByCode(JObject jObject)
        {
            return Response(() =>
            {
                var UserId = ((JObject)jObject).GetValue("UserId", StringComparison.OrdinalIgnoreCase).Value<int>();
                var DeviceId = ((JObject)jObject).GetValue("DeviceId", StringComparison.OrdinalIgnoreCase).Value<string>();
                var Code = ((JObject)jObject).GetValue("Code", StringComparison.OrdinalIgnoreCase).Value<string>();
                if (_SubscriptionService.ActivateByCode(UserId, DeviceId, Code))
                    return GenerateResponse();
                else
                {
                    Status = Enums.E005;
                    return GenerateResponse();
                }
            });
        }


        [HttpPost]
        [ActionName("ChooseSubscriptions")]
        public HttpResponseMessage ChooseSubscriptions(JObject jObject)
        {
            return Response(() =>
            {
                var UserId = ((JObject)jObject).GetValue("UserId", StringComparison.OrdinalIgnoreCase).Value<int>();
                var DeviceId = ((JObject)jObject).GetValue("DeviceId", StringComparison.OrdinalIgnoreCase).Value<string>();
                return _SubscriptionService.ChooseSubscriptions(UserId, DeviceId);
            });
        }
    }
}
