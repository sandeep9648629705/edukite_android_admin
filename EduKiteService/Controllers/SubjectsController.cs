﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using EdukiteService.Business.Entities;
using EdukiteService.Business.Interface;
using EdukiteService.Data;
using System.IO;
using System.IO.Compression;
using System.Threading.Tasks;

namespace EduKiteService.Controllers
{
    public class SubjectsController : Controller
    {
        private readonly ISubjectService _subjectService;
        private readonly IIconsService _iconsService;
        private readonly IGradeService _gradeService;
        private readonly IModuleService _ModuleService;
        private readonly ITopicService _TopicService;
        public SubjectsController(ISubjectService subjectService, IIconsService iconsService, IGradeService gradeService, IModuleService moduleService,ITopicService topicService)
        {
            _subjectService = subjectService;
            _iconsService = iconsService;
            _gradeService = gradeService;
            _ModuleService = moduleService;
            _TopicService = topicService;
        }


        // GET: Subjects
        [HttpGet]
        public ActionResult GetSubjects(int id)
        {
            EdukiteService.Business.Entities.CommonSubjectIconModel model = new EdukiteService.Business.Entities.CommonSubjectIconModel();
            model.SubjectVM = new SubjectModel();
            model.IconsVM = new IconsModel();
            ViewBag.GradeID = id;

            List<SubjectModel> subjectModel = _subjectService.GetSubjects(id); // need to pass gradeID
            ViewData["vdOnlyGradeName"] = _gradeService.GetGradeNameByGradeID(id);
            //ViewBag.GradeName = _gradeService.GetGradeNameByGradeID(id);
            return View(subjectModel);
        }

        // GET: Subjects/Details/5
        public ActionResult Details(int id)
        {
            return View();
        }

        // GET: Subjects/Create
        [HttpGet]
        public ActionResult Create(int id)
        {
            ViewBag.GradeName = _gradeService.GetGradeNameByGradeID(id);
            TempData.Remove("SubjectName");
            TempData["GradeID"] = id;
            TempData.Keep();
            ViewData["ThemeData"] = _subjectService.GetThemes();
            ViewBag.GradeID = id;
            List<SubjectModel> subjectModel = _subjectService.GetSubjects(id);
            ViewBag.SubjectData = subjectModel;
            ViewData["vdOnlyGradeName"] = _gradeService.GetGradeNameByGradeID(id);
            return View();
        }


        // POST: Subjects/Create
        [HttpPost]
        public ActionResult Create(CommonSubjectIconModel data)
        {
            int GradeID = 0;
            int subId = 0;
            try
            {
                List<Icon> iData = new List<Icon>();
                string strDDLValue = string.Empty;
                // GradeID = Convert.ToInt32(TempData["GradeID"]);
                GradeID = Convert.ToInt32(Request.Form["gradeId"]);
                ViewData["ThemeData"] = _subjectService.GetThemes();
                HttpPostedFileBase file = data.IconsVM.PostedFile;
                if (Request.Form["SelectedTheme"] != null && Request.Form["SelectedTheme"] != "")
                {
                    strDDLValue = Request.Form["SelectedTheme"].ToString();
                    if (file != null)
                    {
                        
                        data.SubjectVM.Themedb = strDDLValue;
                        data.SubjectVM.GradeID = GradeID;
                        data.SubjectVM.Code = GradeID + data.SubjectVM.ShortForm;//RikArena 
                        
                        subId = _subjectService.AddSubject(data.SubjectVM);
                        iData = GetFileData(file, GradeID, subId);
                        _iconsService.AddIcon(iData, subId);
                        // return RedirectToAction("GetSubjects", "subjects", new { @id = GradeID });
                    }
                }

               string gradeName = _gradeService.GetGradeNameByGradeID(GradeID);
                ModuleModel moduleModel = new ModuleModel();
                moduleModel.ModuleName = gradeName + " " + data.SubjectVM.SubjectName;
                    moduleModel.CreatedDatime = DateTime.Now;
                    moduleModel.GradeId = GradeID;
                    moduleModel.SubjectId = subId;
               string str = _ModuleService.AddModule(moduleModel);
              }
            catch (Exception ex)
            {
                throw ex;
            }
            return RedirectToAction("GetSubjects", "subjects", new { @id = GradeID });
        }


        private List<Icon> GetFileData(HttpPostedFileBase file, int gradeId, int subId)
        {
            List<Icon> iData = new List<Icon>();
            try
            {
                if (file.ContentLength > 0)
                {
                    var gradePath = Path.Combine(Server.MapPath("~/Content/Upload"), gradeId.ToString());
                    var subPath = Path.Combine(Server.MapPath("~/Content/Upload/" + gradeId.ToString()), subId.ToString());
                    if (!(Directory.Exists(gradePath)) && !(Directory.Exists(subPath)))
                    {
                        Directory.CreateDirectory(gradePath);
                        Directory.CreateDirectory(subPath);
                        iData = GetFileInfo(file, gradeId, subId);
                    }
                    else if ((Directory.Exists(gradePath)) && !(Directory.Exists(subPath)))
                    {
                        Directory.CreateDirectory(subPath);
                        iData = GetFileInfo(file, gradeId, subId);
                    }
                    else
                    {
                        foreach (string fn in Directory.GetFiles(subPath))
                        {
                            System.IO.File.Delete(fn);
                        }
                        iData = GetFileInfo(file, gradeId, subId);
                    }                    
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return iData;
        }

        private List<Icon> GetFileInfo(HttpPostedFileBase file, int gradeId, int subId)
        {
            List<Icon> iData = new List<Icon>();
            try
            {

                var fileName = Path.GetFileName(file.FileName);
                var path = Path.Combine(Server.MapPath("~/Content/Upload/" + gradeId.ToString() + "/" + subId.ToString()), fileName);
                file.SaveAs(path);
                string zipPath = path;
                using (ZipArchive archive = ZipFile.OpenRead(zipPath))
                {
                    foreach (ZipArchiveEntry entry in archive.Entries)
                    {
                        if (entry != null)
                        {
                            Icon im = new Icon();
                            string[] values = entry.Name.Split('.');
                            im.IconName = entry.Name;
                            im.IconPath = entry.FullName;
                            im.IconType = values[1].ToString();
                            iData.Add(im);
                        }
                                             

                    }

                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return iData;

        }


        // GET: Subjects/Edit/5
        [HttpGet]
        public ActionResult Edit(int id, int GradeID)
        {
            ViewData["vdOnlyGradeName"] = _gradeService.GetGradeNameByGradeID(GradeID);
            TempData.Remove("GradeID");
            ViewBag.GradeName = _gradeService.GetGradeNameByGradeID(GradeID);
            EdukiteService.Business.Entities.CommonSubjectIconModel model = new EdukiteService.Business.Entities.CommonSubjectIconModel();
            model.SubjectVM = new SubjectModel();
            model.IconsVM = new IconsModel();
            try
            {
                List<SubjectModel> subjectModel = _subjectService.GetSubjects(GradeID);
                ViewBag.SubjectData = subjectModel;
                ViewBag.GradeID = GradeID;

                ViewData["ThemeData"] = _subjectService.GetThemes();
                var subData = _subjectService.GetSubjectById(id);
                model.SubjectVM.SubjectId = subData.SubjectId;
                model.SubjectVM.SubjectName = subData.SubjectName;
                TempData["SubjectName"] = model.SubjectVM.SubjectName;
                TempData["GradeID"] = GradeID;
                TempData.Keep();
                model.SubjectVM.Themedb = subData.Themedb;
                model.SubjectVM.ShortForm = subData.ShortForm;
                model.SubjectVM.Description = subData.Description;
                model.SubjectVM.GradeID = subData.GradeID;
                string iconName = _iconsService.GetIconById(id);
                model.IconsVM.IconName = iconName.Split('/')[0].ToString() + ".zip";

                return View(model);
            }
            catch (Exception ex)
            {
                throw ex;
            }

            return View(model);
        }

        // POST: Subjects/Edit/5
        [HttpPost]
        public ActionResult Edit(CommonSubjectIconModel data)
        {
            try
            {
                List<Icon> iData = new List<Icon>();
                string strDDLValue = string.Empty;
                if (Request.Form["SelectedTheme"] != null && Request.Form["SelectedTheme"] != "")
                {
                    strDDLValue = Request.Form["SelectedTheme"].ToString();
                    data.SubjectVM.Themedb = strDDLValue;
                    _subjectService.UpdateSubject(data.SubjectVM);
                    HttpPostedFileBase file = data.IconsVM.PostedFile;
                    if (file != null)
                    {
                        iData = GetFileData(file,Convert.ToInt32(data.SubjectVM.GradeID),data.SubjectVM.SubjectId);
                        _iconsService.UpdateIcon(iData, data.SubjectVM.SubjectId);
                    }
                    return RedirectToAction("GetSubjects", "subjects", new { @id = data.SubjectVM.GradeID });
                }
            }

            catch (Exception ex)
            {
                throw ex;
            }
            return RedirectToAction("GetSubjects", "subjects", new { @id = data.SubjectVM.GradeID });
        }

        public ActionResult DeleteSubject(int id, int GradeId)
        {
            try
            {
                string gradeName = _gradeService.GetGradeNameByGradeID(GradeId);
                string subjectName = _subjectService.GetSubjectNameById(id);
                var subPath = Path.Combine(Server.MapPath("~/Content/" + gradeName + " " + subjectName));
                if (Directory.Exists(subPath))
                {
                     DeleteDirectory(subPath);
                    _iconsService.DeleteIcon(id);
                    _TopicService.DeleteTopicBySubjectId(id);
                    _ModuleService.DeleteModuleBySubjectId(id);
                    _subjectService.DeleteSubject(id);
                }
                else
                {
                    _iconsService.DeleteIcon(id);
                    _TopicService.DeleteTopicBySubjectId(id);
                    _ModuleService.DeleteModuleBySubjectId(id);
                    _subjectService.DeleteSubject(id);
                }
               // moduleModel.ModuleName = gradeName + " " + data.SubjectVM.SubjectName;
                         
                //if (Directory.Exists(subPath))
                //{
                //    DeleteDirectory(subPath);
                //}
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return RedirectToAction("GetSubjects", "subjects", new { @id = GradeId });
        }

        private void DeleteDirectory(string subPath)
        {
           try
            {
                // Delete all files from the Directory
                foreach (string filename in Directory.GetFiles(subPath))
                {
                   System.IO.File.Delete(filename);
                }
                // Check all child Directories and delete files
                foreach (string subfolder in Directory.GetDirectories(subPath))
                {
                    DeleteDirectory(subfolder);
                }
                Directory.Delete(subPath);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public ActionResult PublishSubject(int id, int GradeId)
        {
            try
            {
                _subjectService.PublishSubject(id);
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return RedirectToAction("GetSubjects", "subjects", new { @id = GradeId });

        }

        public void GetPublishedSubjectCount(int GradeId)
        {
            int count = _subjectService.GetPublishedSubjectCount(GradeId);
            if (count >= 1)
            {
                TempData["SubPublishFlag"] = true;
            }
            else
            {
                TempData["SubPublishFlag"] = false;
            }
            TempData.Keep();
        }

        [HttpPost]
        public JsonResult DeleteFile(string id)
        {
            if (String.IsNullOrEmpty(id))
            {
                Response.StatusCode = (int)System.Net.HttpStatusCode.BadRequest;
                return Json(new { Result = "Error" });
            }
            try
            {
                //string path = Request.MapPath("~/Content/Upload/" + id);
                //if (System.IO.File.Exists(path))
                //{
                //    System.IO.File.Delete(path);
                //}
                return Json(new { Result = "OK" });
            }
            catch (Exception ex)
            {
                return Json(new { Result = "ERROR", Message = ex.Message });
            }
        }

        public void GetInitials(int themeId)
        {
            //var parts = name.Split(' ');
            //var initials = "";
            //for (var i = 0; i < parts.Length; i++)
            //{
            //    if (parts[i].Length > 0 && parts[i] != "")
            //    {
            //        initials += parts[i][0];
            //    }
            //}
            //TempData["Initials"] = initials;
            //TempData.Keep();

            ThemeModel themeData = _subjectService.GetThemesById(themeId);
            TempData["ThemeName"] = themeData.ThemeColor;
            if ((themeData.ThemeColor).ToLower() == "dark yellow")
            {
                TempData["ThemeClass"] = "dark-yellow-bar";
            }
            else if ((themeData.ThemeColor).ToLower() == "dark blue")
            {
                TempData["ThemeClass"] = "dark-blue-bar";
            }
            else
            {
                TempData["ThemeClass"] = (themeData.ThemeColor).ToLower() + "-bar";
            }

            TempData.Keep();
        }

        public JsonResult IsSubjectNameExist(CommonSubjectIconModel Subject)
        {
            try
            {
                int gradeId = 0;
                if (TempData["SubjectName"] != null)
                {
                    if (Subject.SubjectVM.SubjectName == TempData["SubjectName"].ToString())
                    {
                        return Json(true, JsonRequestBehavior.AllowGet);
                    }
                }

                if (TempData["GradeID"] != null)
                {
                    gradeId = Convert.ToInt32(TempData["GradeID"]);
                }
               
                var data = _subjectService.GetSubjectsByName(Subject.SubjectVM.SubjectName.ToString(), gradeId);

                if (data != null)
                {
                    return Json("Subject name already exists", JsonRequestBehavior.AllowGet);
                }
                else
                {
                    return Json(true, JsonRequestBehavior.AllowGet);
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return Json(true, JsonRequestBehavior.AllowGet);
        }

    }
}
