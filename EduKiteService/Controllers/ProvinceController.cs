﻿using EdukiteService.Business.Interface;
using EdukiteService.Utilities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace EduKiteService.Controllers
{
    public class ProvinceController : ApiController
    {
        private readonly IProvinceService _ProvinceService;

        public ProvinceController(IProvinceService ProvinceService)
        {
            _ProvinceService = ProvinceService;
        }
        [HttpGet]
        [Route("api/province/GetProvince")]
        public HttpResponseMessage GetProvince()
        {
            var provinces = _ProvinceService.GetProvince();
            return Request.CreateResponse(HttpStatusCode.OK, new APIMessage { StatusCode = (int)HttpStatusCode.OK, StatusMessage = Constants.SuccessMessage, Data = provinces });
        }
    }
}
