﻿using EdukiteService.Business.Entities;
using EdukiteService.Business.Interface;
using EduKiteService.Models;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace EdukiteService.Controllers
{
    public class BookMarkController : BaseApi
    {

        private readonly IBookMarkService _BookMarkService;
        public BookMarkController(IBookMarkService BookMarkService)
        {
            _BookMarkService = BookMarkService;
        }

        [HttpPost]
        [ActionName("GetBookmarks")]
        public HttpResponseMessage GetBookmarks(BooKMarkRequest bookMark)
        {
            return Response(() =>
            {
                return _BookMarkService.GetBookmarks(bookMark);
            });
        }

    }
}
