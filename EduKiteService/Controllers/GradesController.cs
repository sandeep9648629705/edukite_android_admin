﻿using AutoMapper;
using EdukiteService.Business.Entities;
using EdukiteService.Business.Interface;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace EduKiteService.Controllers
{
    public class GradesController : Controller
    {
        private readonly IGradeService _GradeService;
        private readonly ISubjectService _subjectService;
        private readonly IModuleService _ModuleService;

        public GradesController(IGradeService gradeService, ISubjectService subjectService, IModuleService moduleService)
        {
            try
            {
                _GradeService = gradeService;
                _subjectService = subjectService;
                _ModuleService = moduleService;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public ActionResult GetGrades()
        {
            ViewData["vdOnlyGradeRoot"] = true;
            List<GradeModel> gradeModels2 = _GradeService.GetGrades();
            return View(gradeModels2);
        }


        [HttpGet]
        public ActionResult Create()
        {
            try
            {
                ViewData["vdOnlyGradeRoot"] = true;
                List<GradeModel> gradeModel = _GradeService.GetGrades().ToList();
                ViewBag.GradeData = gradeModel;

            }
            catch (Exception ex)
            {
                throw ex;
                //return View();
            }
            return View();
            }

        [HttpPost]
        public ActionResult Create(GradeModel gradeData)
        {
            try
            {
                if (gradeData.GradeName != null)
                {
                    var addGrade = new GradeModel();
                    addGrade.GradeName = gradeData.GradeName;
                    addGrade.Description = gradeData.Description;
                    _GradeService.AddGrades(addGrade);
                    ViewBag.GradeData = _GradeService.GetGrades().ToList();
                }

            }
            catch (Exception ex)
            {
                throw ex;
            }
            return RedirectToAction("GetGrades", "Grades");

        }


        public ActionResult Edit(int id)
        {
            try
            {
                ViewData["vdOnlyGradeRoot"] = true;
                var getGrade = _GradeService.GetGradeById(id);
                var gradeDispay = new GradeModel();
                gradeDispay.GradeId = getGrade.GradeId;
                gradeDispay.GradeName = getGrade.GradeName;
                gradeDispay.Description = getGrade.Description;
                ViewBag.GradeData = _GradeService.GetGrades().ToList();
                return View(gradeDispay);
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return View();
        }

        [HttpPost]
        public ActionResult Edit(GradeModel gradeData)
        {
            try
            {
                _GradeService.UpdateGrade(gradeData);
                ViewBag.GradeData = _GradeService.GetGrades().ToList();
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return RedirectToAction("GetGrades", "Grades");

        }

        public JsonResult IsGradeNameExist(string GradeName, string initialGradeName)
        {
            try
            {
                if (GradeName == initialGradeName)
                {
                    return Json(true, JsonRequestBehavior.AllowGet);
                }
                var data = _GradeService.GetGradeByName(GradeName);

                if (data != null)
                {
                    return Json("Grade name already exists", JsonRequestBehavior.AllowGet);
                }
                else
                {
                    return Json(true, JsonRequestBehavior.AllowGet);
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return Json(true, JsonRequestBehavior.AllowGet);
        }


        public ActionResult PublishGrade(int id)
        {
            try
            {
                _GradeService.PublishGrade(id);
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return RedirectToAction("GetGrades", "Grades");

        }

        public ActionResult DeleteGrade(int id)
        {
            try
            {
               _ModuleService.DeleteModule(id);
                
                _subjectService.DeleteSubjectAll(id);
                _GradeService.DeleteGrade(id);
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return RedirectToAction("GetGrades", "Grades");

        }



        public void GetPublishedGrade(int GradeId)
        {
            bool GradePublishFlag = _GradeService.IsGradePublished(GradeId);

            TempData["GradePublishFlag"] = GradePublishFlag;
            TempData.Keep();
        }
    }
}