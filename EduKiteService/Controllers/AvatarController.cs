﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using EdukiteService.Business;
using EdukiteService.Data;

namespace EduKiteService.Controllers
{
    public class AvatarController : ApiController
    {
        Dictionary<string, string> keyValuePairs;

        [HttpGet]
        [ActionName("GetAvatar")]
        public Models.JsonResponseAvatar GetAvatar()
        {
            try
            {
                using (var context = new Edukite_AndroidEntities())
                {
                    List<ImageMaster> lstImage = context.ImageMasters.ToList();

                    if (lstImage.Count() > 0)
                    {
                        return new Models.JsonResponseAvatar() { status = 200, statusMessage = "Success", data = lstImage };
                    }
                    else
                    {
                        return new Models.JsonResponseAvatar() { status = 400, statusMessage = "Failure" };
                    }
                }
            }
            catch (Exception ex)
            {
                return new Models.JsonResponseAvatar() { status = 400, statusMessage = "Failure: " + ex.Message };
            }
        }

        [HttpPost]
        [ActionName("UpdateAvatar")]
        public Models.jsonResponse UpdateAvatar(JObject jInput)
        {
            User ipUser = JsonConvert.DeserializeObject<User>(jInput.ToString());

            try
            {
                using (var context = new Edukite_AndroidEntities())
                {
                    var result = context.Users.SingleOrDefault(b => b.UserId == ipUser.UserId);
                    if (result != null)
                    {
                        result.AvatarID = ipUser.AvatarID;
                        context.SaveChanges();
                    }
                }

                return new Models.jsonResponse() { status = 200, statusMessage = "Success" };
            }
            catch (Exception ex)
            {
                return new Models.jsonResponse() { status = 400, statusMessage = "Failure: " + ex.Message };
            }
        }
    }
}
