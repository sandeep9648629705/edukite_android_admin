﻿using EdukiteService.Business.Entities;
using EdukiteService.Utilities;
using EdukiteService.Business.Interface;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using Newtonsoft.Json.Linq;
using EduKiteService.Models;

namespace EduKiteService.Controllers
{
    public class SubjectController : BaseApi
    {
        private readonly ISubjectService _subjectService;
        public SubjectController(ISubjectService subjectService)
        {
            _subjectService = subjectService;
        }
        [HttpGet]
        [Route("api/Subject/GetSubject")]
        public HttpResponseMessage GetSubject(int gradeId)
        {

            var provinces = _subjectService.GetSubjectDetails(gradeId);

            return Request.CreateResponse(HttpStatusCode.OK, new APIMessage { StatusCode = (int)HttpStatusCode.OK, StatusMessage = Constants.SuccessMessage, Data = provinces });

        }
        [HttpGet]
        [Route("api/Subject/GetSubjectDetails")]
        public HttpResponseMessage GetSubjectDetails(SubjectDetailsModel subj)
        {

            var provinces = _subjectService.GetSubjectDetails(subj.SubjectId);

            return Request.CreateResponse(HttpStatusCode.OK, new APIMessage { StatusCode = (int)HttpStatusCode.OK, StatusMessage = Constants.SuccessMessage, Data = provinces });

        }


        [HttpPost]
        [Route("api/Subject/getSubjectsForExamPaper")]
        public HttpResponseMessage GetSubjectsForExamPaper(JObject jObject)
        {
            var UserId = ((JObject)jObject).GetValue("userId", StringComparison.OrdinalIgnoreCase).Value<int>();
            var DeviceId = ((JObject)jObject).GetValue("deviceId", StringComparison.OrdinalIgnoreCase).Value<string>();
            var GradeId = ((JObject)jObject).GetValue("gradeId", StringComparison.OrdinalIgnoreCase).Value<int>();
            //return Response(() =>
            //{

            //    return _SubscriptionService.ChooseSubscriptions(UserId, DeviceId);
            //});


            List<SubjectModel> lstSubjectModel = new List<SubjectModel>();
            lstSubjectModel.Add(new SubjectModel
            {
                name = "Life Sciences",
                id = 1,
                theme = "GREEN",
                totalExamPapers = 40,
                completedExamPapers = 15

            });
            lstSubjectModel.Add(new SubjectModel
            {
                name = "Mathematics",
                id = 13,
                theme = "RED",
                totalExamPapers = 25,
                completedExamPapers = 10

            });

            //method: post, url : http://localhost:30888/api/Subject/getSubjectsForExamPaper , Request body: { userId:10,deviceId:11,gradeId:22} , Content-Type: application/json            

            return Request.CreateResponse(HttpStatusCode.OK, new APIMessage { StatusCode = (int)HttpStatusCode.OK, StatusMessage = Constants.SuccessMessage, Data = lstSubjectModel });

        }



    }
}
