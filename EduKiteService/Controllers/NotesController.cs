﻿using EdukiteService.Business.Entities;
using EdukiteService.Business.Interface;
using EdukiteService.Data;
using EdukiteService.Data.interfaces;
using EdukiteService.Utilities;
using EduKiteService.Models;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace EdukiteService.Controllers
{
    public class NotesController : BaseApi
    {

        private readonly INoteService _NoteService;
        public NotesController(INoteService NoteService)
        {
            _NoteService = NoteService;

        }

        [HttpPost]
        [ActionName("GetNotes")]
        public HttpResponseMessage GetNotes([FromBody]NoteModel note)
        {
            return Response(() => { return _NoteService.GetNotes(note); });

        }

        [HttpPost]
        [ActionName("AddNotes")]
        public HttpResponseMessage AddNotes([FromBody]NoteModel note)
        {
            return Response(() => { return _NoteService.AddNote(note); });
        }

        [HttpPost]
        [ActionName("DeleteNote")]
        public HttpResponseMessage DeleteNote([FromBody]NoteModel note)
        {
            return Response(() =>
            {
                if (_NoteService.DeleteNote(note))
                    return GenerateResponse();
                else
                {
                    Status = Enums.E005;
                    return GenerateResponse();
                }
            });
        }

    }
}
