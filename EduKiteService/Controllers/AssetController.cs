﻿using EdukiteService.Business.Entities;
using EdukiteService.Business.Interface;
using EdukiteService.Data;
using EdukiteService.Data.interfaces;
using System;
using System.Collections.Generic;
using System.IO;
using System.IO.Compression;
using System.Linq;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.Mvc;
using System.Xml;

namespace EduKiteService.Controllers
{
    public class AssetController : Controller
    {
        private readonly IAssetService _assetService;
        private readonly ISubTopicService _subtopicservice;
        private readonly ITopicService _topicService;
        private readonly ISubjectService _subjectService;
        private readonly IGradeService _gradeService;
        public AssetController(IAssetService assetService, ISubTopicService subTopicService, ITopicService topicService, ISubjectService subjectService, IGradeService gradeService)
        {
            _assetService = assetService;
            _subtopicservice = subTopicService;
            _topicService = topicService;
            _subjectService = subjectService;
            _gradeService = gradeService;
        }



        // GET: Asset
        public ActionResult Index()
        {
            return View();
        }

        [HttpPost]
        public int CreateData(FormCollection assetData)
        {
            AssetModel adata = new AssetModel();
            try
            {
                adata.AssetId = Guid.Parse(assetData["AssetId"]);
                adata.isAssetHide = true;
                adata.AssetName = assetData["AssetName"];
                adata.AssetTypeId = Convert.ToInt32(assetData["AssetTypeId"]);
                if (assetData["SubtopicId"] != null && assetData["SubtopicId"] != "")
                    adata.SubtopicId = Convert.ToInt32(assetData["SubtopicId"]);
                adata.Description = assetData["Description"];
                adata.TopicId = Convert.ToInt32(assetData["TpoicId"]);
                adata.isAssetHide = true;
                TempData["AssetData"] = adata;
                TempData.Keep();
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return Convert.ToInt32(assetData["AssetTypeId"]);
             }

        [HttpGet]
        public ActionResult CreateSimulationorInteractive(int id)
        {
            AssetModel ad = new AssetModel();
            try
            {
                TempData["TopicId"] = id;
                TempData.Keep();
                int topicID = Convert.ToInt32(TempData["TopicId"]);
                getSTAData(topicID);
                List<AssetTypeModel> assetTypeSimIntData = _assetService.GetSimIntAssetType();
                ViewData["assetTypeSimIntData"] = assetTypeSimIntData;
                ad = (AssetModel)TempData["AssetData"];
                AddHeaderForMore(topicID, Convert.ToInt32(ad.SubtopicId));
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return View(ad);
        }

        private void getSTAData(int topicID)
        {
            try
            {
                CommonSubTopicAssetModel model = new CommonSubTopicAssetModel();
                List<SubTopicModel> subTopicData = _subtopicservice.GetSubTopics(topicID); // need to pass topic id
                ViewData["SubTopicData"] = subTopicData;
                model.SubTopicVM = subTopicData;
                model.AssetVM = _assetService.GetAssetByTopicId(topicID);
                ViewBag.SubTopicAssetData = model;
                List<AssetTypeModel> assetTypeData = _assetService.GetAssetType();
                ViewData["AssetTypeData"] = assetTypeData;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        [HttpPost]
        public ActionResult CreateSimulationorInteractive(AssetModel assetData)
        {
            string gradeName;
            string subjectName;

            int TopicId = 0;
            try
            {
                TopicId = Convert.ToInt32(assetData.TopicId);
                Tuple<string, string> res = getSubtopicandAssetTypeData(TopicId);
                gradeName = res.Item1;
                subjectName = res.Item2;
                var data = new AssetModel();
                data.AssetId = assetData.AssetId;
                data.SuggestedActiveDuration = assetData.SuggestedActiveDuration;
                data.AssetName = assetData.AssetName;
                data.Description = assetData.Description;

                if (TempData["isAssetHide"] != null)
                {
                    data.isAssetHide = Convert.ToBoolean(TempData["isAssetHide"]);
                }
                else
                {
                    data.isAssetHide = true;
                }

                if (Request.Form["SelectedMoreScreenSubTopic"] != null && Request.Form["SelectedMoreScreenSubTopic"] != "")
                {
                    data.SubtopicId = Convert.ToInt32(Request.Form["SelectedMoreScreenSubTopic"]);
                }
                else
                {
                    data.SubtopicId = null;
                }

                if (Request.Form["SelectedSimIntAssetType"] != null && Request.Form["SelectedSimIntAssetType"] != "")
                {
                    data.AssetTypeId = Convert.ToInt32(Request.Form["SelectedSimIntAssetType"]);
                }
                else
                {
                    data.AssetTypeId = Convert.ToInt32(assetData.AssetTypeId);
                }

                if (assetData.PostedFileZip != null)
                {
                    uploadFile(assetData.PostedFileZip, assetData.AssetId.ToString(), gradeName, subjectName, data.AssetTypeId);
                    data.FileName = TempData["FileName"].ToString();
                    data.ThumbnailFileName = TempData["ImageName"].ToString();
                    data.FolderName = TempData["FolderName"].ToString();
                }
                data.TopicId = TopicId;
                string assetId = _assetService.AddAssetInsert(data);

                // }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return RedirectToAction("GetSubTopics", "SubTopics", new { @id = TopicId });

        }

        private Tuple<string, string> getSubtopicandAssetTypeData(int topicId)
        {
            int TopicId = topicId;
            var res = (Tuple<string, string>)null;
            try
            {
                res = getSubjectNameandGradeName(TopicId);
                List<SubTopicModel> subTopicData = _subtopicservice.GetSubTopics(TopicId); // need to pass topic id
                ViewData["SubTopicData"] = subTopicData;
                List<AssetTypeModel> assetTypeData = _assetService.GetAssetType();
                ViewData["AssetTypeData"] = assetTypeData;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return res;

        }

        private Tuple<string, string> getSubjectNameandGradeName(int topicId)
        {
            int gradeId;
            int subjectId;
            int TopicId = topicId;
            var res = (Tuple<string, string>)null;
            try
            {
                TopicModel tdata = _topicService.GetTopicById(TopicId);
                subjectId = tdata.SubjectId;
                SubjectModel sdata = _subjectService.GetSubjectById(subjectId);
                gradeId = Convert.ToInt32(sdata.GradeID);
                var modelSubject = _subjectService.GetSubjectById(subjectId);
                string GradeName = _gradeService.GetGradeNameByGradeID(gradeId);
                res = Tuple.Create<string, string>(GradeName, modelSubject.SubjectName);
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return res;
        }


        //private void UploadZipFileWithoutSubtopic(Guid AssetId, int GradeId, int SubjectId, int TopicId, int? SubTopicId, int oldSubtopicId)
        //{
        //    try
        //    {

        //        var gradePath = Path.Combine(Server.MapPath("~/Content/Upload"), GradeId.ToString());
        //        var subPath = Path.Combine(Server.MapPath("~/Content/Upload/" + GradeId.ToString()), SubjectId.ToString());
        //        var topicPath = Path.Combine(Server.MapPath("~/Content/Upload/" + GradeId.ToString() + "/" + SubjectId.ToString()), TopicId.ToString());
        //        var subTopicPath = string.Empty;
        //        var pathAsset = string.Empty;
        //        var oldPathAsset = string.Empty;
        //        subTopicPath = Path.Combine(Server.MapPath("~/Content/Upload/" + GradeId.ToString() + "/" + SubjectId.ToString() + "/" + TopicId.ToString()), SubTopicId.ToString());
        //        pathAsset = Path.Combine(Server.MapPath("~/Content/Upload/" + GradeId.ToString() + "/" + SubjectId.ToString() + "/" + TopicId.ToString() + "/" + SubTopicId.ToString()), AssetId.ToString());
        //        if (oldSubtopicId != 0)
        //        {

        //            oldPathAsset = Path.Combine(Server.MapPath("~/Content/Upload/" + GradeId.ToString() + "/" + SubjectId.ToString() + "/" + TopicId.ToString() + "/" + oldSubtopicId.ToString()), AssetId.ToString());
        //        }
        //        else
        //        {

        //            oldPathAsset = Path.Combine(Server.MapPath("~/Content/Upload/" + GradeId.ToString() + "/" + SubjectId.ToString() + "/" + TopicId.ToString()), AssetId.ToString());
        //        }               

        //        if (!(Directory.Exists(gradePath)) && !(Directory.Exists(subPath)) && !(Directory.Exists(topicPath)) && !(Directory.Exists(subTopicPath)) && !(Directory.Exists(pathAsset)))
        //        {
        //            Directory.CreateDirectory(gradePath);
        //            Directory.CreateDirectory(subPath);
        //            Directory.CreateDirectory(topicPath);
        //            Directory.CreateDirectory(subTopicPath);
        //            Directory.CreateDirectory(pathAsset);

        //        }
        //        else if ((Directory.Exists(gradePath)) && !(Directory.Exists(subPath)) && !(Directory.Exists(topicPath)) && !(Directory.Exists(subTopicPath)) && !(Directory.Exists(pathAsset)))
        //        {
        //            Directory.CreateDirectory(subPath);
        //            Directory.CreateDirectory(topicPath);
        //            Directory.CreateDirectory(subTopicPath);
        //            Directory.CreateDirectory(pathAsset);

        //        }
        //        else if ((Directory.Exists(gradePath)) && (Directory.Exists(subPath)) && !(Directory.Exists(topicPath)) && !(Directory.Exists(subTopicPath)) && !(Directory.Exists(pathAsset)))
        //        {
        //            Directory.CreateDirectory(topicPath);
        //            Directory.CreateDirectory(subTopicPath);
        //            Directory.CreateDirectory(pathAsset);
        //        }
        //        else if ((Directory.Exists(gradePath)) && (Directory.Exists(subPath)) && (Directory.Exists(topicPath)) && !(Directory.Exists(subTopicPath)) && !(Directory.Exists(pathAsset)))
        //        {
        //            Directory.CreateDirectory(subTopicPath);
        //            Directory.CreateDirectory(pathAsset);
        //        }
        //        else if ((Directory.Exists(gradePath)) && (Directory.Exists(subPath)) && (Directory.Exists(topicPath)) && (Directory.Exists(subTopicPath)) && !(Directory.Exists(pathAsset)))
        //        {
        //            Directory.CreateDirectory(pathAsset);
        //        }

        //        if (Directory.Exists(oldPathAsset))
        //        {
        //            DirectoryCopy(oldPathAsset, pathAsset, true);
        //            DeleteDirectory(oldPathAsset);
        //        }
        //        else
        //        {
        //            Console.WriteLine("Source path does not exist!");
        //        }




        //    }
        //    catch (Exception ex)
        //    {

        //    }
        //}



        private static void DirectoryCopy(string sourceDirName, string destDirName, bool copySubDirs)
        {

            //Now Create all of the directories
            foreach (string dirPath in Directory.GetDirectories(sourceDirName, "*",
                SearchOption.AllDirectories))
                Directory.CreateDirectory(dirPath.Replace(sourceDirName, destDirName));

            //Copy all the files & Replaces any files with the same name
            foreach (string newPath in Directory.GetFiles(sourceDirName, "*.*",
                SearchOption.AllDirectories))
                System.IO.File.Copy(newPath, newPath.Replace(sourceDirName, destDirName), true);
        }


        [HttpGet]
        public ActionResult CreateVideo(int id)
        {
            AssetModel ad = new AssetModel();
            try
            {
                TempData["TopicId"] = id;
                TempData.Keep();
                int topicID = Convert.ToInt32(TempData["TopicId"]);
                getSTAData(topicID);
                ad = (AssetModel)TempData["AssetData"];
                ad.TopicId = topicID;
                AddHeaderForMore(topicID, Convert.ToInt32(ad.SubtopicId));
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return View(ad);
        }


        [HttpPost]
        public ActionResult CreateVideo(AssetModel assetData)
        {
            string gradeName;
            string subjectName;

            int TopicId = 0;
            try
            {
                TopicId = Convert.ToInt32(TempData["TopicId"]);


                Tuple<string, string> res = getSubtopicandAssetTypeData(TopicId);
                gradeName = res.Item1;
                subjectName = res.Item2;

                if (Request.Form["SelectedMoreAssetType"] != null && Request.Form["SelectedMoreAssetType"] != "")
                {

                    var data = new AssetModel();
                    data.AssetId = assetData.AssetId;
                    data.AssetLength = assetData.AssetLength;
                    data.AssetName = assetData.AssetName;
                    data.AssetTypeId = Convert.ToInt32(Request.Form["SelectedMoreAssetType"]);
                    data.Description = assetData.Description;


                    string FileNametoSaveDb = string.Empty;

                    if (assetData.PostedFileLow != null && !string.IsNullOrEmpty(assetData.PostedFileLow.FileName))
                    {
                        FileNametoSaveDb = assetData.PostedFileLow.FileName;
                    }
                    if (assetData.PostedFileMedium != null && !string.IsNullOrEmpty(assetData.PostedFileMedium.FileName))
                    {
                        FileNametoSaveDb = assetData.PostedFileMedium.FileName;
                    }
                    if (assetData.PostedFileHigh != null && !string.IsNullOrEmpty(assetData.PostedFileHigh.FileName))
                    {
                        FileNametoSaveDb = assetData.PostedFileHigh.FileName;
                    }
                    data.FileName = FileNametoSaveDb;
                    if (TempData["isAssetHide"] != null)
                    {
                        data.isAssetHide = Convert.ToBoolean(TempData["isAssetHide"]);
                    }
                    else
                    {
                        data.isAssetHide = true;
                    }

                    if (Request.Form["SelectedMoreScreenSubTopic"] != null && Request.Form["SelectedMoreScreenSubTopic"] != "")
                    {
                        data.SubtopicId = Convert.ToInt32(Request.Form["SelectedMoreScreenSubTopic"]);
                    }
                    else
                    {
                        data.SubtopicId = null;
                    }
                    data.ThumbnailFileName = assetData.PostedFileThumbnail.FileName;
                    data.TopicId = TopicId;

                    string assetId = _assetService.AddAssetInsert(data);

                    Upload(assetId, gradeName, subjectName, assetData.PostedFileHigh, assetData.PostedFileMedium, assetData.PostedFileLow, assetData.PostedFileThumbnail, data.AssetTypeId);

                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return RedirectToAction("GetSubTopics", "SubTopics", new { @id = TopicId });
        }

        private void Upload(string assetId, string gradeName, string subjectName, HttpPostedFileBase postedFileHigh, HttpPostedFileBase postedFileMedium, HttpPostedFileBase postedFileLow, HttpPostedFileBase postedFileThumbnail, int? AssetTypeId)
        {
            try
            {
                if (postedFileHigh != null)
                {
                    uploadFile(postedFileHigh, assetId, gradeName, subjectName, AssetTypeId);
                }
                if (postedFileMedium != null)
                {
                    uploadFile(postedFileMedium, assetId, gradeName, subjectName, AssetTypeId);
                }
                if (postedFileLow != null)
                {
                    uploadFile(postedFileLow, assetId, gradeName, subjectName, AssetTypeId);
                }
                if (postedFileThumbnail != null)
                {
                    uploadFile(postedFileThumbnail, assetId, gradeName, subjectName, AssetTypeId);
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        [HttpGet]
        public ActionResult Create(int id)
        {
            var assetdata = new AssetModel();
            try
            {
                TempData.Remove("isAssetHide");
                TempData["TopicId"] = id;
                TempData.Keep();
                ViewBag.AssetsWithoutSubTopic = _assetService.GetAssetByTopicIdWithoutSubTopic(id);
                AddHeader(id);
                getSTAData(id);
                assetdata.AssetId = Guid.NewGuid();
                assetdata.isAssetHide = true;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return View(assetdata);
        }

        private void AddHeader(int topicId)
        {
            TopicModel tdata = _topicService.GetTopicById(topicId);
            int subjectId = tdata.SubjectId;
            SubjectModel sdata = _subjectService.GetSubjectById(subjectId);
            int gradeId = Convert.ToInt32(sdata.GradeID);
            TopicInitialization(gradeId, subjectId, topicId);
        }

        private void TopicInitialization(int GradeID, int SubId, int topicId)
        {
            var modelSubject = _subjectService.GetSubjectById(SubId);
            string GradeName = _gradeService.GetGradeNameByGradeID(GradeID);
            ViewData["vdWithoutSubGradeName"] = GradeName;
            ViewData["vdWithoutSubSubjectName"] = modelSubject.SubjectName;
            var modelTopic = _topicService.GetTopicById(topicId);
            ViewData["vdWithoutSubTopicName"] = modelTopic.TopicName;

        }

        private void AddHeaderForMore(int topicId, int subTopicId)
        {
            TopicModel tdata = _topicService.GetTopicById(topicId);
            int subjectId = tdata.SubjectId;
            SubjectModel sdata = _subjectService.GetSubjectById(subjectId);
            int gradeId = Convert.ToInt32(sdata.GradeID);
            SubTopicInitialization(gradeId, subjectId, topicId, subTopicId);
        }

        private void SubTopicInitialization(int GradeID, int SubId, int topicId, int subTopicId)
        {
            var modelSubject = _subjectService.GetSubjectById(SubId);
            string GradeName = _gradeService.GetGradeNameByGradeID(GradeID);
            ViewData["vdWithSubTopicGradeName"] = GradeName;
            ViewData["vdWithSubTopicSubjectName"] = modelSubject.SubjectName;
            var modelTopic = _topicService.GetTopicById(topicId);
            ViewData["vdWithSubTopicTopicName"] = modelTopic.TopicName;
            var modelsubtopic = _subtopicservice.GetSubTopicById(subTopicId);
            if (modelsubtopic != null)
            {
                ViewData["vdWithSubTopSubTopicName"] = modelsubtopic.SubtopicName;
            }
            else
            {
                ViewData["vdWithSubTopSubTopicName"] = null;
            }
        }
        [HttpPost]
        public ActionResult Create(AssetModel assetData)
        {
            int TopicId = 0;
            try
            {
                TopicId = Convert.ToInt32(TempData["TopicId"]);
                if (Request.Form["SelectedMoreAssetType"] != null && Request.Form["SelectedMoreAssetType"] != "")
                {
                    var data = new AssetModel();
                    data.AssetId = assetData.AssetId;
                    data.AssetLength = assetData.AssetLength;
                    data.AssetName = assetData.AssetName;
                    data.AssetTypeId = Convert.ToInt32(Request.Form["SelectedMoreAssetType"]);
                    data.Description = assetData.Description;
                    data.FileName = assetData.PostedFileHigh.FileName;
                    if (TempData["isAssetHide"] != null)
                    {
                        data.isAssetHide = Convert.ToBoolean(TempData["isAssetHide"]);
                    }
                    else
                    {
                        data.isAssetHide = true;
                    }
                    if (Request.Form["SelectedMoreScreenSubTopic"] != null && Request.Form["SelectedMoreScreenSubTopic"] != "")
                    {
                        data.SubtopicId = Convert.ToInt32(Request.Form["SelectedMoreScreenSubTopic"]);
                    }
                    else
                    {
                        data.SubtopicId = null;
                    }
                    data.ThumbnailFileName = assetData.PostedFileThumbnail.FileName;
                    data.TopicId = TopicId;
                    string assetId = _assetService.AddAssetInsert(data);
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return RedirectToAction("GetSubTopics", "SubTopics", new { @id = TopicId });
        }
        private void uploadFile(HttpPostedFileBase file, string AssetId, string GradeName, string SubjectName, int? AssetTypeId)
        {
            try
            {

                if (file.ContentLength > 0)
                {
                    string strGradeSubject = GradeName + " " + SubjectName;
                    string core = "Core";
                    var gradeSubjectPath = Path.Combine(Server.MapPath("~/Content/"), strGradeSubject);
                    var corePath = Path.Combine(Server.MapPath("~/Content/" + strGradeSubject), core);

                    if (!(Directory.Exists(gradeSubjectPath)) && !(Directory.Exists(corePath)))
                    {
                        Directory.CreateDirectory(gradeSubjectPath);
                        Directory.CreateDirectory(corePath);
                    }
                    else if ((Directory.Exists(gradeSubjectPath)) && !(Directory.Exists(corePath)))
                    {
                        Directory.CreateDirectory(corePath);
                    }

                    //var path = "";
                    var fileName = Path.GetFileName(file.FileName);
                    if (AssetTypeId == 1) // video
                    {
                        file.SaveAs(Path.Combine(corePath, fileName));
                    }
                    else if (AssetTypeId == 2) // test 
                    {

                    }
                    else if (AssetTypeId == 3 || AssetTypeId == 4) // simulation or interactive
                    {
                        var zipWithoutExtension = fileName.Split('.')[0].ToString();
                        var pathZipFile = Path.Combine(corePath, zipWithoutExtension);
                        var zipPath = Path.Combine(pathZipFile, fileName);
                        using (ZipArchive archive = ZipFile.OpenRead(zipPath))
                        {
                            foreach (ZipArchiveEntry entry in archive.Entries)
                            {
                                TempData["FolderName"] = entry.FullName.Split('/')[0].ToString();

                                if (Regex.IsMatch(entry.Name, @".html$"))
                                {
                                    TempData["FileName"] = entry.FullName;
                                }
                                else if (Regex.IsMatch(entry.Name, @".jpg|.png$"))
                                {
                                    TempData["ImageName"] = entry.Name;
                                }
                            }

                        }
                    }

                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        [HttpGet]
        public ActionResult Edit(Guid id, int topicID)
        {
            AssetModel adata = new AssetModel();
            try
            {
                int TopicId = topicID;
                TempData["TopicId"] = TopicId;
                TempData.Keep();
                ViewBag.AssetsWithoutSubTopic = _assetService.GetAssetByTopicIdWithoutSubTopic(topicID);
                AddHeader(topicID);
                getSTAData(TopicId);
                var assetData = _assetService.GetAssetByAssetId(id);
                adata.AssetId = assetData.AssetId;
                adata.AssetLength = assetData.AssetLength;
                adata.AssetName = assetData.AssetName;
                adata.AssetTypeId = assetData.AssetTypeId;
                adata.Description = assetData.Description;
                adata.FileName = assetData.FileName;
                adata.isAssetHide = assetData.isAssetHide;
                adata.SubtopicId = assetData.SubtopicId;
                TempData["SubtopicId"] = assetData.SubtopicId;
                adata.TopicId = assetData.TopicId;
                adata.isAssetHide = assetData.isAssetHide;
                adata.Version = assetData.Version;
                TempData["Version"] = assetData.Version;
                TempData["isAssetHide"] = assetData.isAssetHide;
                adata.ThumbnailFileName = assetData.ThumbnailFileName;
                adata.FolderName = assetData.FolderName;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return View(adata);
        }
        [HttpGet]
        public ActionResult EditVideo(Guid? id, int topicID)
        {
            AssetModel adata = new AssetModel();
            try
            {
                int TopicId = topicID;
                string subjectName;
                string gradeName;
                TempData["TopicId"] = TopicId;
                TempData.Keep();
                ViewBag.AssetsWithoutSubTopic = _assetService.GetAssetByTopicIdWithoutSubTopic(topicID);
                getSTAData(TopicId);
                Tuple<string, string> res = getSubjectNameandGradeName(TopicId);
                gradeName = res.Item1;
                subjectName = res.Item2;
                var assetData = _assetService.GetAssetByAssetId(id.Value);
                adata.AssetId = assetData.AssetId;
                adata.AssetLength = assetData.AssetLength;
                adata.AssetName = assetData.AssetName;
                adata.AssetTypeId = assetData.AssetTypeId;
                adata.Description = assetData.Description;
                adata.FileName = assetData.FileName;
                adata.SubtopicId = assetData.SubtopicId;
                TempData["SubtopicId"] = assetData.SubtopicId;
                adata.TopicId = assetData.TopicId;
                adata.isAssetHide = assetData.isAssetHide;
                TempData["isAssetHide"] = assetData.isAssetHide;
                adata.ThumbnailFileName = assetData.ThumbnailFileName;
                adata.FolderName = assetData.FolderName;
                adata.Version = assetData.Version;
                TempData["Version"] = assetData.Version;

                AddHeaderForMore(topicID, Convert.ToInt32(assetData.SubtopicId));

                if (assetData.ThumbnailFileName != null)
                {
                    ViewBag.ThumbnailPath = "/Content/" + gradeName + " " + subjectName + "/Core/" + assetData.ThumbnailFileName;
                    var fileName1 = Path.Combine(Server.MapPath("~/Content/" + gradeName + " " + subjectName + "/Core/"), assetData.ThumbnailFileName);
                    FileInfo fi1 = new FileInfo(fileName1);
                    if (fi1.Exists)
                        ViewBag.ThumbnailSize = "(" + ((decimal)fi1.Length / 1024M / 1024M).ToString("F") + "MB)";
                }


                if (assetData.FileName != null)
                {
                    
                    var fileHigh = Path.Combine(Server.MapPath("~/Content/" + gradeName + " " + subjectName + "/Core/"), assetData.FileName.Replace(".mp4","H.mp4"));
                    FileInfo fHigh = new FileInfo(fileHigh);
                    if (fHigh.Exists)
                        ViewBag.fileHSize = 1;
                    else
                        ViewBag.fileHSize = 0;
                    //       ViewBag.fileHSize = "(" + ((decimal)fHigh.Length / 1024M / 1024M).ToString("F") + "MB)";


                    var fileMedium = Path.Combine(Server.MapPath("~/Content/" + gradeName + " " + subjectName + "/Core/"), assetData.FileName.Replace(".mp4","M.mp4"));
                    FileInfo fMedium = new FileInfo(fileMedium);
                    if (fMedium.Exists)
                        ViewBag.fileMSize = 1;
                    else
                        ViewBag.fileMSize = 0;
                    // ViewBag.fileMSize = "(" + ((decimal)fMedium.Length / 1024 / 1024).ToString("F") + "MB)";

                    var fileLow = Path.Combine(Server.MapPath("~/Content/" + gradeName + " " + subjectName + "/Core/"), assetData.FileName.Replace(".mp4","L.mp4"));
                    FileInfo fLow = new FileInfo(fileLow);
                    if (fLow.Exists)
                        ViewBag.fileLSize = 1;
                    else
                        ViewBag.fileLSize = 0;
                    //    ViewBag.fileLSize = "(" + ((decimal)fLow.Length / 1024 / 1024).ToString("F") + "MB)";
                }


            }
            catch (Exception ex)
            {
                throw ex;
            }
            return View(adata);
        }

        [HttpGet]
        public ActionResult EditSimulationorInteractive(Guid? id, int topicID)
        {
            AssetModel adata = new AssetModel();
            try
            {
                int TopicId = topicID;
                string subjectName;
                string gradeName;
                TempData["TopicId"] = TopicId;
                TempData.Keep();
                ViewBag.AssetsWithoutSubTopic = _assetService.GetAssetByTopicIdWithoutSubTopic(topicID);
                getSTAData(TopicId);
                List<AssetTypeModel> assetTypeSimIntData = _assetService.GetSimIntAssetType();
                ViewData["assetTypeSimIntData"] = assetTypeSimIntData;
                Tuple<string, string> res = getSubjectNameandGradeName(TopicId);
                gradeName = res.Item1;
                subjectName = res.Item2;
                var assetData = _assetService.GetAssetByAssetId(id.Value);
                adata.AssetId = assetData.AssetId;
                adata.SuggestedActiveDuration = assetData.SuggestedActiveDuration;
                adata.AssetName = assetData.AssetName;
                adata.AssetTypeId = assetData.AssetTypeId;
                adata.Description = assetData.Description;
                adata.FileName = assetData.FileName;
                adata.SubtopicId = assetData.SubtopicId;
                TempData["SubtopicId"] = assetData.SubtopicId;
                adata.TopicId = assetData.TopicId;
                adata.isAssetHide = assetData.isAssetHide;
                TempData["isAssetHide"] = assetData.isAssetHide;
                adata.ThumbnailFileName = assetData.ThumbnailFileName;
                adata.FolderName = assetData.FolderName;
                adata.Version = assetData.Version;
                TempData["Version"] = assetData.Version;
                // adata.hidden = assetData.hidden;
                AddHeaderForMore(topicID, Convert.ToInt32(assetData.SubtopicId));
                if (assetData.ThumbnailFileName != null)
                {
                    ViewBag.ThumbnailPath = "/Content/" + gradeName + " " + subjectName + "/Core/" + assetData.FolderName + "/" + assetData.ThumbnailFileName;
                }
                if (assetData.FolderName != null)
                {
                    var fileHigh = Path.Combine(Server.MapPath("~/Content/" + gradeName + " " + subjectName + "/Core/" + assetData.FolderName + "/"), assetData.FolderName);
                    fileHigh = fileHigh + ".zip";
                    ViewBag.FileZip = assetData.FolderName + ".zip";
                    FileInfo fHigh = new FileInfo(fileHigh);
                    if (fHigh.Exists)
                        ViewBag.fileHSize = "(" + ((decimal)fHigh.Length / 1024M / 1024M).ToString("F") + "MB)";
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }

            return View(adata);
        }

        [HttpPost]
        public ActionResult EditSimulationorInteractive(AssetModel assetData)
        {
            string gradeName;
            string subjectName;
            int TopicId = 0;
            try
            {
                TopicId = Convert.ToInt32(assetData.TopicId);
                Tuple<string, string> res = getSubtopicandAssetTypeData(TopicId);
                gradeName = res.Item1;
                subjectName = res.Item2;

                var data = new AssetModel();
                data.AssetId = assetData.AssetId;
                data.SuggestedActiveDuration = assetData.SuggestedActiveDuration;
                data.AssetName = assetData.AssetName;
                if (Request.Form["SelectedSimIntAssetType"] != null && Request.Form["SelectedSimIntAssetType"] != "")
                {
                    data.AssetTypeId = Convert.ToInt32(Request.Form["SelectedSimIntAssetType"]);
                }
                else
                {
                    data.AssetTypeId = Convert.ToInt32(assetData.AssetTypeId);
                }
                data.Description = assetData.Description;

                if (TempData["isAssetHide"] != null)
                {
                    //data.isAssetHide = Convert.ToBoolean(assetData.hidden);
                    data.isAssetHide = Convert.ToBoolean(TempData["isAssetHide"]);
                }
                else
                {
                    data.isAssetHide = true;
                }
                if (TempData["Version"] != null)
                {
                    data.Version = Convert.ToInt32(TempData["Version"]);
                }
                else
                {
                    data.Version = 0;
                }
                if (Request.Form["SelectedMoreScreenSubTopic"] != null && Request.Form["SelectedMoreScreenSubTopic"] != "")
                {
                    data.SubtopicId = Convert.ToInt32(Request.Form["SelectedMoreScreenSubTopic"]);
                }
                else
                {
                    data.SubtopicId = null;
                }
                if (assetData.PostedFileZip != null)
                {
                    uploadFile(assetData.PostedFileZip, assetData.AssetId.ToString(), gradeName, subjectName, assetData.AssetTypeId);
                    data.FileName = TempData["FileName"].ToString();
                    data.ThumbnailFileName = TempData["ImageName"].ToString();
                    data.FolderName = TempData["FolderName"].ToString();
                }
                else
                {
                    data.FileName = assetData.FileName;
                    data.ThumbnailFileName = assetData.ThumbnailFileName;
                    data.FolderName = assetData.FolderName;
                }
                data.TopicId = TopicId;
                _assetService.UpdateAsset(data);
            }
            catch (Exception ex)
            {
                //throw ex;
            }
            return RedirectToAction("GetSubTopics", "SubTopics", new { @id = TopicId });

        }

        [HttpPost]
        public ActionResult Edit(AssetModel assetData)
        {
            string gradeName;
            string subjectName;
            int TopicId = 0;
            try
            {
                TopicId = Convert.ToInt32(TempData["TopicId"]);
                Tuple<string, string> res = getSubtopicandAssetTypeData(TopicId);
                gradeName = res.Item1;
                subjectName = res.Item2;
                if (Request.Form["SelectedMoreScreenSubTopic"] != null && Request.Form["SelectedMoreScreenSubTopic"] != "")
                {
                    assetData.SubtopicId = Convert.ToInt32(Request.Form["SelectedMoreScreenSubTopic"]);
                }
                else
                {
                    assetData.SubtopicId = null;
                }

                //if (assetData.PostedFileHigh != null)
                //{
                //    assetData.FileName = assetData.PostedFileHigh.FileName;
                //}
                string FileNametoSaveDb = string.Empty;

                if (assetData.PostedFileLow != null && !string.IsNullOrEmpty(assetData.PostedFileLow.FileName))
                {
                    FileNametoSaveDb = assetData.PostedFileLow.FileName;
                }
                if (assetData.PostedFileMedium != null && !string.IsNullOrEmpty(assetData.PostedFileMedium.FileName))
                {
                    FileNametoSaveDb = assetData.PostedFileMedium.FileName;
                }
                if (assetData.PostedFileHigh != null && !string.IsNullOrEmpty(assetData.PostedFileHigh.FileName))
                {
                    FileNametoSaveDb = assetData.PostedFileHigh.FileName;
                }
                if (FileNametoSaveDb == null && string.IsNullOrEmpty(FileNametoSaveDb.Trim()))
                {
                    FileNametoSaveDb = assetData.FileName;
                }
                

                if (assetData.PostedFileThumbnail != null)
                {
                    assetData.ThumbnailFileName = assetData.PostedFileThumbnail.FileName;
                }
                assetData.TopicId = TopicId;
                if (TempData["isAssetHide"] != null)
                {
                    assetData.isAssetHide = Convert.ToBoolean(TempData["isAssetHide"]);
                }
                else
                {
                    assetData.isAssetHide = true;
                }
                if (TempData["Version"] != null)
                {
                    assetData.Version = Convert.ToInt32(TempData["Version"]);
                }
                else
                {
                    assetData.Version = 0;
                }
                if (assetData.PostedFileZip != null)
                {
                    _assetService.UpdateAsset(assetData);
                    uploadFile(assetData.PostedFileZip, assetData.AssetId.ToString(), gradeName, subjectName, assetData.AssetTypeId);
                }
                else
                {
                    _assetService.UpdateAsset(assetData);

                }                
                    
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return RedirectToAction("GetSubTopics", "SubTopics", new { @id = TopicId });
        }

        [HttpPost]
        public ActionResult SaveForm(AssetModel assetData)
        {
            string gradeId;
            string subjectId;
            int TopicId = 0;
            try
            {
                TopicId = Convert.ToInt32(TempData["TopicId"]);

                Tuple<string, string> res = getSubtopicandAssetTypeData(TopicId);
                gradeId = res.Item1;
                subjectId = res.Item2;
                if (Request.Form["SelectedSubTopic"] != null && Request.Form["SelectedSubTopic"] != "")
                {
                    assetData.SubtopicId = Convert.ToInt32(Request.Form["SelectedSubTopic"]);
                }
                else
                {
                    assetData.SubtopicId = null;
                }

                if (assetData.SubtopicId != Convert.ToInt32(TempData["SubtopicId"]))
                {
                    int oldSubtopiId = Convert.ToInt32(TempData["SubtopicId"]);
                }

                if (Request.Form["SelectedAssetType"] != null && Request.Form["SelectedAssetType"] != "")
                {
                    assetData.AssetTypeId = Convert.ToInt32(Request.Form["SelectedAssetType"]);
                }
                else
                {
                    assetData.AssetTypeId = null;
                }
                if (TempData["isAssetHide"] != null)
                {
                   
                    assetData.isAssetHide = Convert.ToBoolean(TempData["isAssetHide"]);
                }
                else
                {
                    assetData.isAssetHide = true;
                }
                if (TempData["Version"] != null)
                {
                    assetData.Version = Convert.ToInt32(TempData["Version"]);
                }
                else
                {
                    assetData.Version = 0;
                }

                assetData.TopicId = TopicId;
                _assetService.UpdateAsset(assetData);

            }
            
            catch (Exception ex)
            {
                throw ex;
            }
            return RedirectToAction("GetSubTopics", "SubTopics", new { @id = TopicId });
        }

        private void DeleteDirectory(string subPath)
        {
            try
            {
                // Delete all files from the Directory
                foreach (string filename in Directory.GetFiles(subPath))
                {
                    System.IO.File.Delete(filename);
                }
                // Check all child Directories and delete files
                foreach (string subfolder in Directory.GetDirectories(subPath))
                {
                    DeleteDirectory(subfolder);
                }
                Directory.Delete(subPath);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void setAssetHide(int id)
        {
            try
            {
                TempData["isAssetHide"] = id;
                TempData.Keep();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }


        [HttpPost]
        public string GetZipFileInfo(HttpPostedFileBase filesInput, Guid? AssetId, int AssetTypeId, int? TopicId)
        {
            string subjectName;
            string gradeName;
            string imgName = string.Empty;
            string extractPath = string.Empty;
            try
            {
                Tuple<string, string> res = getSubjectNameandGradeName(Convert.ToInt32(TopicId));
                gradeName = res.Item1;
                subjectName = res.Item2;
                var fileName = Path.GetFileName(filesInput.FileName);
                string strGradeSubject = gradeName + " " + subjectName;
                string core = "Core";
                var gradeSubjectPath = Path.Combine(Server.MapPath("~/Content/"), strGradeSubject);
                var corePath = Path.Combine(Server.MapPath("~/Content/" + strGradeSubject), core);
                if (!(Directory.Exists(gradeSubjectPath)) && !(Directory.Exists(corePath)))
                {
                    Directory.CreateDirectory(gradeSubjectPath);
                    Directory.CreateDirectory(corePath);
                }
                else if ((Directory.Exists(gradeSubjectPath)) && !(Directory.Exists(corePath)))
                {
                    Directory.CreateDirectory(corePath);
                }
                var zipWithoutExtension = fileName.Split('.')[0].ToString();
                var pathZipFile = Path.Combine(corePath, zipWithoutExtension);
                DeleteDirectory(pathZipFile);
                if (!(Directory.Exists(pathZipFile)))
                {
                    Directory.CreateDirectory(pathZipFile);
                }
                var path = Path.Combine(pathZipFile, fileName);
                filesInput.SaveAs(path);
                string zipPath = path;
                var fPath = pathZipFile + "/" + filesInput.FileName;
                ZipFile.ExtractToDirectory(zipPath, corePath);
                using (ZipArchive archive = ZipFile.OpenRead(zipPath))
                {
                    foreach (ZipArchiveEntry entry in archive.Entries)
                    {
                        if (Regex.IsMatch(entry.Name, @".jpg|.png$"))
                        {
                            imgName = "/Content/" + strGradeSubject + "/Core/" + zipWithoutExtension + "/" + entry.Name;

                        }
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return imgName;

        }

        [AllowAnonymous]
        public JsonResult IsAssetIdValid(Guid? AssetId)
        {
            try
            {
                Guid guidOutput;
                bool isValid = Guid.TryParse(AssetId.ToString(), out guidOutput);

                if (!isValid)
                {
                    return Json("Asset Id is not valid Guid", JsonRequestBehavior.AllowGet);
                }
                else
                {
                    return Json(true, JsonRequestBehavior.AllowGet);
                }
            }
            catch (Exception ex)
            {
            }
            return Json(true, JsonRequestBehavior.AllowGet);
        }
        [HttpGet]
        public ActionResult CreateTest(int id)
        {
            AssetModel ad = new AssetModel();
            try
            {
                TempData["TopicId"] = id;
                TempData.Keep();
                int topicID = Convert.ToInt32(TempData["TopicId"]);
                getSTAData(topicID);
                ad = (AssetModel)TempData["AssetData"];
                AddHeaderForMore(topicID, Convert.ToInt32(ad.SubtopicId));
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return View(ad);
        }


        [HttpPost]
        // public string GetTestZipFileInfo(HttpPostedFileBase filesInput)
        public string GetTestZipFileInfo(HttpPostedFileBase filesInput, Guid? AssetId, int AssetTypeId, int? TopicId)
        {
            string noOfQuestions = string.Empty;
            string extractPath = string.Empty;
            string subjectName;
            string gradeName;
            try
            {
                Tuple<string, string> res = getSubjectNameandGradeName(Convert.ToInt32(TopicId));
                gradeName = res.Item1;
                subjectName = res.Item2;
                var fileName = Path.GetFileName(filesInput.FileName);
                string strGradeSubject = gradeName + " " + subjectName;
                string core = "Core";
                var gradeSubjectPath = Path.Combine(Server.MapPath("~/Content/"), strGradeSubject);
                var corePath = Path.Combine(Server.MapPath("~/Content/" + strGradeSubject), core);
                if (!(Directory.Exists(gradeSubjectPath)) && !(Directory.Exists(corePath)))
                {
                    Directory.CreateDirectory(gradeSubjectPath);
                    Directory.CreateDirectory(corePath);
                }
                else if ((Directory.Exists(gradeSubjectPath)) && !(Directory.Exists(corePath)))
                {
                    Directory.CreateDirectory(corePath);
                }
                var zipWithoutExtension = fileName.Split('.')[0].ToString();

                var path = Path.Combine(corePath, fileName);
                filesInput.SaveAs(path);
                string zipPath = path;

                var fPath = corePath + "/" + zipWithoutExtension;

                DeleteDirectory(fPath);
                ZipFile.ExtractToDirectory(zipPath, corePath);


                using (ZipArchive archive = ZipFile.OpenRead(zipPath))
                {
                    foreach (ZipArchiveEntry entry in archive.Entries)
                    {
                        if (Regex.IsMatch(entry.Name, @".xml$"))
                        {
                            var xmlName = corePath + "/" + zipWithoutExtension + "/" + entry.Name;
                            XmlDocument xdoc = new XmlDocument();
                            xdoc.Load(xmlName);
                            foreach (XmlNode node in xdoc.ChildNodes)
                            {
                                if (node.InnerXml != "")
                                {
                                    // string text = node.InnerText; //or loop through its children as well
                                    noOfQuestions = node.Attributes["NoofQuestions"]?.InnerText;
                                }
                            }

                        }
                    }
                }

            }
            catch (Exception ex)
            {
                throw ex;
            }
            return noOfQuestions;

        }

        [HttpPost]
        public ActionResult CreateTest(AssetModel assetData)
        {
            int TopicId = 0;
            try
            {
                TopicId = Convert.ToInt32(TempData["TopicId"]);
                var data = new AssetModel();
                data.AssetId = assetData.AssetId;
                data.SuggestedActiveDuration = assetData.AssetLength;
                data.AssetName = assetData.AssetName;
                data.AssetTypeId = assetData.AssetTypeId;
                data.Description = assetData.Description;
                data.FileName = assetData.PostedFileZip.FileName;
                if (TempData["isAssetHide"] != null)
                {
                    data.isAssetHide = Convert.ToBoolean(TempData["isAssetHide"]);
                }
                else
                {
                    data.isAssetHide = true;
                }

                if (Request.Form["SelectedMoreScreenSubTopic"] != null && Request.Form["SelectedMoreScreenSubTopic"] != "")
                {
                    data.SubtopicId = Convert.ToInt32(Request.Form["SelectedMoreScreenSubTopic"]);
                }
                else
                {
                    data.SubtopicId = null;
                }

                data.TopicId = TopicId;
                string assetId = _assetService.AddAssetInsert(data);
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return RedirectToAction("GetSubTopics", "SubTopics", new { @id = TopicId });
        }

        [HttpPost]
        public string DeleteZipFile(HttpPostedFileBase filesInput, Guid? AssetId, int AssetTypeId, int? TopicId)
        {
            string subjectName;
            string gradeName;
            try
            {
                Tuple<string, string> res = getSubjectNameandGradeName(Convert.ToInt32(TopicId));
                gradeName = res.Item1;
                subjectName = res.Item2;
                var fileName = Path.GetFileName(filesInput.FileName);
                string strGradeSubject = gradeName + " " + subjectName;
                string core = "Core";
                var gradeSubjectPath = Path.Combine(Server.MapPath("~/Content/"), strGradeSubject);
                var corePath = Path.Combine(Server.MapPath("~/Content/" + strGradeSubject), core);
                if (!(Directory.Exists(gradeSubjectPath)) && !(Directory.Exists(corePath)))
                {
                    Directory.CreateDirectory(gradeSubjectPath);
                    Directory.CreateDirectory(corePath);
                }
                else if ((Directory.Exists(gradeSubjectPath)) && !(Directory.Exists(corePath)))
                {
                    Directory.CreateDirectory(corePath);
                }
                if (AssetTypeId == 1) // video
                {

                }
                else if (AssetTypeId == 2) // Test
                {
                    var zipWithoutExtension = fileName.Split('.')[0].ToString();
                    var pathZipFile = Path.Combine(corePath, zipWithoutExtension);
                    DeleteDirectory(pathZipFile);
                    // var fPath = corePath + "/" + filesInput.FileName;
                    var fPath = Path.Combine(corePath, filesInput.FileName);
                    System.IO.File.Delete(fPath);

                }
                else if (AssetTypeId == 3 || AssetTypeId == 4) // Sim/Int
                {
                    var zipWithoutExtension = fileName.Split('.')[0].ToString();
                    var pathZipFile = Path.Combine(corePath, zipWithoutExtension);
                    DeleteDirectory(pathZipFile);
                }

            }
            catch (Exception ex)
            {
                throw ex;
            }
            return "Success";

        }


        public ActionResult Delete(Guid? AssetId, int topicId)
        {
            try
            {
                //_subTopicService.DeleteSubTopic(subTopicId);
                _assetService.DeleteAsset(AssetId.Value);
            }
            catch (Exception ex)
            {
                throw ex;
            }

            return RedirectToAction("GetSubTopics", "SubTopics", new { @id = topicId });
        }


    }
}