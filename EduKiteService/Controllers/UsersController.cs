﻿using AutoMapper;
using EdukiteService.Business.Entities;
using EdukiteService.Business.Interface;
using EdukiteService.Utilities;
using EduKiteService.Business.Interface;
//using EduKiteService.Business.Interface;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web;
using System.Web.Mvc;
using EduKiteService.Models;
using EdukiteService.Data;
using EduKiteService.ViewModels;
using System.Threading.Tasks;
using System.IO;
using Excel = Microsoft.Office.Interop.Excel;
using System.Data;
using System.Data.OleDb;
using System.Configuration;
using System.Data.SqlClient;
//using Edukite.Bussiness;

namespace EduKiteService.Controllers
{

    public class UsersController : Controller
    {
        Edukite_AndroidEntities db = new Edukite_AndroidEntities();
        // GET: Users
        private readonly IUsersServices _UsersServices;
        private readonly IProvinceService _ProvinceService;
        //private readonly ISubscriptionService _subscriptionService;
        //private readonly IGradeService _GradeService;

        public UsersController(IUsersServices UsersServices, IProvinceService ProvinceService)//,,ISubscriptionService SubscriptionService
        {
            _UsersServices = UsersServices;
            _ProvinceService = ProvinceService;
            //_subscriptionService = SubscriptionService;
            //_GradeService = GradeService;

            using (Edukite_AndroidEntities ut = new Edukite_AndroidEntities())
            {
                var UserType = new SelectList(ut.UserTypes.ToList(), "UserTypeId", "UserType1");
                ViewData["UserType"] = UserType.ToList();
            }

            using (Edukite_AndroidEntities prov = new Edukite_AndroidEntities())
            {
                var Province = new SelectList(prov.Provinces.ToList(), "ProvinceId", "ProvinceName");
                ViewData["Province"] = Province.ToList();
            }

            using (Edukite_AndroidEntities grd = new Edukite_AndroidEntities())
            {
                var Grade = new SelectList(grd.Grades.ToList(), "GradeId", "GradeName");
                ViewData["Grade"] = Grade.ToList();
            }

            using (Edukite_AndroidEntities sub = new Edukite_AndroidEntities())
            {
                var Subject = new SelectList(sub.Subjects.ToList(), "SubjectId", "SubjectName");
                ViewData["Subject"] = Subject.ToList();
            }
            using (Edukite_AndroidEntities ug = new Edukite_AndroidEntities())
            {
                var UserGroup = new SelectList(ug.UserGroups.ToList(), "UserGroupId", "UserGroupName");
                ViewData["UserGroup"] = UserGroup.ToList();
            }
            using (Edukite_AndroidEntities Cellpcode = new Edukite_AndroidEntities())
            {
                var CellphoneCode = new SelectList(Cellpcode.CellphoneCodes.ToList(), "Id", "MobileCode");
                ViewData["CellphoneCode"] = CellphoneCode.ToList();
            }
            using (Edukite_AndroidEntities Pack = new Edukite_AndroidEntities())
            {
                var SubsType = new SelectList(Pack.SubscriptionTypes.ToList(), "SubscriptionTypeId", "SubscriptionType1");
                ViewData["Package"] = SubsType.ToList();
            }
        }
        public ActionResult LoginPage(string UserName, string Password)
        {
            this.Session["User"] = "";
            string adminuser = "Admin";
            string adminpass = "Admin@kft";
            string superadminuser = "SuperAdmin";
            string superadminpass = "SuperAdmin@kft";
            if (UserName == adminuser && Password == adminpass)
            {
                // var claimsIdentity = new ClaimsIdentity(DefaultAuthenticationTypes.ApplicationCookie);
                Response.Cookies.Add(new HttpCookie("mTrainerUserName", "Admin"));
                Response.Cookies.Add(new HttpCookie("mTrainerEmail", "Admin"));
                Response.Cookies.Add(new HttpCookie("mTrainerRegionId", "0"));
                //Request.GetOwinContext().Authentication.SignIn(claimsIdentity);
                //return Json(new APIMessage { StatusCode = (int)HttpStatusCode.OK, StatusMessage = "SUCCESS" }, JsonRequestBehavior.AllowGet);
                //return View("LoginPage"); //LoginPage
                this.Session["User"] = UserName;
                //ViewData["vdLoginPage"] = true;
                return RedirectToAction("GetGrades", "Grades");
            }
            else if (UserName == superadminuser && Password == superadminpass)
            {
                // var claimsIdentity = new ClaimsIdentity(DefaultAuthenticationTypes.ApplicationCookie);
                Response.Cookies.Add(new HttpCookie("mTrainerUserName", "Admin"));
                Response.Cookies.Add(new HttpCookie("mTrainerEmail", "Admin"));
                Response.Cookies.Add(new HttpCookie("mTrainerRegionId", "0"));
                //Request.GetOwinContext().Authentication.SignIn(claimsIdentity);
                //return Json(new APIMessage { StatusCode = (int)HttpStatusCode.OK, StatusMessage = "SUCCESS" }, JsonRequestBehavior.AllowGet);
                //return View("LoginPage"); //LoginPage
                this.Session["User"] = UserName;
                //ViewData["vdLoginPage"] = true;
                return Redirect("/Users/SuperAdmin");
            }
            else
            {
                ViewData["vdLoginPage"] = true;
                return View("LoginPage");
            }

        }
        public ActionResult Logout()
        {
            return View();
        }
        public ActionResult SuperAdmin()
        {
            ViewData["vdSuperAdmin"] = true;
            return View();
        }
        public ActionResult UserDashbord()
        {
            ViewData["vdUserDashbord"] = true;
            return View();
        }
        public ActionResult AddUser()
        {
           
            ViewData["vdAddUser"] = true;
            return View("AddUser");
        }
        public ActionResult BulkUserUpload()
        {
            ViewData["vdBulkUserUpload"] = true;
            return View("BulkUserUpload");
        }
        public ActionResult GetData()
        {
            List<getUserModel> UserModels2 = _UsersServices.GetUsers();
            return Json(new { rows = UserModels2 }, JsonRequestBehavior.AllowGet);
        }
        public ActionResult SearchingUsers()
        {
            ViewData["vdSearchingUsers"] = true;
            List<getUserModel> UserModels2 = _UsersServices.GetUsers();  
            
            
            return View(UserModels2);
        }
        [HttpGet]
        public ActionResult EditUsers(int userid)
        {

            try
            {
                ViewData["vdEditUsers"] = true;
                int UserId = userid;
                TempData["UserId"] = UserId;
                TempData.Keep();

                var UserData = _UsersServices.GetUserByUserID(userid);
                var UserDispay = new getUserModel();

                UserDispay.UserId = UserData.UserId;
                UserDispay.FirstName = UserData.FirstName;
                UserDispay.LastName = UserData.LastName;
                UserDispay.Gender = UserData.Gender;
                UserDispay.dob = UserData.dob;
                UserDispay.ContactNo = UserData.ContactNo;
                UserDispay.TelephoneNo = UserData.TelephoneNo;
                UserDispay.EmailId = UserData.EmailId;
                UserDispay.City = UserData.City;
                UserDispay.ProvinceId = UserData.ProvinceId;
                UserDispay.GradeID = UserData.GradeID;
                UserDispay.UserTypeId = UserData.UserTypeId;
                UserDispay.SchoolId = UserData.SchoolId;
                UserDispay.TelephoneCode = UserData.TelephoneCode;
                UserDispay.TelephoneNo = UserData.TelephoneNo;
                //UserDispay.SchoolId = UserData.SchoolId;
                UserDispay.FavSubID = UserData.FavSubID;
                UserDispay.P_Name = UserData.P_Name;
                UserDispay.P_EmailId = UserData.P_EmailId;
                UserDispay.P_CellphoneCode = UserData.P_CellphoneCode;
                UserDispay.P_CellPhoneNo = UserData.P_CellPhoneNo;
                UserDispay.P_TelephoneCode = UserData.P_TelephoneCode;
                UserDispay.P_TelePhoneNo = UserData.P_TelePhoneNo;
                UserDispay.IsActive = UserData.IsActive;

                ViewBag.UserData = _UsersServices.GetUsers().ToList();
                return View(UserDispay);


            }

            catch (Exception ex)
            {
                throw ex;
            }

        }

        [HttpPost]
        public ActionResult Update(getUserModel userData)
        {
            try
            {
                _UsersServices.UpdateUser(userData);

                ViewBag.userData = _UsersServices.GetUsers().ToList();
            }
            catch (Exception ex)
            {
                throw ex;
            }
            // return ("SearchingUsers")
            return RedirectToAction("SearchingUsers", "Users");

        }

        public ActionResult DeletUser(int userid)
        {
            try
            {
                _UsersServices.DeletedeactiveUser(userid);                              
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return View("SearchingUsers");
           
        }
        public ActionResult SUBSCRIPTIONS()
        {

            ViewData["vdSUBSCRIPTIONS"] = true;
            return View("SUBSCRIPTIONS");
        }
        public ActionResult EditUsers()
        {
            return View("EditUsers");
        }
        ////private readonly ICountryService _CountryService;

        //public UsersController(IProvinceService ProvinceService)
        //{
        //    try
        //    {
        //        _ProvinceService = ProvinceService;
        //       // _CountryService = CountryService;
        //    }
        //    catch(Exception ex)
        //    {

        //    }
        //}

        [HttpGet]
        [Route("GetUsers")]
        public JsonResult GetUsers()
        {
            var provinces = _ProvinceService.GetProvince();
            return Json(new APIMessage { StatusCode = (int)HttpStatusCode.OK, StatusMessage = Constants.SuccessMessage, Data = provinces }, JsonRequestBehavior.AllowGet);
        }
        /* [HttpGet]
         [Route("GetCountryCode")]
         public JsonResult GetCountryCode()
         {
             var countrycode = _CountryService.GetCountry();
             return Json(new APIMessage { StatusCode = (int)HttpStatusCode.OK, StatusMessage = Constants.SuccessMessage, Data = countrycode }, JsonRequestBehavior.AllowGet);
         }*/

        public JsonResult IsUsersExist(string FirstName, string initialFirstName)
        {
            try
            {
                if (FirstName == initialFirstName)
                {
                    return Json(true, JsonRequestBehavior.AllowGet);
                }
                var data = _UsersServices.GetUserByName(FirstName);

                if (data != null)
                {
                    return Json("Users name already exists", JsonRequestBehavior.AllowGet);
                }
                else
                {
                    return Json(true, JsonRequestBehavior.AllowGet);
                }
            }
            catch (Exception ex)
            {

            }
            return Json(true, JsonRequestBehavior.AllowGet);
        }

        public JsonResult IsLastNameExist(string LastName, string initialLastName)
        {
            try
            {
                if (LastName == initialLastName)
                {
                    return Json(true, JsonRequestBehavior.AllowGet);
                }
                var data1 = _UsersServices.GetUserByLastName(LastName);

                if (data1 != null)
                {
                    return Json("Users name already exists", JsonRequestBehavior.AllowGet);
                }
                else
                {
                    return Json(true, JsonRequestBehavior.AllowGet);
                }
            }
            catch (Exception ex)
            {

            }
            return Json(true, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult gradesection(int id)
        {
            getUserModel dtuser = new getUserModel();
            try
            {
               
                var UserData = _UsersServices.GetUserByUserID(id);
               
                return View("EditUsers");
            }

            catch (Exception ex)
            {
                throw ex;
            }

        }

        //[HttpPost]
        //public ActionResult AddUser(UserViewModel userview)
        //{

        //    try
        //    {

        //        User user = new User();

        //        user.UserTypeId = userview.UserTypeId;
        //        user.EmailId = userview.EmailId;
        //        user.FirstName = userview.FirstName;
        //        user.LastName = userview.LastName;
        //        user.CellphoneCode = userview.CellphoneCode;
        //        user.ContactNo = userview.ContactNo;
        //        user.TelephoneCode = userview.TelephoneCode;
        //        user.TelephoneNo = userview.TelephoneNo;
        //        user.Gender = userview.Gender;
        //        user.dob = userview.dob;
        //        user.City = userview.City;
        //        user.ProvinceId = userview.ProvinceId;
        //        user.GradeID = userview.GradeID;
        //        //user.SchoolId = userview.SchoolId;
        //        user.FavSubID = userview.FavSubID;
        //        user.P_Name = userview.P_Name;
        //        user.P_EmailId = userview.P_EmailId;
        //        user.P_CellphoneCode = userview.P_CellphoneCode;
        //        user.P_CellPhoneNo = userview.P_CellPhoneNo;
        //        user.P_TelephoneCode = userview.P_TelephoneCode;
        //        user.P_TelePhoneNo = userview.P_TelePhoneNo;


        //        db.Users.Add(user);
        //        db.SaveChanges();

        //        int SubsUserID = user.UserId;

        //        Subscription subscription = new Subscription();

        //        if(userview.UserTypeId.ToString() == "4" || userview.UserTypeId.ToString() == "5")
        //        {
        //            subscription.UserId = SubsUserID;
        //            subscription.UserGroupId = userview.UserGroup;
        //            //subscription.GradeId = 0;
        //            subscription.SubscribedDatetime = null;
        //            subscription.PromoCodeId = null;
        //            subscription.SubscriptionTypeId = userview.Package;
        //            subscription.StartDatetime = null;
        //            subscription.EndDatetime = null;
        //            subscription.IsPaid = null;
        //            subscription.CreatedDateTime = DateTime.Now;
        //        }
        //        else
        //        {
        //            subscription.UserId = SubsUserID;
        //            subscription.UserGroupId = userview.UserGroup;
        //            //subscription.GradeId = 0;
        //            subscription.SubscribedDatetime = null;
        //            subscription.PromoCodeId = null;
        //            subscription.SubscriptionTypeId = userview.Package;
        //            subscription.StartDatetime = userview.StartDatetime;
        //            subscription.EndDatetime = userview.EndDatetime;
        //            subscription.IsPaid = null;
        //            subscription.CreatedDateTime = DateTime.Now;
        //        }



        //        db.Subscriptions.Add(subscription);
        //        db.SaveChanges();

        //        UserGradeMapping USGM = new UserGradeMapping();

        //        USGM.userId = SubsUserID;
        //        USGM.gradeId = userview.GradeID;

        //        db.UserGradeMappings.Add(USGM);
        //        db.SaveChanges();
        //    }
        //    catch (Exception ex)
        //    {
        //        throw ex;
        //        // "~//UserDashbord/"
        //    }
        //    return RedirectToAction("SearchingUsers", "Users");

        //}

        [HttpPost]
        public ActionResult AddUser(UserViewModel userview)
        {

            try
            {

                User user = new User();

                user.UserTypeId = userview.UserTypeId;
                user.EmailId = userview.EmailId;
                user.FirstName = userview.FirstName;
                user.LastName = userview.LastName;
                user.CellphoneCode = userview.CellphoneCode;
                user.ContactNo = userview.ContactNo;
                user.TelephoneCode = userview.TelephoneCode;
                user.TelephoneNo = userview.TelephoneNo;
                user.Gender = userview.Gender;
                user.dob = userview.dob;
                user.City = userview.City;
                user.ProvinceId = userview.ProvinceId;
                user.GradeID = userview.GradeID;
                //user.SchoolId = userview.SchoolId;
                user.FavSubID = userview.FavSubID;
                user.P_Name = userview.P_Name;
                user.P_EmailId = userview.P_EmailId;
                user.P_CellphoneCode = userview.P_CellphoneCode;
                user.P_CellPhoneNo = userview.P_CellPhoneNo;
                user.P_TelephoneCode = userview.P_TelephoneCode;
                user.P_TelePhoneNo = userview.P_TelePhoneNo;


                db.Users.Add(user);
                db.SaveChanges();

                int SubsUserID = user.UserId;

                Subscription subscription = new Subscription();

                if (userview.UserTypeId.ToString() == "4" || userview.UserTypeId.ToString() == "5")
                {
                    var test = ViewBag.UserMultGrade;
                    subscription.UserId = SubsUserID;
                    subscription.UserGroupId = userview.UserGroup;
                    //subscription.GradeId = 0;
                    subscription.SubscribedDatetime = null;
                    subscription.PromoCodeId = null;
                    subscription.SubscriptionTypeId = userview.Package;
                    subscription.StartDatetime = null;
                    subscription.EndDatetime = null;
                    subscription.IsPaid = null;
                    subscription.CreatedDateTime = DateTime.Now;
                }
                else
                {
                    subscription.UserId = SubsUserID;
                    subscription.UserGroupId = userview.UserGroup;
                    //subscription.GradeId = 0;
                    subscription.SubscribedDatetime = null;
                    subscription.PromoCodeId = null;
                    subscription.SubscriptionTypeId = userview.Package;
                    subscription.StartDatetime = userview.StartDatetime;
                    subscription.EndDatetime = userview.EndDatetime;
                    subscription.IsPaid = null;
                    subscription.CreatedDateTime = DateTime.Now;
                }



                db.Subscriptions.Add(subscription);
                db.SaveChanges();

                UserGradeMapping USGM = new UserGradeMapping();

                USGM.userId = SubsUserID;
                USGM.gradeId = userview.GradeID;

                db.UserGradeMappings.Add(USGM);
                db.SaveChanges();
            }
            catch (Exception ex)
            {
                throw ex;
                // "~//UserDashbord/"
            }
            return RedirectToAction("SearchingUsers", "Users");

        }
        [HttpPost]
        public async Task<ActionResult> BulkUserUpload(BulkUserModel bulkUserModelcombi)
        {
            try
            {
                ////int usertypeid = 0;
                HttpPostedFileBase postedFile = bulkUserModelcombi.excelFile;
                
                string filePath = string.Empty;
                if (postedFile != null)
                {
                    string path = Server.MapPath("~/Content/UserUpload/");
                    if (!Directory.Exists(path))
                    {
                        Directory.CreateDirectory(path);
                    }

                    filePath = path + Path.GetFileName(postedFile.FileName);
                    string extension = Path.GetExtension(postedFile.FileName);
                    postedFile.SaveAs(filePath);

                    string conString = string.Empty;
                    switch (extension)
                    {
                        case ".xls": //Excel 97-03.
                            conString = ConfigurationManager.ConnectionStrings["Excel03ConString"].ConnectionString;
                            break;
                        case ".xlsx": //Excel 07 and above.
                            conString = ConfigurationManager.ConnectionStrings["Excel07ConString"].ConnectionString;
                            break;
                    }

                    DataTable dt = new DataTable();
                    conString = string.Format(conString, filePath);

                    using (OleDbConnection connExcel = new OleDbConnection(conString))
                    {
                        using (OleDbCommand cmdExcel = new OleDbCommand())
                        {
                            using (OleDbDataAdapter odaExcel = new OleDbDataAdapter())
                            {
                                cmdExcel.Connection = connExcel;

                                //Get the name of First Sheet.
                                connExcel.Open();
                                DataTable dtExcelSchema;
                                dtExcelSchema = connExcel.GetOleDbSchemaTable(OleDbSchemaGuid.Tables, null);
                                string sheetName = dtExcelSchema.Rows[0]["TABLE_NAME"].ToString();
                                connExcel.Close();

                                //Read Data from First Sheet.
                                connExcel.Open();
                                cmdExcel.CommandText = "SELECT * From [" + sheetName + "]";
                                odaExcel.SelectCommand = cmdExcel;
                                odaExcel.Fill(dt);
                                connExcel.Close();
                            }
                        }
                    }
                    // return RedirectToAction("");
                    //Insert records to database table.
                    Edukite_AndroidEntities entities = new Edukite_AndroidEntities();

                    foreach (DataRow row in dt.Rows)
                    {
                        User user = new User();
                        if (row["UserType"].ToString() != null)
                        {
                            if (row["UserType"].ToString() == "Learner")
                            {
                                user.UserTypeId = 3;
                            }
                            else if ((row["UserType"].ToString() == "CorporateLearner"))
                            {
                                user.UserTypeId = 4;
                            }
                            else if ((row["UserType"].ToString() == "Teacher"))
                            {
                                user.UserTypeId = 5;
                            }
                            else if ((row["UserType"].ToString() == "Admin"))
                            {
                                user.UserTypeId = 6;
                            }
                            else
                            {
                                user.UserTypeId = 0;
                            }
                        }
                        if (row["Emailaddress"].ToString()!=null)
                        {
                            user.EmailId = row["Emailaddress"].ToString();
                        }
                        else
                        {
                            user.EmailId = string.Empty;
                        }
                        if (row["FirstName"].ToString()!=null)
                        {
                            user.FirstName = row["FirstName"].ToString();
                        }
                        else
                        {
                            user.FirstName = string.Empty;
                        }
                        if (row["LastName"].ToString()!=null)
                        {
                            user.LastName = row["LastName"].ToString();
                        }
                        else
                        {
                            user.LastName = string.Empty;
                        }
                        
                        user.CellphoneCode = null;
                        if (row["CellphoneNo"].ToString()!=null)
                        {
                            user.ContactNo = row["CellphoneNo"].ToString();
                        }
                       else
                        {
                            user.ContactNo = string.Empty;
                        }
                        user.TelephoneCode = null;
                        user.TelephoneNo = null;
                        if (row["Gender"].ToString()!=null)
                        {
                            if (row["Gender"].ToString()== "Male")
                            {
                                user.Gender = 1;
                            }
                            else if (row["Gender"].ToString() == "Female")
                            {
                                user.Gender = 2;
                            }
                            else if (row["Gender"].ToString() == "Other")
                            {
                                user.Gender = 3;
                            }
                            else
                            {
                                user.Gender = null;
                            }
                        }
                        if (row["Dob"].ToString()!=null)
                        {
                            user.dob = Convert.ToDateTime(row["Dob"]);
                        }
                        else
                        {
                            user.dob = null;
                        }
                        if (row["City_Town"].ToString()!=null)
                        {
                            user.City = row["City_Town"].ToString();
                        }
                        else
                        {
                            user.City = string.Empty;
                        }
                        if (row["Province"].ToString()!=null)
                        {
                            var ProvinceName = row["Province"].ToString();
                            var Province_Id = db.Provinces.FirstOrDefault(p => p.ProvinceName == ProvinceName);
                            if (Province_Id != null)
                            {
                                ProvinceModel data = Mapper.Map<Province, ProvinceModel>(Province_Id);
                                int P_Id = data.ProvinceId;
                                user.ProvinceId = Convert.ToInt32(P_Id);
                            }
                            else
                            {
                                user.ProvinceId = null;

                            }
                        }
                        else
                        {
                            user.ProvinceId = null;
                        }
                        if (row["Grade"].ToString()!=null)
                        {
                            var gradename = row["Grade"].ToString();
                            var grade_id = db.Grades.FirstOrDefault(p => p.GradeName == gradename);
                            if (grade_id != null)
                            {
                                GradeModel data = Mapper.Map<Grade, GradeModel>(grade_id);
                                int g_id = data.GradeId;
                                user.GradeID = Convert.ToInt32(g_id);
                            }
                            else
                            {
                                user.GradeID = null;
                            }
                        }
                        else
                        {
                            user.GradeID = null;
                        }
                        if(row["School"].ToString()!=null)
                        {
                            var SchoolName = row["School"].ToString();
                            var School_Id = db.Schools.FirstOrDefault(p => p.SchoolName == SchoolName);
                           // SchoolModel data = Mapper.Map<School, SchoolModel>(School_Id);
                           if (School_Id!=null)
                            {
                                int s_id = School_Id.SchoolId;
                                user.SchoolId = Convert.ToInt32(s_id);
                            }
                           else
                            {
                                user.SchoolId = null;
                            }
                            
                        }
                        else
                        {
                            user.SchoolId = null;
                        }
                        if (row["FavouriteSubject"].ToString()!=null)
                        {
                            var SubjectName = row["FavouriteSubject"].ToString();
                            var SubId = db.Subjects.FirstOrDefault(p => p.SubjectName == SubjectName);
                            if (SubId!=null)
                            {
                               // SubjectModel data = Mapper.Map<Subject, SubjectModel>(SubId);
                                int sub_id = SubId.SubjectId;
                                user.FavSubID = Convert.ToInt32(sub_id);
                            }                     
                            else
                            {
                                user.FavSubID = null;
                            }
                            
                        }
                        else
                        {
                            user.FavSubID = null;
                        }                
                        if (row["ParentsName"].ToString()!=null)
                        {
                            user.P_Name = row["ParentsName"].ToString();
                        }
                        else
                        {
                            user.P_Name = string.Empty;
                        }
                        if (row["ParentsEmailaddress"].ToString()!=null)
                        {
                            user.P_EmailId = row["ParentsEmailaddress"].ToString();
                        }
                        else
                        {
                            user.P_EmailId = string.Empty;
                        }
                          
                        user.P_CellphoneCode = null;
                        if (row["ParentsCellphoneNo"].ToString()!=null)
                        {
                            user.P_CellPhoneNo = null;//Convert.ToInt32(row["ParentsCellphoneNo"]);
                        }
                        else
                        {
                            user.P_CellPhoneNo = null;
                        }
                        
                        user.P_TelephoneCode = null;
                        user.P_TelePhoneNo = null;
                        user.IsActive = true;

                        if (row["UserGroup"].ToString()!=null)
                        {
                            var UserGroup = row["UserGroup"].ToString();
                            var UserGroupID = db.UserGroups.FirstOrDefault(p => p.UserGroupName == UserGroup);
                            // SchoolModel data = Mapper.Map<School, SchoolModel>(School_Id);
                            if (UserGroupID != null)
                            {
                                int UGId = UserGroupID.UserGroupId;
                                user.UserGroup = Convert.ToInt32(UGId);
                            }
                            else
                            {
                                user.UserGroup = null;
                            }
                        }
                        else
                        {
                            user.UserGroup = null;
                        }
                        if (row["Package"].ToString() != null)
                        {
                            // subscription.SubscriptionTypeId = Convert.ToInt32(row["Package"]);
                            var Packagename = row["Package"].ToString();
                            var Packageid = db.SubscriptionTypes.FirstOrDefault(p => p.SubscriptionType1 == Packagename);
                            if (Packageid != null)
                            {
                                // Subs data = Mapper.Map<Grade, GradeModel>(Packageid);
                                int Package_id = Packageid.SubscriptionTypeId;
                                user.Package = Convert.ToInt32(Package_id);
                            }
                            else
                            {
                                user.Package = null;
                            }
                        }

                        //user.UserGroup=row["UserGroup"].ToString()



                        db.Users.Add(user);
                        db.SaveChanges();

                        int SubsUserID = user.UserId;


                        Subscription subscription = new Subscription();
                        UserGradeMapping USGM = new UserGradeMapping();
                        subscription.UserId = SubsUserID;
                        //subscription.UserGroupId = userview.UserGroup;
                        for (int i = 18; i <= 30; i++)
                        {

                            if (row[i].ToString() != null)
                            {
                                var gradename = row[i].ToString();
                                var grade_id = db.Grades.FirstOrDefault(p => p.GradeName == gradename);
                                if (grade_id != null)
                                {
                                    GradeModel data = Mapper.Map<Grade, GradeModel>(grade_id);
                                    int g_id = data.GradeId;
                                    subscription.GradeId = Convert.ToInt32(g_id);
                                    USGM.userId = SubsUserID;
                                    USGM.gradeId = Convert.ToInt32(g_id);
                                }

                            }
                            else
                            {
                                subscription.GradeId = null;
                                USGM.userId = SubsUserID;
                                USGM.gradeId = null;
                            }

                            subscription.SubscribedDatetime = null;
                            subscription.PromoCodeId = null;
                            if (row["Package"].ToString() != null)
                            {
                                // subscription.SubscriptionTypeId = Convert.ToInt32(row["Package"]);
                                var Packagename = row["Package"].ToString();
                                var Packageid = db.SubscriptionTypes.FirstOrDefault(p => p.SubscriptionType1 == Packagename);
                                if (Packageid != null)
                                {
                                    // Subs data = Mapper.Map<Grade, GradeModel>(Packageid);
                                    int Package_id = Packageid.SubscriptionTypeId;
                                    subscription.SubscriptionTypeId = Convert.ToInt32(Package_id);
                                }
                                else
                                {
                                    subscription.SubscriptionTypeId = null;
                                }
                            }
                            else
                            {
                                subscription.SubscriptionTypeId = null;
                            }
                            if (row["UserGroup"].ToString() != null)
                            {
                                var UserGroup = row["UserGroup"].ToString();
                                var UserGroupID = db.UserGroups.FirstOrDefault(p => p.UserGroupName == UserGroup);
                                // SchoolModel data = Mapper.Map<School, SchoolModel>(School_Id);
                                if (UserGroupID != null)
                                {
                                    int UGId = UserGroupID.UserGroupId;
                                    subscription.UserGroupId = Convert.ToInt32(UGId);
                                }
                                else
                                {
                                    subscription.UserGroupId = null;
                                }
                            }

                            subscription.StartDatetime = null;
                            subscription.EndDatetime = null;
                            subscription.IsPaid = null;
                            subscription.CreatedDateTime = DateTime.Now;


                            db.Subscriptions.Add(subscription);
                            db.UserGradeMappings.Add(USGM);
                            db.SaveChanges();
                        }                                             
                    }
                    entities.SaveChanges();
                }            
                return View("BulkUserUpload");
            }
            catch (Exception ex)
            {
                throw ex;
            }

        }  


    }
}