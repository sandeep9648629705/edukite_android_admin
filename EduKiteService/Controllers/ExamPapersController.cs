﻿using EdukiteService.Business.Entities;
using EdukiteService.Business.Interface;
using EdukiteService.Data;
using Microsoft.AspNet.SignalR;
using System;
using System.Collections.Generic;
using System.IO;
using System.IO.Compression;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using System.Xml;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using EduKiteService.Business.Interface;

namespace EduKiteService.Controllers
{
    public class ExamPapersController : Controller
    {
        private readonly ITopicService _TopicService;
        private readonly ISubTopicService _SubTopicService;
        private readonly IGradeService _GradeService;
        private readonly ISubjectService _SubjectService;
        private readonly IAssetService _AssetService;
        private readonly IModuleService _ModuleService;
        private readonly IExamPaperService _ExamPaperService;

        public ExamPapersController(ITopicService topicService, IGradeService gradeService, ISubTopicService subTopicService, ISubjectService subjectService, IAssetService assetService, 
            IModuleService moduleService, IExamPaperService ExamPaperService)
        {
            _TopicService = topicService;
            _GradeService = gradeService;
            _SubTopicService = subTopicService;
            _SubjectService = subjectService;
            _AssetService = assetService;
            _ModuleService = moduleService;
            _ExamPaperService = ExamPaperService;


            using (Edukite_AndroidEntities grd = new Edukite_AndroidEntities())
            {
                var Grade = new SelectList(grd.Grades.ToList(), "GradeId", "GradeName");
                ViewData["Grade"] = Grade.ToList();
            }

            using (Edukite_AndroidEntities sub = new Edukite_AndroidEntities())
            {
                var Subject = new SelectList(sub.Subjects.ToList(), "SubjectId", "SubjectName");
                ViewData["Subject"] = Subject.ToList();
            }
            using (Edukite_AndroidEntities ey = new Edukite_AndroidEntities())
            {
                var ExamYear = new SelectList(ey.ExamYears.ToList(), "id", "Year");
                ViewData["ExamYear"] = ExamYear.ToList();
            }
            using (Edukite_AndroidEntities et = new Edukite_AndroidEntities())
            {
                var ExamType = new SelectList(et.ExamPapertypes.ToList(), "id", "Type");
                ViewData["ExamType"] = ExamType.ToList();
            }
            using (Edukite_AndroidEntities et = new Edukite_AndroidEntities())
            {
                var Question = new SelectList(et.ExamPaperQuestions.ToList(), "QuestionId", "QuestionText");
                ViewData["Question"] = Question.ToList();
            }
            using (Edukite_AndroidEntities t =new Edukite_AndroidEntities())
            {
                var Topic = new SelectList(t.Topics.ToList(), "Topicid", "TopicName");
                ViewData["Topic"] = Topic.ToList();
            }
            using (Edukite_AndroidEntities at = new Edukite_AndroidEntities())
            {
                var Asset_Type = new SelectList(at.AssetTypes.ToList(), "AssetTypeId", "AssetType1");
                ViewData["AssetType"] = Asset_Type.ToList();
            }
            using (Edukite_AndroidEntities ey = new Edukite_AndroidEntities())
            {
                var Exam_year = new SelectList(ey.ExamYears.ToList(), "id", "Year");
                ViewData["ExamYear"] = Exam_year.ToList();
            }
            using (Edukite_AndroidEntities p = new Edukite_AndroidEntities())
            {
                var Exam_paper = new SelectList(p.ExamPapers.ToList(), "PaperId", "PaperName");
                ViewData["ExamPaper"] = Exam_paper.ToList();
            }
        }
        // GET: ExamPapers
        public ActionResult Index()
        {
            return View();
        }

        // GET: ExamPapers/Details/5
        public ActionResult Details(int id)
        {
            return View();
        }

        // GET: ExamPapers/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: ExamPapers/Create
        [HttpPost]
        public ActionResult Create(FormCollection collection)
        {
            try
            {
                // TODO: Add insert logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

        // GET: ExamPapers/Edit/5
        public ActionResult Edit(int id)
        {
            return View();
        }

        // GET: ExamPapers/Delete/5
        public ActionResult Delete(int id)
        {
            return View();
        }
        [HttpPost]
        public async Task<ActionResult> BulkAdding(AssetModel assetModelCombi)
        {
            try
            {
                var hubContext = GlobalHost.ConnectionManager.GetHubContext<BulkaddingHub>();
                int GradeId = 0; int SubId = 0;
                string Grade = Request.Form["GradeID"].ToString();
                string subject = Request.Form["FavSubID"].ToString();
                GradeId = Convert.ToInt32(Grade);
                SubId = Convert.ToInt32(subject);

                HttpPostedFileBase file = assetModelCombi.PostedFile;
                if (file == null)
                {
                    ModelState.AddModelError("File", "Please Upload Your file");
                }
                else if (file.ContentLength > 0)
                {
                    string[] AllowedFileExtensions = new string[] { ".zip" };

                    if (!AllowedFileExtensions.Contains(file.FileName.Substring(file.FileName.LastIndexOf('.'))))
                    {
                        ModelState.AddModelError("", "Please file of type: " + string.Join(", ", AllowedFileExtensions));
                    }
                    string SuccMsg = await ExamBulkUpload(file, GradeId, SubId, hubContext);

                }

                return RedirectToAction("Index", "ExamPapers", new { @GradeId = GradeId, @SubId = SubId });
            }
            catch (Exception ex)
            {
                throw ex;
            }

        }
        private async Task<string> ExamBulkUpload(HttpPostedFileBase file, int GradeId, int SubId, IHubContext hubContext)
        {
            try
            {
                int itemsCount = 0;
                var percentage = 0;
                long TotalCoreFileSize = 0;
                int assetFileCountforPerc = 0;
                int yearid = 0;
                int paperid = 0;
                int QuestionID = 0;
                

                #region size of upload directory 
                var fileName = Path.GetFileName(file.FileName);
                string fileuploadpath = file.FileName;
                
                //Crete Folder With Grade name and Subject.
                string gradeName = _GradeService.GetGradeNameByGradeID(GradeId);
                string subjectName = _SubjectService.GetSubjectNameById(SubId);
                String path = Server.MapPath("~/Content/ExamZip/");
                
                #region copy core folder 
                hubContext.Clients.All.AddProgress("Reading Files....", percentage + "%");
                //prepare directory to copy for Zip File 
                if (!Directory.Exists(path))
                {
                    Directory.CreateDirectory(path);
                }
                string Zippath = Path.Combine(path, fileName);
                file.SaveAs(Zippath);
                hubContext.Clients.All.AddProgress("File Uploaded....", "100" + "%");           

                //Extract Zip File 
                hubContext.Clients.All.AddProgress("Zip Files is extracting....", percentage + "%");
                string Examfilepath = Server.MapPath("~/Content/" + gradeName + " " + subjectName + "/exampapers/");
                if (!Directory.Exists(Examfilepath))
                {
                    Directory.CreateDirectory(Examfilepath);
                }
                ZipFile.ExtractToDirectory(Server.MapPath("~/Content/ExamZip/" + fileName), (Examfilepath));
                hubContext.Clients.All.AddProgress("Zip Files is extracted ....", "100" + "%");

                //Strat Reading XML File 
                #region read from ExamPaper.xml and insert data to db
                List<AssetModel> AssetLst = new List<AssetModel>();
                XmlDocument doc = new XmlDocument();
                hubContext.Clients.All.AddProgress("XML file reading ....", percentage + "%");
                doc.Load((Examfilepath + " /" + "ExamPaper.xml"));
                foreach (XmlNode node in doc.SelectNodes("/NewDataSet/dtAsset"))
                {
                    //Fetch the Node values and assign it to Model.
                    //-------------------------------------------------------------//
                    //QuestionAsset
                    AssetModel QuestionAsset = new AssetModel();
                    if (IsNodeExist(node, "AssetId"))
                    {
                        QuestionAsset.AssetId = Guid.Parse(node["AssetId"].InnerText);
                        if (IsNodeExist(node, "AssetName"))
                        {
                            QuestionAsset.AssetName = node["AssetName"].InnerText.ToString();
                        }
                        AssetTypeModel assetTypeModel = new AssetTypeModel();
                        if (IsNodeExist(node, "AssetType"))
                        {
                            assetTypeModel.AssetType1 = node["AssetType"].InnerText;
                            QuestionAsset.assetTypeModelInAsset = assetTypeModel;
                        }

                        if (IsNodeExist(node, "FileName"))
                        {
                            QuestionAsset.FileName = node["FileName"].InnerText.ToString();
                        }
                        if (IsNodeExist(node, "FolderName"))
                        {
                            QuestionAsset.FolderName = node["FolderName"].InnerText.ToString();
                        }
                        if (IsNodeExist(node, "SuggestedActiveDuration"))
                        {
                            QuestionAsset.SuggestedActiveDuration = Convert.ToInt32(node["SuggestedActiveDuration"].InnerText);
                        }
                        if (IsNodeExist(node, "Version"))
                        {
                            var version = string.IsNullOrEmpty(node["Version"].InnerText) ? 0 : Convert.ToInt32(node["Version"].InnerText);
                            QuestionAsset.Version = Convert.ToInt32(version);
                        }
                        if (IsNodeExist(node, "isAssetHide"))
                        {
                            QuestionAsset.isAssetHide = true;
                        }
                        else
                        {
                            QuestionAsset.isAssetHide = true;
                        }
                        QuestionAsset.Description = "ExamPaperQuestionAssetID";
                    }
                    

                    //SolutionAsset
                    AssetModel SolutionAsset = new AssetModel();
                    if (IsNodeExist(node, "SolutionAssetId"))
                    {
                        SolutionAsset.AssetId = Guid.Parse(node["SolutionAssetId"].InnerText);

                        AssetTypeModel assetTypeModel = new AssetTypeModel();
                        if (IsNodeExist(node, "SolutionAssetType"))
                        {
                            assetTypeModel.AssetType1 = node["SolutionAssetType"].InnerText;
                            SolutionAsset.assetTypeModelInAsset = assetTypeModel;
                        }
                        if (IsNodeExist(node, "AssetName"))
                        {
                            SolutionAsset.AssetName = node["AssetName"].InnerText.ToString();
                        }
                        if (IsNodeExist(node, "SolutionFileName"))
                        {
                            SolutionAsset.FileName = node["SolutionFileName"].InnerText.ToString();
                        }

                        if (IsNodeExist(node, "SolutionLength"))
                        {
                            SolutionAsset.AssetLength = Convert.ToInt32(node["SolutionLength"].InnerText);
                        }
                        if (IsNodeExist(node, "SuggestedActiveDuration"))
                        {
                            SolutionAsset.SuggestedActiveDuration = Convert.ToInt32(node["SuggestedActiveDuration"].InnerText);
                        }
                        if (IsNodeExist(node, "FolderName"))
                        {
                            SolutionAsset.FolderName = node["FolderName"].InnerText.ToString();
                        }
                        if (IsNodeExist(node, "SolutionThumbnailFileName"))
                        {
                            SolutionAsset.ThumbnailFileName = node["SolutionThumbnailFileName"].InnerText.ToString();
                        }
                        if (IsNodeExist(node, "Version"))
                        {
                            var version = string.IsNullOrEmpty(node["Version"].InnerText) ? 0 : Convert.ToInt32(node["Version"].InnerText);
                            SolutionAsset.Version = Convert.ToInt32(version);
                        }
                        if (IsNodeExist(node, "isAssetHide"))
                        {
                            SolutionAsset.isAssetHide = true;
                        }
                        else
                        {
                            SolutionAsset.isAssetHide = true;
                        }
                        SolutionAsset.Description = "ExamPaperQuestionSolutionAssetId";
                    }
                    AssetModel ass_ques = _AssetService.GetAssetByAssetId(Guid.Parse(node["SolutionAssetId"].InnerText));
                    if (ass_ques != null)
                    {
                        _AssetService.AddAsset(QuestionAsset);
                    }

                    AssetModel ass_sol = _AssetService.GetAssetByAssetId(Guid.Parse(node["AssetId"].InnerText));
                    if (ass_sol != null)
                    {
                        _AssetService.AddAsset(SolutionAsset);
                    }

                    //Exam Year Table
                    using (Edukite_AndroidEntities db = new Edukite_AndroidEntities())
                    {
                        ExamYear ExamYear = new ExamYear();
                        ExamYearModel year = _ExamPaperService.GetExamYearIDByYearName(node["Year"].InnerText.ToString());
                        if (year == null)
                        {
                            if (IsNodeExist(node, "YearNo"))
                            {
                                var YearNo = string.IsNullOrEmpty(node["YearNo"].InnerText) ? 0 : Convert.ToInt32(node["YearNo"].InnerText);
                                ExamYear.YearNo = Convert.ToInt32(YearNo);
                            }
                            if (IsNodeExist(node, "Year"))
                            {
                                ExamYear.Year = node["Year"].InnerText.ToString();
                            }
                            db.ExamYears.Add(ExamYear);
                            db.SaveChanges();
                            ExamYearModel year_id = _ExamPaperService.GetExamYearIDByYearName(node["Year"].InnerText.ToString());
                            yearid = year_id.id;
                        }
                        else
                        {
                            yearid = year.id;
                        }
                    }
                    //Exam Paper Table 
                    if (IsNodeExist(node, "PaperNo"))
                    {
                        ExamPaperModel paper = new ExamPaperModel();
                        ExamPaperModel Exampaper = _ExamPaperService.GetExamPaperIdbyPaperNo((node["PaperNo"].InnerText));
                        if (Exampaper == null)
                        {
                            paper.PaperNo = node["PaperNo"].InnerText.ToString();
                            paper.PaperName = fileName.Replace(".zip", "");
                            paper.GradeId = GradeId;
                            paper.SubId = SubId;
                            paper.YearID = yearid;
                            paper.ExamTypeId = null;
                            paper.Instruction = "Instruction";
                            _ExamPaperService.AddExamPaper(paper);
                            ExamPaperModel Exampaperid = _ExamPaperService.GetExamPaperIdbyPaperNo((node["PaperNo"].InnerText));
                            paperid = Exampaperid.PaperId;
                        }
                        else
                        {
                            paperid = Exampaper.PaperId;
                        }
                    }
                    //----------------------------Read Json File-------------------------------------------//                   
                    string jsonFile = (Examfilepath + node["FolderName"].InnerText.ToString() + "\\" + node["FileName"].InnerText.ToString());
                    if (System.IO.File.Exists(jsonFile))
                    {
                        var json = System.IO.File.ReadAllText(jsonFile);
                        var jObject = JObject.Parse(json);
                        //Set Value 
                        dynamic d = JObject.Parse(json);
                        var ques = d.question;

                        //ExamPaperQuestion
                        using (Edukite_AndroidEntities ee = new Edukite_AndroidEntities())
                        {
                            string questionnum = ques.quesNum;
                            ExamPaperQuestionModel que = _ExamPaperService.GetExamPaperQuestionIdby(questionnum, paperid);
                            //Parrent Question 
                            if (que == null)
                            {
                                ExamPaperQuestion question = new ExamPaperQuestion();
                                question.QuestionId = ques.quesId;
                                question.QuestionNo = ques.quesNum;
                                question.QuestionText = ques.quesText;
                                question.startat = ques.startsAt;
                                question.endat = ques.endsAt;
                                question.marks = ques.marks;
                                question.Note = ques.description;
                                question.PaperId = paperid;
                                question.Assetid = Guid.Parse(node["AssetId"].InnerText);
                                question.SolutionAssetId = Guid.Parse(node["SolutionAssetId"].InnerText);

                                ee.ExamPaperQuestions.Add(question);
                                ee.SaveChanges();
                                ExamPaperQuestionModel quess = _ExamPaperService.GetExamPaperQuestionIdby(questionnum, paperid);
                                QuestionID = quess.QuestionId;
                            }
                            else
                            {
                                QuestionID = que.QuestionId;
                            }
                        }

                        //SubQuestion and Option 
                        using (Edukite_AndroidEntities ee = new Edukite_AndroidEntities())
                        {
                            var SubQuestion = ques.subQuestions;
                            string questionnum = ques.quesNum;
                            foreach (var item in SubQuestion)
                            {
                                ExamPaperQuestionModel que = _ExamPaperService.GetExamPaperQuestionIdby(questionnum, paperid);
                                QuestionID = que.QuestionId;
                                ExamPaperQuestion questions = new ExamPaperQuestion();
                                if (item.quesId != null)
                                {
                                    questions.QuestionId = item.quesId;
                                }
                                if (item.quesNum != string.Empty)
                                {
                                    questions.QuestionNo = item.quesNum;
                                }
                                if (item.quesText != string.Empty)
                                {
                                    questions.QuestionText = item.quesText;
                                }
                                if (item.startsAt != null)
                                {
                                    questions.startat =Convert.ToInt32(item.startsAt);
                                }
                                if (item.endsAt != null)
                                {
                                    questions.endat =Convert.ToInt32(item.endsAt);
                                }
                                if (item.marks != string.Empty)
                                {
                                    questions.marks = Convert.ToInt32(item.marks);
                                }
                                if (item.description != string.Empty)
                                {
                                    questions.Note = item.description;
                                }
                                questions.PaperId = paperid;
                                questions.Assetid = Guid.Parse(node["AssetId"].InnerText);
                                questions.SolutionAssetId = Guid.Parse(node["SolutionAssetId"].InnerText);
                                questions.PQid = QuestionID;

                                ee.ExamPaperQuestions.Add(questions);
                                ee.SaveChanges();

                                if (item.options != null|| item.options !=string.Empty)
                                {
                                    var SubQuestionoptions = item.options;
                                    //get Question  ID 
                                    string QueNum = item.quesNum;
                                    ExamPaperQuestionModel SubQues = _ExamPaperService.GetExamPaperQuestionIdby(QueNum, paperid);
                                    int subQuestionID = SubQues.QuestionId;
                                    //Save Data 
                                    ExamPaperQuestionOption Subquesoption = new ExamPaperQuestionOption();
                                    foreach (var SubQuestionoption in SubQuestionoptions)
                                    {   if (SubQuestionoption.optionNum!=string.Empty)
                                        {
                                            Subquesoption.OptionNumber = SubQuestionoption.optionNum;
                                        }
                                        if(SubQuestionoption.optionOrder!=null)
                                        {
                                            Subquesoption.OptionShort = SubQuestionoption.optionOrder;
                                        }
                                        if(SubQuestionoption.optionText!=string.Empty)
                                        {
                                            Subquesoption.OptionText = SubQuestionoption.optionText;
                                        }                                                                               
                                        Subquesoption.QuestionId = subQuestionID;

                                        ee.ExamPaperQuestionOptions.Add(Subquesoption);
                                        ee.SaveChanges();
                                    }                                   
                                }
                            }

                        }
                    }


                    //ExamPaperTopic 
                    using (Edukite_AndroidEntities db = new Edukite_AndroidEntities())
                    {
                        //List<ExamPaperTopic> ExamPaperTopiclist = new List<ExamPaperTopic>();
                        ExamPaperTopic QuestionTopic = new ExamPaperTopic();
                        //TopicName1
                        if (IsNodeExist(node, "TopicName1"))
                        {
                            string TopicName = node["TopicName1"].InnerText.ToString();
                            if (TopicName != string.Empty)
                            {
                                TopicModel record = _TopicService.GetTopicByName(TopicName, SubId);
                                QuestionTopic.PaperId = paperid;
                                QuestionTopic.QuestionId = QuestionID;
                                QuestionTopic.TopicId = record.TopicId;
                                QuestionTopic.TopicNo = record.TopicNo;
                                
                                db.ExamPaperTopics.Add(QuestionTopic);
                                db.SaveChanges();
                            }
                        }
                        
                        //TopicName2
                        if (IsNodeExist(node, "TopicName2"))
                        {
                            string TopicName = node["TopicName2"].InnerText.ToString();
                            if (TopicName != string.Empty)
                            {
                                TopicModel record = _TopicService.GetTopicByName(TopicName, SubId);
                                QuestionTopic.PaperId = paperid;
                                QuestionTopic.QuestionId = QuestionID;
                                QuestionTopic.TopicId = record.TopicId;
                                QuestionTopic.TopicNo = record.TopicNo;

                                db.ExamPaperTopics.Add(QuestionTopic);
                                db.SaveChanges();
                            }
                        }
                        
                        //TopicName3
                        if (IsNodeExist(node, "TopicName3"))
                        {
                            string TopicName = node["TopicName3"].InnerText.ToString();
                            if (TopicName != string.Empty)
                            {
                                TopicModel record = _TopicService.GetTopicByName(TopicName, SubId);
                                QuestionTopic.PaperId = paperid;
                                QuestionTopic.QuestionId = QuestionID;
                                QuestionTopic.TopicId = record.TopicId;
                                QuestionTopic.TopicNo = record.TopicNo;

                                db.ExamPaperTopics.Add(QuestionTopic);
                                db.SaveChanges();
                            }
                        }
                       
                        //TopicName4
                        if (IsNodeExist(node, "TopicName4"))
                        {
                            string TopicName = node["TopicName4"].InnerText.ToString();
                            if (TopicName != string.Empty)
                            {
                                TopicModel record = _TopicService.GetTopicByName(TopicName, SubId);
                                QuestionTopic.PaperId = paperid;
                                QuestionTopic.QuestionId = QuestionID;
                                QuestionTopic.TopicId = record.TopicId;
                                QuestionTopic.TopicNo = record.TopicNo;

                                db.ExamPaperTopics.Add(QuestionTopic);
                                db.SaveChanges();

                            }
                        }                      

                    }
                }               
                //total item count ie: asset list count + core folder size in MB
                itemsCount = itemsCount + AssetLst.Count;
                assetFileCountforPerc += 1;
                percentage = (assetFileCountforPerc * 100) / itemsCount;
                //update msg in view 
                hubContext.Clients.All.AddProgress("(Creating Papers and Questions.... 0 MB of " + TotalCoreFileSize + " MB added.)", percentage + "%");
                #endregion
                #endregion
                #endregion
                hubContext.Clients.All.AddProgress("(File uploaded successfully)", "100%");
                return "File uploaded successfully";            

            }
            catch (Exception ex)
            {
               return ex.Message;
            }
            
        }
        
        public bool IsNodeExist(XmlNode node, string prop)
        {
            try
            {
                if (node[prop].InnerText == null || node[prop] != null)
                    return true;
                else
                    return false;
            }
            catch (Exception ex)
            {
                return false;
            }
        }
		public ActionResult PaperList()
        {

            try
            {             
                ViewBag.lstmodelTopics = _ExamPaperService.GetPaperList();
            }
            catch (Exception ex)
            {

                throw ex;
            }

            return View();
        }
        public ActionResult GetQuestionsList(int id)
        {

            try
            {                
                ViewBag.lstmodelQuestions = _ExamPaperService.GetQuestions(id);
            }
            catch (Exception ex)
            {

                throw ex;
            }

            return View();
        }

        public ActionResult AddPaper()
        {
            ViewBag.lstmodelTopics = _ExamPaperService.GetPaperList();
            return View("AddPaper");
        }
        public ActionResult AddQuestion()
        {
           
            return View("AddQuestion");
        }
        public ActionResult AddOption()
        {
            return View("AddOption");
        }
        public ActionResult GetOptions(int id)
        {
            try
            {
                //var lstmodelTopics = _TopicService.GetPaperList();
                ViewBag.lstmodeloptions = _ExamPaperService.GetOptions(id);
            }
            catch (Exception ex)
            {

                throw ex;
            }

            return View();

        }
        [HttpPost]
        public ActionResult CreatePaper(ExamPaperModel papermodel)
        {

            try
                
            {
                if (papermodel != null)
                {
                    var addpaper = new ExamPaperModel();
                    addpaper.PaperNo = papermodel.PaperNo;
                    addpaper.PaperName = papermodel.PaperName;
                    addpaper.YearID =Convert.ToInt32(Request.Form["ExamYearid"].ToString());
                    addpaper.GradeId = Convert.ToInt32(Request.Form["GradeId"].ToString());
                    addpaper.SubId = Convert.ToInt32(Request.Form["SubjectId"].ToString());
                    addpaper.ExamTypeId = Convert.ToInt32(Request.Form["ExamTypeid"].ToString());
                    addpaper.Instruction = papermodel.Instruction;
                   _ExamPaperService.AddPaper(addpaper);
                }               
                return RedirectToAction("PaperList", "ExamPapers");
            }
            catch (Exception ex)
            {
                throw ex;
            }

        }
        
        [HttpPost]
        public async Task<ActionResult> CreateQustionAsync(ExamAssetModel QustAss)
        {

            try
            {
                int yearid = 0;
               
                //HttpPostedFileBase questionfile = QustAss.QuestionFilename;
                if (QustAss != null)
                {
                    
                        var Questionasset = new AssetModel();
                        Questionasset.AssetId = QustAss.QuestionAssetID;
                        Questionasset.AssetName = QustAss.QuestionAssetName;
                        Questionasset.AssetTypeId = Convert.ToInt32(Request.Form["AssetType"].ToString());
                        Questionasset.FileName = QustAss.PostedQuestionFilename.FileName;//"E:\\Desktop\\exam\\exam\\ExamPaperQuestionJson.json"
                        Questionasset.FolderName = QustAss.Foldername;
                        Questionasset.Version = 1;
                        Questionasset.isAssetHide = true;

                        var SoluctionAsset = new AssetModel();
                        SoluctionAsset.AssetId = QustAss.SoluctionAssetID;
                        SoluctionAsset.AssetName = QustAss.QuestionAssetName;
                        SoluctionAsset.AssetTypeId = Convert.ToInt32(Request.Form["SoluctionAssetType"].ToString());
                        //SoluctionAsset.FileName = QustAss.SoluctionFilename.ToString();
                        SoluctionAsset.FileName = QustAss.PostedSoluctionFilename.FileName;//"E:\\Desktop\\exam\\exam\\PS12_Nov2016P1_Q5.mp4";
                        SoluctionAsset.AssetLength = QustAss.SoluctionLenth;
                        SoluctionAsset.SuggestedActiveDuration = QustAss.SoluctionActiveDuration;
                        SoluctionAsset.FolderName = QustAss.Foldername;
                        SoluctionAsset.ThumbnailFileName = QustAss.PostedFileThumbnail.FileName;//"E:\\Desktop\\exam\\exam\\Exam_Paper_Folder_Structure.png";
                        SoluctionAsset.Version = 1;
                        SoluctionAsset.isAssetHide = true;

                    AssetModel asset = _AssetService.GetAssetByAssetId(QustAss.QuestionAssetID);
                    if (asset == null)
                    {
                        _AssetService.AddAsset(Questionasset);
                        
                    }
                    AssetModel sol_asset = _AssetService.GetAssetByAssetId(QustAss.SoluctionAssetID);
                    if (sol_asset == null)
                    {
                        _AssetService.AddAsset(SoluctionAsset);
                    }

                        string years = QustAss.Year;
                    int yearno = QustAss.Yearno;
                    using (Edukite_AndroidEntities db = new Edukite_AndroidEntities())
                    {
                        ExamYear ExamYear = new ExamYear();
                        ExamYearModel year = _ExamPaperService.GetExamYearIDByYearName(years, yearno);
                        if (year == null)
                        {
                            ExamYear.Year = QustAss.Year;
                            ExamYear.YearNo = QustAss.Yearno;
                            db.ExamYears.Add(ExamYear);
                            db.SaveChanges();
                            //_ExamPaperService.AddExamyear(ExamYear);

                            ExamYearModel year_id = _ExamPaperService.GetExamYearIDByYearName(years, yearno);
                            yearid = year_id.id;
                        }
                        else
                        {
                            yearid = year.id;
                        }
                    }

                    int gradeid =Convert.ToInt32(Request.Form["Gradeid"].ToString());
                    int subid = Convert.ToInt32(Request.Form["SubjectId"].ToString());
                    int paperid= Convert.ToInt32(Request.Form["ExamPaper"].ToString());
                    int topic1 = Convert.ToInt32(Request.Form["Topic1"].ToString());
                    int topic2 = Convert.ToInt32(Request.Form["Topic2"].ToString());
                    int topic3 = Convert.ToInt32(Request.Form["Topic3"].ToString());
                    int topic4 = Convert.ToInt32(Request.Form["Topic4"].ToString());
                    Guid QuestionAssetid = QustAss.QuestionAssetID;
                    Guid SoluctionAssetid = QustAss.SoluctionAssetID;
                    string foldername = QustAss.Foldername;
                    HttpPostedFileBase jsonfile = QustAss.PostedQuestionFilename;
                    HttpPostedFileBase Videofile = QustAss.PostedSoluctionFilename;
                    HttpPostedFileBase Imagefile = QustAss.PostedFileThumbnail;

                    string SuccMsg = await SaveFile(jsonfile, Videofile, Imagefile,gradeid, subid, paperid, topic1, topic2, topic3, topic4,
                        foldername, QuestionAssetid, SoluctionAssetid);

                    return View(SuccMsg);
                   
                }
                var question = _ExamPaperService.GetExamPaperQuestionId(QustAss.QuestionNo.ToString(), QustAss.PaperNo);
                int ques_id = question.QuestionId;
                return RedirectToAction("GetQuestionsList", "ExamPapers", new { @id = ques_id });
            }
            catch (Exception ex)
            {
                throw ex;
            }
            
            //return RedirectToAction("PaperList", "ExamPapers");
        }
        [HttpPost]
        public async Task<string> SaveFile (HttpPostedFileBase JsonFile, HttpPostedFileBase Videofile, HttpPostedFileBase imagefile, int GradeId,int SubId,int paperid, 
            int topic1, int topic2, int topic3, int topic4,string foldername,Guid QuestionAsset,Guid SoluctionAsset)
        {
            try
            {
                if (JsonFile != null || Videofile != null)
                {
                    string gradeName = _GradeService.GetGradeNameByGradeID(GradeId);
                    string subjectName = _SubjectService.GetSubjectNameById(SubId);
                    string folder = foldername;
                    string modulpath= Server.MapPath("~/Content/" + gradeName + " " + subjectName);
                    if (!Directory.Exists(modulpath))
                    {
                        Directory.CreateDirectory(modulpath);
                    }
                    string exampaperpath =(modulpath+"\\exampapers");
                    if (!Directory.Exists(exampaperpath))
                    {
                        Directory.CreateDirectory(exampaperpath);
                    }

                    string Examfilepath = (exampaperpath+"\\" + folder);
                    if (!Directory.Exists(Examfilepath))
                    {
                        Directory.CreateDirectory(Examfilepath);
                    }

                    string path = (Examfilepath+"\\");
                    if (!Directory.Exists(path))
                    {
                        Directory.CreateDirectory(path);
                    }

                    JsonFile.SaveAs(path + Path.GetFileName(JsonFile.FileName));
                    Videofile.SaveAs(path + Path.GetFileName(Videofile.FileName));
                    imagefile.SaveAs(path + Path.GetFileName(imagefile.FileName));

                    string jsonfilepath = path + Path.GetFileName(JsonFile.FileName);
                    UploadJson(jsonfilepath, paperid, topic1, topic2, topic3, topic4, QuestionAsset, SoluctionAsset);
                                       
                }
                return ("Uploaded Successfully");
            }
             catch(Exception ex)
            {
                throw ex;
            }
        }

        [HttpPost]
        public void UploadJson(string jsonfile,int paperid,int topic1, int topic2, int topic3, int topic4,
            Guid QuestionAsset, Guid SoluctionAsset)
        {
            try
            {
                int QuestionID = 0;
                int paper = paperid;
                int topic_1 = topic1;
                int topic_2 = topic2;
                int topic_3 = topic3;
                int topic_4 = topic4;
                Guid QuestionAssetid = QuestionAsset;
                Guid SoluctionAssetid = SoluctionAsset;
                

                if (System.IO.File.Exists(jsonfile))
                {
                    var json = System.IO.File.ReadAllText(jsonfile);
                    var jObject = JObject.Parse(json);
                    //Set Value 
                    dynamic d = JObject.Parse(json);
                    var ques = d.question;

                    //ExamPaperQuestion
                    using (Edukite_AndroidEntities ee = new Edukite_AndroidEntities())
                    {
                        string questionnum = ques.quesNum;
                        ExamPaperQuestionModel que = _ExamPaperService.GetExamPaperQuestionIdby(questionnum, paperid);
                        //Parrent Question 
                        if (que == null)
                        {
                            ExamPaperQuestion question = new ExamPaperQuestion();
                            question.QuestionId = ques.quesId;
                            question.QuestionNo = ques.quesNum;
                            question.QuestionText = ques.quesText;
                            question.startat = ques.startsAt;
                            question.endat = ques.endsAt;
                            question.marks = ques.marks;
                            question.Note = ques.description;
                            question.PaperId = paperid;
                            question.Assetid = QuestionAssetid;
                            question.SolutionAssetId = SoluctionAssetid;

                            ee.ExamPaperQuestions.Add(question);
                            ee.SaveChanges();
                            ExamPaperQuestionModel quess = _ExamPaperService.GetExamPaperQuestionIdby(questionnum, paperid);
                            QuestionID = quess.QuestionId;
                        }
                        else
                        {
                            QuestionID = que.QuestionId;
                        }
                    }

                    //SubQuestion and Option 
                    using (Edukite_AndroidEntities ee = new Edukite_AndroidEntities())
                    {
                        var SubQuestion = ques.subQuestions;
                        string questionnum = ques.quesNum;
                        foreach (var item in SubQuestion)
                        {
                            ExamPaperQuestionModel que = _ExamPaperService.GetExamPaperQuestionIdby(questionnum, paperid);
                            QuestionID = que.QuestionId;
                            ExamPaperQuestion questions = new ExamPaperQuestion();
                            if (item.quesId != null)
                            {
                                questions.QuestionId = item.quesId;
                            }
                            if (item.quesNum != string.Empty)
                            {
                                questions.QuestionNo = item.quesNum;
                            }
                            if (item.quesText != string.Empty)
                            {
                                questions.QuestionText = item.quesText;
                            }
                            if (item.startsAt != null)
                            {
                                questions.startat = Convert.ToInt32(item.startsAt);
                            }
                            if (item.endsAt != null)
                            {
                                questions.endat = Convert.ToInt32(item.endsAt);
                            }
                            if (item.marks != string.Empty)
                            {
                                questions.marks = Convert.ToInt32(item.marks);
                            }
                            if (item.description != string.Empty)
                            {
                                questions.Note = item.description;
                            }
                            questions.PaperId = paperid;
                            questions.Assetid = QuestionAssetid;
                            questions.SolutionAssetId = SoluctionAssetid;
                            questions.PQid = QuestionID;

                            ee.ExamPaperQuestions.Add(questions);
                            ee.SaveChanges();

                            if (item.options != null || item.options != string.Empty)
                            {
                                var SubQuestionoptions = item.options;
                                //get Question  ID 
                                string QueNum = item.quesNum;
                                ExamPaperQuestionModel SubQues = _ExamPaperService.GetExamPaperQuestionIdby(QueNum, paperid);
                                int subQuestionID = SubQues.QuestionId;
                                //Save Data 
                                ExamPaperQuestionOption Subquesoption = new ExamPaperQuestionOption();
                                foreach (var SubQuestionoption in SubQuestionoptions)
                                {
                                    if (SubQuestionoption.optionNum != string.Empty)
                                    {
                                        Subquesoption.OptionNumber = SubQuestionoption.optionNum;
                                    }
                                    if (SubQuestionoption.optionOrder != null)
                                    {
                                        Subquesoption.OptionShort = SubQuestionoption.optionOrder;
                                    }
                                    if (SubQuestionoption.optionText != string.Empty)
                                    {
                                        Subquesoption.OptionText = SubQuestionoption.optionText;
                                    }
                                    Subquesoption.QuestionId = subQuestionID;

                                    ee.ExamPaperQuestionOptions.Add(Subquesoption);
                                    ee.SaveChanges();
                                }
                            }
                        }

                    }
                }
                //ExamPaperTopic 
                using (Edukite_AndroidEntities db = new Edukite_AndroidEntities())
                {
                    //List<ExamPaperTopic> ExamPaperTopiclist = new List<ExamPaperTopic>();
                    ExamPaperTopic QuestionTopic = new ExamPaperTopic();
                    //TopicName1
                    if (topic1 != 0)
                    {
                        TopicModel record = _TopicService.GetTopicById(topic1);
                        QuestionTopic.PaperId = paperid;
                        QuestionTopic.QuestionId = QuestionID;
                        QuestionTopic.TopicId = record.TopicId;
                        QuestionTopic.TopicNo = record.TopicNo;

                        db.ExamPaperTopics.Add(QuestionTopic);
                        db.SaveChanges();

                    }
                    //TopicName2
                    if (topic2 != 0)
                    {
                        TopicModel record = _TopicService.GetTopicById(topic2);
                        QuestionTopic.PaperId = paperid;
                        QuestionTopic.QuestionId = QuestionID;
                        QuestionTopic.TopicId = record.TopicId;
                        QuestionTopic.TopicNo = record.TopicNo;

                        db.ExamPaperTopics.Add(QuestionTopic);
                        db.SaveChanges();
                    }
                    //TopicName3
                    if (topic3 != 0)
                    {
                        TopicModel record = _TopicService.GetTopicById(topic3);
                        QuestionTopic.PaperId = paperid;
                        QuestionTopic.QuestionId = QuestionID;
                        QuestionTopic.TopicId = record.TopicId;
                        QuestionTopic.TopicNo = record.TopicNo;

                        db.ExamPaperTopics.Add(QuestionTopic);
                        db.SaveChanges();
                    }
                    //TopicName4
                    if (topic4 != 0)
                    {
                        TopicModel record = _TopicService.GetTopicById(topic4);
                        QuestionTopic.PaperId = paperid;
                        QuestionTopic.QuestionId = QuestionID;
                        QuestionTopic.TopicId = record.TopicId;
                        QuestionTopic.TopicNo = record.TopicNo;

                        db.ExamPaperTopics.Add(QuestionTopic);
                        db.SaveChanges();
                    }

                }
            }
            catch(Exception ex)
            {
                throw ex;
            }
        }

        [HttpPost]
        public ActionResult CreateOption(ExamPaperQuestionOptionModel optionmodel)
        {
            try

            {
                int questionid = Convert.ToInt32(Request.Form["Question"].ToString());


                if (optionmodel != null)
                {
                    var addoption = new ExamPaperQuestionOptionModel();
                    addoption.QuestionId=Convert.ToInt32(Request.Form["Question"].ToString());
                    addoption.OptionText = optionmodel.OptionText;
                    addoption.OptionShort = optionmodel.OptionShort;
                    addoption.OptionNumber = optionmodel.OptionNumber;
                    _ExamPaperService.AddOption(addoption);
                }
                return RedirectToAction("GetOptions", "ExamPapers", new { @id = questionid });
            }
            catch (Exception ex)
            {
                throw ex;
            }

        }
    }
}
   
