﻿using System.Web;
using System.Web.Optimization;

namespace EduKiteService
{
    public class BundleConfig
    {
        // For more information on bundling, visit https://go.microsoft.com/fwlink/?LinkId=301862
        public static void RegisterBundles(BundleCollection bundles)
        {
            bundles.Add(new ScriptBundle("~/bundles/jquery").Include("~/Scripts/js/jquery.min.js"));// added by RK on 03-06-2019
            bundles.Add(new ScriptBundle("~/bundles/jquery-ui").Include("~/Scripts/js/jquery-ui.min.js"));// added by RK on 25-06-2019

            // Use the development version of Modernizr to develop with and learn from. Then, when you're
            // ready for production, use the build tool at https://modernizr.com to pick only the tests you need.
            //bundles.Add(new ScriptBundle("~/bundles/modernizr").Include(
            // "~/Scripts/modernizr-*"));

            bundles.Add(new ScriptBundle("~/bundles/bootstrap").Include(
                      "~/Scripts/js/bootstrap.min.js",
                      "~/Scripts/js/bootstrap-select.min.js",
                      "~/Scripts/js/jquery.nice-select.js"));// added by RK on 03-06-2019

            bundles.Add(new ScriptBundle("~/bundles/validate").Include("~/Scripts/Validate/jquery.validate*"));// added by RK on 03-06-2019

            bundles.Add(new ScriptBundle("~/bundles/upload").Include(
                  "~/Scripts/js/upload.js"));// added by Anil on 13-06-2019
            bundles.Add(new ScriptBundle("~/bundles/deleteUpload").Include(
                 "~/Scripts/js/DeleteUploadedFile.js"));// added by Anil on 17-06-2019


            bundles.Add(new StyleBundle("~/Content/css").Include(
                      "~/Content/css/bootstrap.min.css",
                      "~/Content/css/AdminBLI.min.css",
                      "~/Content/css/bli-style.css",
                      "~/Content/css/bli-backend-style.css",
                      "~/Content/css/nice-select.css"
                      ));// added by RK on 03-06-2019
            BundleTable.EnableOptimizations = false;

        }
    }
}
