﻿using Autofac;
using Autofac.Integration.WebApi;
using Edukite.Bussiness;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Web;
using System.Web.Http;


namespace EdukiteService.Business.Implementation
{
    public class DependencyConfiguration
    {
        public static IContainer Initialize(HttpConfiguration config)
        {
            var builder = new ContainerBuilder();
            var container = RegisterServices(builder);

            config.DependencyResolver = new AutofacWebApiDependencyResolver(container);
            return container;
        }

        private static IContainer RegisterServices(ContainerBuilder builder)
        {
            var result = Assembly.GetExecutingAssembly();
            builder.RegisterApiControllers(Assembly.GetExecutingAssembly());
            //builder.RegisterModule(new BusinessInjectionModule());
            return builder.Build();
        }
    }
}