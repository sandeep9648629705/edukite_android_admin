﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EdukiteService.Business.Entities
{
   public class ExamPapertypeModel
    {
        public int id { get; set; }
        public string Type { get; set; }
        public string PaperName { get; set; }
    }
}
