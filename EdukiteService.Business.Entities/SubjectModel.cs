﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;

namespace EdukiteService.Business.Entities
{
    public class SubjectModel
    {
        [Key]
        public int SubjectId { get; set; }
        [StringLength(150, MinimumLength = 1, ErrorMessage = "Only 150 character are allowed")]
        public string Description { get; set; }
        [Required]
        [RegularExpression("^[a-zA-Z\\s]*$", ErrorMessage = "Only Alphabets allowed.")]
        [StringLength(100, MinimumLength = 1, ErrorMessage = "Only 100 character are allowed")]
        [Remote("IsSubjectNameExist", "Subjects")]
        public string SubjectName { get; set; }
        public string Themedb { get; set; }
        [Required]
        [RegularExpression("^[a-zA-Z0-9\\s]*$", ErrorMessage = "Only Alphabets and Numbers allowed.")]
        [StringLength(50, MinimumLength = 1, ErrorMessage = "Only 50 character are allowed")]
        public string ShortForm { get; set; }
        public string Code { get; set; }

        public Nullable<int> GradeID { get; set; }
        public bool IsSubjectPublished { get; set; }


        //for webapi - added by on 22-07-2019
        public string name { get; set; }
        public int id { get; set; }
        public string theme { get; set; }
        public int totalExamPapers { get; set; }
        public int completedExamPapers { get; set; }

    }
}
