﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EdukiteService.Business.Entities
{
    public class ExamPaperModel
    {
        public int PaperId { get; set; }
        public string PaperNo { get; set; }
        public string PaperName { get; set; }
        public Nullable<int> YearID { get; set; }
        public Nullable<int> GradeId { get; set; }
        public Nullable<int> SubId { get; set; }
        public Nullable<int> ExamTypeId { get; set; }
        public string Instruction { get; set; }
    }
}
