﻿using EdukiteService.Data;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;


namespace EdukiteService.Business.Entities
{
    public class ModuleModel
    {
        [Key]
        public int ModuleId { get; set; }
        public string ModuleName { get; set; }
        public string Description { get; set; }
        public Nullable<int> GradeId { get; set; }
        public Nullable<int> SubjectId { get; set; }
        public Nullable<int> Version { get; set; }
        public Nullable<System.DateTime> CreatedDatime { get; set; }
        public Nullable<System.DateTime> UpdatedDatime { get; set; }
        public string FolderPath { get; set; }
        public bool IsEncrypted { get; set; }

        public virtual Grade Grade { get; set; }
        public virtual Subject Subject { get; set; }
    }
}
