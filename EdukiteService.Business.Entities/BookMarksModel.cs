﻿using EdukiteService.Business.Entities;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EdukiteService.Business.Entities
{
    public class BooKMarkRequest
    {
        public int userId { get; set; }
        public string deviceId { get; set; }
        public int PageNo { get; set; }
        public int PageSize { get; set; }
    }

    public class BookMarksModel
    {
        public int Id { get; set; }
        public string TopicName { get; set; }
        public string SubTopicName { get; set; }
        public string GradeName { get; set; }
        public string AssetName { get; set; }
        public string AssetPath { get; set; }
        public bool IsCompleted { get; set; }
        public string FileSize { get; set; }
    }

    public class BookMarksResponse : JsonResponse
    {
        public IEnumerable<BookMarksModel> BookMarks { get; set; }

    }
}
