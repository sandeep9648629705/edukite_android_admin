﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EdukiteService.Business.Entities
{
   public class AssetTypeModel
    {
        [Key]
        public int AssetTypeId { get; set; }
        public string AssetType1 { get; set; }
        public string Description { get; set; }
        public string Extension { get; set; }
    }
}
