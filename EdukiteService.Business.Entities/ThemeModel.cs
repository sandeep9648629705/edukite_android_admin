﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EdukiteService.Business.Entities
{
    public class ThemeModel
    {
        [Key]
        public int ThemeId { get; set; }
        public string ThemeColor { get; set; }
        public string ThemeSubject { get; set; }
    }
}
