﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EdukiteService.Business.Entities
{
    public class ProvinceModel
    {
        public int ProvinceId { get; set; }
        public string ProvinceName { get; set; }
    }
}
