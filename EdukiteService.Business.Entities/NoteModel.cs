﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace EdukiteService.Business.Entities
{
    public class NoteModel : BaseModel
    {
        public int NoteId { get; set; }
        public int UserId { get; set; }
        public int DeviceId { get; set; }
        public int TopicId { get; set; }
        public int SubTopicId { get; set; }
        public int SubjectId { get; set; }
        public Guid? AssetId { get; set; }
    }

    public class BaseModel
    {
        public int Id { get; set; }
        public string Text { get; set; }
        public DateTime DateTime { get; set; }
        public string ImagePath { get; set; }
    }

    public class NotesResponse : JsonResponse
    {
        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public IEnumerable<BaseModel> Notes { get; set; }
        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public int? NoteId { get; set; }

    }

}
