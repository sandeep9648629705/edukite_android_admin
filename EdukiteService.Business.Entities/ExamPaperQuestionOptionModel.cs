﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EdukiteService.Business.Entities
{
    public class ExamPaperQuestionOptionModel
    {
        public int Optionid { get; set; }
        public string OptionText { get; set; }
        public string OptionNumber { get; set; }
        public Nullable<int> OptionShort { get; set; }
        public Nullable<int> QuestionId { get; set; }
    }
}
