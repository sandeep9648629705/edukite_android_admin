﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;

namespace EdukiteService.Business.Entities
{
    public class SubTopicModel
    {
        [Key]
        public int SubtopicId { get; set; }
        [Required]
        [RegularExpression("^[a-zA-Z\\s]*$", ErrorMessage = "Only Alphabets allowed.")]
        [StringLength(200, MinimumLength = 1, ErrorMessage = "Only 200 character are allowed")]
        [Remote("IsSubTopicNameExists","SubTopics", AdditionalFields = "initialSubTopicName")]
        public string SubtopicName { get; set; }
        public Nullable<int> TopicId { get; set; }
        public Nullable<int> SubtopicNo { get; set; }
        [Required]
        [RegularExpression("^[a-zA-Z\\s]*$", ErrorMessage = "Only Alphabets allowed.")]
        [StringLength(2, MinimumLength = 1, ErrorMessage = "Only 2 character are allowed")]
        public string TextIcon { get; set; }
        [StringLength(200, MinimumLength = 1, ErrorMessage = "Only 200 character are allowed")]
        public string Description { get; set; }
    }
}
