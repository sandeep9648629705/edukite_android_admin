﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;

namespace EdukiteService.Business.Entities
{
    public class TopicModel
    {
        [Key]
        public int TopicId { get; set; }
        [Required]
        [RegularExpression("^[a-zA-Z\\s]*$", ErrorMessage = "Only Alphabets allowed.")]
        [StringLength(200, MinimumLength = 1, ErrorMessage = "Only 200 characters are allowed")]
        [Remote("IsTopicNameExist", "Topics")]
        public string TopicName { get; set; }
        public Nullable<int> TopicNo { get; set; }
        
        public int SubjectId { get; set; }
        [Required]
        [StringLength(2, MinimumLength = 1, ErrorMessage = "Please enter Text Icon within 2 characters.")]
        public string TextIcon { get; set; }
        [StringLength(200, ErrorMessage = "MaximumLength allowed for description is 200 characters.")]
        public string Description { get; set; }
    }
}
