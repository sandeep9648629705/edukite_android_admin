﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;


namespace EdukiteService.Business.Entities
{
   public class ExamYearModel
    {
        public int id { get; set; }
        public string Year { get; set; }
        public Nullable<int> YearNo { get; set; }
    }
}
