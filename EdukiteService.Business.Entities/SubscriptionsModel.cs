﻿using EdukiteService.Business.Entities;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EdukiteService.Business.Entities
{
    public class SubscriptionsModel
    {
        public int Id { get; set; }
        public string GradeName { get; set; }
        public DateTime DateTime { get; set; }
        public int? DaysLeft { get; set; }
        public string Type { get; set; }
        public int UserId { get; set; }
        public string UserGroupID { get; set; }
        public int? GradeId { get; set; }
        public DateTime SubscribedDatetime { get; set; }
        public int? PromoCodeId { get; set; }
        public int? SubscriptionTypeId { get; set; }
        public DateTime StartDatetime { get; set; }
        public DateTime EndDatetime { get; set; }
        public string IsPaid { get; set; }
        public DateTime CreatedDateTime { get; set; }

        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public string Subjects { get; set; }
        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public string Price { get; set; }

        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public IEnumerable<PromoCodeModel> PromoDiscounts { get; set; }
        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public IEnumerable<PromoCodeModel> EdukiteCredits { get; set; }

    }


    public class SubscriptionResponse : JsonResponse
    {
        public IEnumerable<SubscriptionsModel> Subscriptions { get; set; }
      
    }
}
