﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EdukiteService.Business.Entities
{
    public class ExamPaperTopicModel
    {
        public int ExamPaperTopicId { get; set; }
        public Nullable<int> PaperId { get; set; }
        public Nullable<int> QuestionId { get; set; }
        public Nullable<int> TopicId { get; set; }
        public Nullable<int> TopicNo { get; set; }
    }
}
