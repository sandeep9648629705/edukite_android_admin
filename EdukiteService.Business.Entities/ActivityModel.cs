﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using static EdukiteService.Utilities.Constants;
using EdukiteService.Data;
namespace EdukiteService.Business.Entities
{
    public class ActivityModel
    {
        public UserTypeEnum UserType { get; set; }
        public int DistrictId { get; set; } = 0;
        public int SubjectId { get; set; } = 0;
        public int GradeId { get; set; } = 0;
        public int ProvinceId { get; set; } = 0;
        public int CircuitId { get; set; } = 0;
        public int SchoolId { get; set; } = 0;
        public Term Terms { get; set; }
        public int UserId { get; set; }
        public int RoleTypeId { get; set; }
        public AuthorityTypeEnum Page { get; set; }

    }


    public class DropDown : Activity
    {
        public int Id { get; set; }
        public string Name { get; set; }
    }

    public class ProgressReports : ProgressReportsProperty
    {
        public int CoveredPercentage { get; set; } = 0;
        public int RecommendedPercentage { get; set; } = 0;

    }

}
