﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;


namespace EdukiteService.Business.Entities
{
    public class UsersModel
    {
        [Key]
        public int UserId { get; set; }

        [Required]
        [StringLength(30, ErrorMessage = "Only 30 characters are allowed")]
        [Remote("IsUsersExist", "Users", AdditionalFields = "initialFirstName")]
        public string FirstName { get; set; }
        [Required]
        [StringLength(30, ErrorMessage = "Only 30 characters are allowed")]
        [Remote("IsLastNameExist", "Users", AdditionalFields = "initialLastName")]
        public string LastName { get; set; }
        [Display(Name = "Email address")]
        [Required(ErrorMessage = "The email address is required")]
        [EmailAddress(ErrorMessage = "Invalid Email Address")]
        public string EmailId { get; set; }
        [Required(ErrorMessage = "You must provide a phone number")]
        [Display(Name = "Home Phone")]
        [DataType(DataType.PhoneNumber)]
        [RegularExpression(@"^\(?([0-9]{3})\)?[-. ]?([0-9]{3})[-. ]?([0-9]{4})$", ErrorMessage = "Not a valid phone number")]
        public string ContactNo { get; set; }
        public int? ProvinceId { get; set; }
        public int? GradeID { get; set; }
        public int? UserTypeId { get; set; }
        public int? SchoolId { get; set; }
        public int? Gender { get; set; }
        public DateTime? dob { get; set; }
        public int? TelephoneNo { get; set; }
        public string City { get; set; }
        public int? FavSubID { get; set; }
        public string P_Name { get; set; }
        public string P_EmailId { get; set; }
        public int? P_CellPhoneNo { get; set; }
        public int? P_TelePhoneNo { get; set; }
        public int UserGroup { get; set; }
        public int? Package { get; set; }
        public string CellphoneCode { get; set; }
        public string TelephoneCode { get; set; }
        public string P_CellphoneCode { get; set; }
        public string P_TelephoneCode { get; set; }
        public DateTime? StartDatetime { get; set; }
        public DateTime? EndDatetime { get; set; }
        //public HttpPostedFileBase excelFile { get; set; }

    }
    public class getUserModel
    {
        [Key]
        [Display(Name = "User Id")]
        public int UserId { get; set; }
        [Display(Name = "First Name")]
        public string FirstName { get; set; }
        [Display(Name = "Last Name")]
        public string LastName { get; set; }
        [Display(Name = "Email address")]
        public string EmailId { get; set; }
        [Display(Name = "Phone No")]
        public string ContactNo { get; set; }
        [Display(Name = "Province")]
        public int? ProvinceId { get; set; }
        [Display(Name = "Grade")]
        public int? GradeID { get; set; }
        [Display(Name = "User Type")]
        public int? UserTypeId { get; set; }
        [Display(Name = "School")]
        public int? SchoolId { get; set; }
        [Display(Name = "Gender")]
        public int? Gender { get; set; }
        [Display(Name = "DOB")]
        public DateTime? dob { get; set; }
        [Display(Name = "Telephone No")]
        public int? TelephoneNo { get; set; }
        [Display(Name = "City")]
        public string City { get; set; }
        public int? FavSubID { get; set; }
        public string P_Name { get; set; }
        public string P_EmailId { get; set; }
        public int? P_CellPhoneNo { get; set; }
        public int? P_TelePhoneNo { get; set; }
        public int UserGroup { get; set; }
        public int? Package { get; set; }
        public string CellphoneCode { get; set; }
        public string TelephoneCode { get; set; }
        public string P_CellphoneCode { get; set; }
        public string P_TelephoneCode { get; set; }
        [Display(Name ="Is Active")]
        public bool? IsActive { get; set; }


    }


    public class BulkUserModel
    {
        public int UserTypeId { get; set; }
        [Required(ErrorMessage = "Please select file")]
        [FileExt(Allow = ".xls,.xlsx", ErrorMessage = "Only excel file")]
        public HttpPostedFileBase excelFile { get; set; }
        //HttpPostedFile

    }
    public class FileExt : ValidationAttribute
    {
        public string Allow;
        protected override ValidationResult IsValid(object value, ValidationContext validationContext)
        {
            if (value != null)
            {
                string extension = ((System.Web.HttpPostedFileBase)value).FileName.Split('.')[1];
                if (Allow.Contains(extension))
                    return ValidationResult.Success;
                else
                    return new ValidationResult(ErrorMessage);
            }
            else
                return ValidationResult.Success;
        }
    }
}
