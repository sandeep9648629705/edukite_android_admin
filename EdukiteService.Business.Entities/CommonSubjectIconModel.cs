﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EdukiteService.Business.Entities
{
    public class CommonSubjectIconModel
    {
        [Key]
        public SubjectModel SubjectVM { get; set; }
        public IconsModel IconsVM { get; set; }
    }
}
