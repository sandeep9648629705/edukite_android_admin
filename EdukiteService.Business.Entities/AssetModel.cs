﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;

namespace EdukiteService.Business.Entities
{
    public class AssetModel
    {
        [Key]
        [Required]
        [Remote("IsAssetIdValid", "Asset")]
        public System.Guid AssetId { get; set; }
        public Nullable<int> AssetNo { get; set; }
        [Required]
        [RegularExpression("^[a-zA-Z\\s]*$", ErrorMessage = "Only Alphabets allowed.")]
        [StringLength(300, MinimumLength = 1, ErrorMessage = "Only 300 character are allowed")]
        public string AssetName { get; set; }
        public string FileName { get; set; }

        [StringLength(200, MinimumLength = 1, ErrorMessage = "Only 200 character are allowed")]
        public string Description { get; set; }
        [Required]        
        [Range(0, 9999999999, ErrorMessage = "Only 10 digits are allowed")]
        public Nullable<int> AssetLength { get; set; }
        [Required]
        [Range(0, 9999999999, ErrorMessage = "Only 10 digits are allowed")]
        public Nullable<int> SuggestedActiveDuration { get; set; }
        public string FolderName { get; set; }
        public int Version { get; set; }
        public Nullable<int> AssetTypeId { get; set; }
        public Nullable<int> SubtopicId { get; set; }
        public Nullable<int> TopicId { get; set; }
        public Nullable<int> TermNo { get; set; }
        public Nullable<int> WeekNo { get; set; }
        public string ThumbnailFileName { get; set; }
        public TopicModel topicModelInAsset { get; set; }
        public ModuleModel moduleModelInAsset { get; set; }
        public SubTopicModel subtopicModelInAsset { get; set; }
        public AssetTypeModel assetTypeModelInAsset { get; set; }
        [RegularExpression(@"([a-zA-Z0-9\s_\\.\-:])+(.zip)$", ErrorMessage = "Only Zip file allowed.")]
        public HttpPostedFileBase PostedFile { get; set; }
        [RegularExpression(@"([a-zA-Z0-9\s_\\.\-:])+(.mp4)$", ErrorMessage = "Only mp4 files allowed.")]
        public HttpPostedFileBase PostedFileHigh { get; set; }
        [RegularExpression(@"([a-zA-Z0-9\s_\\.\-:])+(.mp4)$", ErrorMessage = "Only mp4 files allowed.")]
        public HttpPostedFileBase PostedFileMedium { get; set; }
        [RegularExpression(@"([a-zA-Z0-9\s_\\.\-:])+(.mp4)$", ErrorMessage = "Only mp4 files allowed.")]
        public HttpPostedFileBase PostedFileLow { get; set; }
        [RegularExpression(@"([a-zA-Z0-9\s_\\.\-:])+(.png|.jpg)$", ErrorMessage = "Only png/jpg files allowed.")]
        public HttpPostedFileBase PostedFileThumbnail { get; set; }
        [RegularExpression(@"([a-zA-Z0-9\s_\\.\-:])+(.zip)$", ErrorMessage = "Only zip files allowed.")]
        public HttpPostedFileBase PostedFileZip { get; set; }
        public bool isAssetHide { get; set; } 
    }
}
