﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;

namespace EdukiteService.Business.Entities
{
    public class GradeModel
    {
        [Key]
        
        public int GradeId { get; set; }
        [Required]
        [StringLength(3, ErrorMessage = "Only 3 digits are allowed")]
        [Remote("IsGradeNameExist", "Grades", AdditionalFields = "initialGradeName")]
        public string GradeName { get; set; }
        public int? PhaseId { get; set; }        
        [StringLength(150, ErrorMessage = "Only 150 characters are allowed")]
        public string Description { get; set; }
        public bool IsPublished { get; set; }
        public bool isEnabled { get; set; }

    }
}
