﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EdukiteService.Business.Entities
{
    class SchoolModel
    {
        public int SchoolId { get; set; }
        public string SchoolName { get; set; }
        public Nullable<int> CircuitId { get; set; }

    }
}
