﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EdukiteService.Business.Entities
{
    public class UserDetailsModel
    {
        public string lastName { get; set; }
        public string profilePicPath { get; set; }
        public DateTime? dob { get; set; }
        public int? gender { get; set; }
        public string firstName { get; set; }
        public string schoolName { get; set; }
        public List<Grades> grades { get; set; }
        public int userId { get; set; }
        public string parentsName { get; set; }
        public int? provinceId { get; set; }
        public string provinceName { get; set; }
        public int? favoriteSubjectId { get; set; }
        public string favoriteSubjectName { get; set; }
        public string cellphone { get; set; }
        public string telephone { get; set; }
        public string email { get; set; }
        public string parentCellphone { get; set; }
        public string parentEmail { get; set; }
        public int? cityId { get; set; }
        public string cityName { get; set; }
        public int? isFirstTimeLoggedIn { get; set; }
        public DateTime? registerDate { get; set; }
    }
    public class Grades
    {
        public int? id { get; set; }
        public string gradeName { get; set; }

    }
    public class UsersMode
    {
        public UserDetailsModel userDetails { get; set; }
        public int userId { get; set; }
        public int deviceId { get; set; }
    }
}
