﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EdukiteService.Business.Entities
{
    public class UserModel :Activity
    {
        public int UserId { get; set; }
        public string UserName { get; set; }
        public string Password { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string ContactNo { get; set; }
        public string EmailId { get; set; }
        public int? Status { get; set; }
        public int? deviceId { get; set; }
        public int UserTypeId { get; set; }
        public string UserType { get; set; }
        public int? RoleType { get; set; }
        public string SchoolName { get; set; }
        public DateTime? CreatedDatetime { get; set; }
        public DateTime? UpdatedDatetime { get; set; }
        public bool IsAdminUser { get; set; }
        public int? ProvinceId { get; set; }
        public int? DistrictId { get; set; }
        public int? CircuitId { get; set; }
        public int? SchoolId { get; set; }
        public bool? IsHOD { get; set; }
        public bool? IsEmailSent { get; set; }
        public int? ClassId { get; set; }

        public int? SchoolCircuitId { get; set; }
        public bool? IsGuest { get; set; }
        public bool? IsFirstLogin { get; set; }
        public string OldPassword { get; set; }

        public int? LoginAttempt { get; set; } = 0;
    }
}
