﻿using EdukiteService.Business.Entities;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EdukiteService.Business.Entities
{
    public class VideoCommentModel : VideoCommentBaseModel
    {
        public int CommentId { get; set; }
        public int UserId { get; set; }
        public int DeviceId { get; set; }
        public int TopicId { get; set; }
        public int SubTopicId { get; set; }
        public int SubjectId { get; set; }
        public Guid? AssetId { get; set; }
    }

    public class VideoCommentBaseModel
    {
        public int Id { get; set; }
        public string Text { get; set; }
        public string PostedBy { get; set; }
        public DateTime DateTime { get; set; }
        public string UserThumbailPath { get; set; }
    }

    public class VideoCommentReplies : VideoCommentBaseModel
    {
        public int RepliesCount { get; set; }
    }
    public class VideoCommentResponse : JsonResponse
    {
        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public IEnumerable<VideoCommentReplies> Comments { get; set; }

        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public IEnumerable<VideoCommentBaseModel> Replies { get; set; }
    }

}
