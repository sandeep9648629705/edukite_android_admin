﻿using EdukiteService.Utilities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EdukiteService.Business.Entities
{
    public class JsonResponse
    {
        public string StatusMessage { get; set; } = Enums.S001.ToString();
        public string Status { get; set; } = "SUCCESS";
    }
}
