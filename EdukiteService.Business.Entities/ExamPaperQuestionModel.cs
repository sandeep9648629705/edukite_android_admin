﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EdukiteService.Business.Entities
{
   public  class ExamPaperQuestionModel
    {
        public int QuestionId { get; set; }
        public Nullable<int> PaperId { get; set; }
        public string QuestionNo { get; set; }
        public string QuestionText { get; set; }
        public string Note { get; set; }
        public Nullable<int> endat { get; set; }
        public Nullable<int> startat { get; set; }
        public Nullable<int> marks { get; set; }
        public Nullable<System.Guid> Assetid { get; set; }
        public Nullable<System.Guid> SolutionAssetId { get; set; }
        public Nullable<int> PQid { get; set; }
    }
}
