﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EdukiteService.Business.Entities
{
    public class CommonGradeSubjectModel
    {
        [Key]
        public List<GradeModel> GradeVM { get; set; }
        public List<SubjectModel> SubjectVM { get; set; }
    }
}
