﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EdukiteService.Business.Entities
{
    public class QuestionJsonDataModel
    {

        public string statusMessage { get; set; }
        public string statusCode { get; set; }

        public class Question
        {
            public string quesId { get; set; }
            public string quesNum { get; set; }
            public string quesText { get; set; }
            public string videoName { get; set; }
            public string videoPath { get; set; }
            public string startsAt { get; set; }
            public string topicId { get; set; }
            public string topicName { get; set; }
            public string marks { get; set; }
            public string endsAt { get; set; }
            public string description { get; set; }
        }
        public class SubQuestion
        {
            public string quesId { get; set; }
            public string quesNum { get; set; }
            public string quesText { get; set; }
            public string videoName { get; set; }
            public string videoPath { get; set; }
            public string startsAt { get; set; }
            public string topicId { get; set; }
            public string topicName { get; set; }
            public string marks { get; set; }
            public string endsAt { get; set; }
            public string description { get; set; }
        }

        public class subQuestions
        {
          public string optionNum { get; set; }
          public int? optionOrder { get; set; }
          public string optionText { get; set; }
        }
    }
}
