﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;

namespace EdukiteService.Business.Entities
{
    public class IconsModel
    {
        [Key]
        public int IconID { get; set; }
        public int SubjectId { get; set; }
        public string IconName { get; set; }
        public string IconPath { get; set; }
        public string IconType { get; set; }
        [RegularExpression(@"([a-zA-Z0-9\s_\\.\-:])+(.zip)$", ErrorMessage = "Only Zip files allowed.")]
        public HttpPostedFileBase PostedFile { get; set; }
    }
}
