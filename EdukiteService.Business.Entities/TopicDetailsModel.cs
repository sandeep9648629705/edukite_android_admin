﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EdukiteService.Business.Entities
{
    public class TopicDetailsModel
    {
        public int subjectId { get; set; }
        public int topicId { get; set; }
        public int deviceId { get; set; }
        public int userId { get; set; }
        public string TopicName { get; set; }
        public int? totalVideosCount { get; set; }
        public int? completedVideosCount { get; set; }
        public int? totalTestCount { get; set; }
        public int? completedTestCount { get; set; }
        public int? totalInteractiveCount { get; set; }
        public int? completedInteractiveCount { get; set; }
        public int? totalExamQuestionVideoCount { get; set; }
        public int? completedExamQuestionVideoCount { get; set; }
        public List<SubTopicsDetailsModel> subTopics { get; set; }
    }
    public class SubTopicsDetailsModel
    {
        public int? id { get; set; }
        public string name { get; set; }
        public string imagePath { get; set; }
        public Double? completionPercentage { get; set; }
    }
}
