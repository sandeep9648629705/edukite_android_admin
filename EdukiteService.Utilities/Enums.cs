﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EdukiteService.Utilities
{
    public enum Enums
    {
        S001,
        E001,
        E005,
    }

    public enum CodeTypes
    {
        NONE = 0,
        PROMOCODE = 1,
        EDUKITECREDITS = 2
    }
}
