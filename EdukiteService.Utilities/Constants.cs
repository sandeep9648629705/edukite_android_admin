﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EdukiteService.Utilities
{
    public static class Constants
    {
        #region RESPONSE MESSAGE
        public static string SuccessMessage = "SUCCESS!";
        public static string FailureMessage = "FAILED!";
        public static string NotExistsMessage = "NOT EXISTS!";
        public static string AlreadyExistMessage = "ALREADY EXIST!";
        public static string InvalidKeyMessage = "INVALID KEY!";
        public static string AlreadyUsedMessage = "ALREADY USED!";
        public static string LicenseExpiredMessage = "LICENSE EXPIRED!";
        public static string NotAllowedMessage = "NOT ALLOWED TO INSTALL IN THIS MACHINE!";


        #endregion
        public const string OTPLENGTH = "OTPlength";
        public const string SP_PARAM_EMPLOYEEID = "EmployeeId";
        public const string SP_PARAM_USERID = "UserID";
        public const string DATEFORMAT = "dd-MM-yyyy HH:mm:ss";
        public const string ALLOWEDCHARACTERS = "0,1,2,3,4,5,6,7,8,9";
        #region 
        //List of Procedures  
        public const string sp_SetSubscription = "exec [Edu_Android_SetSubscription]  @UserId,@UserGroup";
        public const string SP_GETUSERS = "exec [GetUsers] @EmployeeId";
        public const string SP_GETUSERDETAILS = "exec [Edu_Android_GetUserDetails] @UserId";
        public const string SP_GETUSERGRADES = "exec [Edu_Android_GetUserGrades] @UserId";
        public const string SP_GETSUBJECTDETAILS = "exec [Edu_Android_GetSubjectDetails] @SubjectId";
        public const string SP_GETTOPICDETAILSBYSUBJECT = "exec [Edu_Android_GetTopicDetailsBySubject] @SubjectId";

        public const string SP_GETTOPICDETAILS = "exec [Edu_Android_GetTopicDetails] @TopicId";
        public const string SP_GETSUBTOPICDETAILSBYTOPIC = "exec [Edu_Android_GetsubTopicDetailsByTopic] @TopicId";
        public static string SP_GET_GRADES = "dbo.GetGrades @UserId, @ProvinceId, @DistrictId,@CircuitId,@SchoolId";
        #endregion

        #region STORED PROCEDURE VARIABLE

        public static string SP_PARAM_EMAILID = "EmailId";
        public static string SP_PARAM_PASSWORD = "Password";
        public static string SP_PARAM_IS_ADMIN_USER = "IsAdminUser";
        public static string SP_PARAM_PROVINCEID = "ProvinceId";

        public static string SP_PARAM_ACTIVITY_USERTYPE = "UserType";
        public static string SP_PARAM_ACTIVITY_SUBJECTID = "SubjectId";
        public static string SP_PARAM_ACTIVITY_GRADEID = "GradeId";
        public static string SP_PARAM_ACTIVITY_PROVINCEID = "ProvinceId";
        public static string SP_PARAM_ACTIVITY_DISTRICTID = "DistrictId";
        public static string SP_PARAM_ACTIVITY_CIRCUITID = "CircuitId";
        public static string SP_PARAM_ACTIVITY_SCHOOLID = "SchoolId";
        public static string SP_PARAM_ACTIVITY_USERID = "UserId";
        public static string SP_PARAM_ACTIVITY_TERMNO = "TermNo";
        public static string SP_PARAM_ACTIVITY_STRARTDATETIME = "StartDatetime";
        public static string SP_PARAM_ACTIVITY_ENDDATETIME = "EndDateTime";
        public static string SP_PARAM_ACTIVITY_Filter = "Filter";
        public static string SP_PARAM_ACTIVITY_ROLETYPEID = "RoleType";
        public static string SP_PARAM_ACTIVITY_TOPICID = "TopicId";
        public static string SP_PARAM_ACTIVITY_PAGE = "Page";
        public static string SP_PARAM_USAGE_STARTWEEKNO = "StartWeekNo";
        public static string SP_PARAM_USAGE_ENDWEEKNO = "EndWeekNo";
        public static string SP_PARAM_ACTIVITY_USERGROUPID = "UserGroup";
        #endregion


        public enum LicenseTypeEnum
        {
            FreeTrail = 1,
            SingleLicense = 2,
            VolumeLicense = 3
        }

        public enum UserTypeEnum
        {
            Learner = 1,
            Teacher = 2

        }

        public enum ProjectLinkingEnum
        {
            [Description("Province")]
            Province = 1,
            [Description("Individual School")]
            School = 2,
            [Description("Individual User")]
            User = 3

        }

        public enum ClassTypeEnum
        {
            A = 1,
            B = 2,
            C = 3,
            D = 4,
            E = 5,
            F = 6,
            G = 7,
            H = 8,
            I = 9,
            J = 10

        }

        public enum AuthorityTypeEnum
        {
            [Description("Province")]
            Province = 1,
            [Description("District")]
            District = 2,
            [Description("Circuit")]
            Circuit = 3,
            [Description("School")]
            School = 4

        }
    }
}
