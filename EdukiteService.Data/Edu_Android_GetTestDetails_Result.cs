//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace EdukiteService.Data
{
    using System;
    
    public partial class Edu_Android_GetTestDetails_Result
    {
        public string testName { get; set; }
        public int totalQuestions { get; set; }
        public int totalMarks { get; set; }
        public int TotalDuration { get; set; }
    }
}
