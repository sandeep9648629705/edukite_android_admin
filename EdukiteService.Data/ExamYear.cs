//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace EdukiteService.Data
{
    using System;
    using System.Collections.Generic;
    
    public partial class ExamYear
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public ExamYear()
        {
            this.ExamPapers = new HashSet<ExamPaper>();
        }
    
        public int id { get; set; }
        public string Year { get; set; }
        public Nullable<int> YearNo { get; set; }
    
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<ExamPaper> ExamPapers { get; set; }
    }
}
