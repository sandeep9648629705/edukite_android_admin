//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace EdukiteService.Data
{
    using System;
    using System.Collections.Generic;
    
    public partial class AnswerOption
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public AnswerOption()
        {
            this.TestAnswers = new HashSet<TestAnswer>();
            this.TestResponses = new HashSet<TestResponse>();
        }
    
        public int AnswerOptionId { get; set; }
        public Nullable<int> QuestionId { get; set; }
        public string Option { get; set; }
    
        public virtual TestQuestion TestQuestion { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<TestAnswer> TestAnswers { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<TestResponse> TestResponses { get; set; }
    }
}
